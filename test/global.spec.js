'use strict';

var _ = require('lodash');
var createApp = require('../app/app.js');
var mongoose = require('mongoose');
var Promise = require('bluebird');
Promise.longStackTraces();

var app;
before(function ()
{
    var port = 3000;
    app = createApp(port);
    this.baseUrl = 'http://localhost:' + port;
});

after(function ()
{
    app.close();
    _.forEach(mongoose.models, function (value, key)
    {
        delete mongoose.models[key];
    });
});
