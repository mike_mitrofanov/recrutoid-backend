'use strict';

var expect = require('chai').expect;
var request = require('supertest-as-promised');
var sinon = require('sinon');
var sinonChai = require('sinon-chai');
var proxyquire = require('proxyquire');
require('chai').use(sinonChai);


var seeder = require('./seeder.js');

describe('Users endpoint', function ()
{
    describe('GET /api/users', function ()
    {
        before(function ()
        {
            return seeder().dropCollectionsAndSeed(require('./fixtures/users.search.json'));
        });

        describe('when user is admin', function ()
        {
            var authHeader;
            beforeEach(function ()
            {
                var credentials = {email: 'bernard.labno@example.com', password: 'password'};
                return request(this.baseUrl).post('/api/users/auth').send(credentials).expect(200).then(function (response)
                {
                    authHeader = 'Token ' + new Buffer(response.body.token).toString('base64');
                });
            });
            it('should respond with paginated job seekers list', function ()
            {
                return request(this.baseUrl).get('/api/users').set('Authorization', authHeader).expect(200).expect(function (res)
                {
                    expect(res.body.results).to.have.length(20);
                    expect(res.body.total).to.equal(55);
                });
            });
            it('should respond with paginated employers list', function ()
            {
                return request(this.baseUrl).get('/api/users?role=employer').set('Authorization', authHeader).expect(200).expect(function (res)
                {
                    expect(res.body.results).to.have.length(20);
                    expect(res.body.total).to.equal(56);
                });
            });
        });

        describe('when user is authenticated as employer', function ()
        {
            var authHeader;
            beforeEach(function ()
            {
                var credentials = {email: 'employer@example.com', password: 'password'};
                return request(this.baseUrl).post('/api/users/auth').send(credentials).expect(200).then(function (response)
                {
                    authHeader = 'Token ' + new Buffer(response.body.token).toString('base64');
                });
            });
            it('should respond with paginated users list containing only normal, verified, not banned users', function ()
            {
                return request(this.baseUrl).get('/api/users').set('Authorization', authHeader).expect(200).expect(function (res)
                {
                    expect(res.body.results).to.have.length(1);
                    expect(res.body.total).to.equal(1);
                });
            });
        });

        describe('when user is NOT authenticated as employer or admin', function ()
        {
            var authHeader;
            beforeEach(function ()
            {
                var credentials = {email: 'jack@example.com', password: 'password'};
                return request(this.baseUrl).post('/api/users/auth').send(credentials).expect(200).then(function (response)
                {
                    authHeader = 'Token ' + new Buffer(response.body.token).toString('base64');
                });
            });
            it('should respond with 403', function ()
            {
                return request(this.baseUrl).get('/api/users').set('Authorization', authHeader).expect(403);
            });
        });

        describe('when user is NOT authenticated', function ()
        {
            it('should respond with paginated users list containing only normal, verified, not banned users', function ()
            {
                return request(this.baseUrl).get('/api/users').expect(403);
            });
        });
    });
    describe('POST /api/users/:id/ban', function ()
    {
        beforeEach(function ()
        {
            return seeder().dropCollectionsAndSeed(require('./fixtures/users.search.json'));
        });

        describe('when user is admin', function ()
        {
            var authHeader;
            beforeEach(function ()
            {
                var credentials = {email: 'bernard.labno@example.com', password: 'password'};
                return request(this.baseUrl).post('/api/users/auth').send(credentials).expect(200).then(function (response)
                {
                    authHeader = 'Token ' + new Buffer(response.body.token).toString('base64');
                });
            });
            it('should ban that user', function ()
            {
                var baseUrl = this.baseUrl;
                return request(baseUrl).post('/api/users/88eae100c2e6b6c222ec3431/ban').set('Authorization', authHeader).expect(200).then(function ()
                {
                    return request(baseUrl).get('/api/users?role=employer&email=employer@example.com').set('Authorization', authHeader).then(function (res)
                    {
                        expect(res.body.total).to.equal(1);
                        expect(res.body.results[0].banned).to.equal(true);
                    });
                });
            });
        });
        describe('when user is NOT admin', function ()
        {
            var authHeader;
            beforeEach(function ()
            {
                var credentials = {email: 'employer@example.com', password: 'password'};
                return request(this.baseUrl).post('/api/users/auth').send(credentials).expect(200).then(function (response)
                {
                    authHeader = 'Token ' + new Buffer(response.body.token).toString('base64');
                });
            });
            it('should respond with 403', function ()
            {
                return request(this.baseUrl).post('/api/users/88eae100c2e6b6c222ec3431/ban').set('Authorization', authHeader).expect(403);
            });
        });
        describe('when user is NOT authenticated', function ()
        {
            it('should respond with 401', function ()
            {
                return request(this.baseUrl).post('/api/users/88eae100c2e6b6c222ec3431/ban').expect(401);
            });
        });
    });
});
describe('User manager', function ()
{
    var business = require('../app/business/business.container.js');
    var sparkPostMock = {sendActivationEmail: sinon.spy()};
    var userManager = proxyquire('../app/business/users.manager.js', {'../service/sparkPost': sparkPostMock});
    describe('when user is admin', function ()
    {
        describe('when user not activated account', function ()
        {
            beforeEach(function ()
            {
                return seeder().dropCollectionsAndSeed(require('./fixtures/users.search.json')).then(function ()
                {
                    return userManager.create({user: {role: 'admin'}}, business).resendRegistrationEmail('55e81a98d6152bb713a60d1c');
                });
            });
            it('should resend email', function ()
            {
                expect(sparkPostMock.sendActivationEmail.callCount).to.equal(1);
            });
        });
        describe('when user activated account', function ()
        {
            beforeEach(function ()
            {
                sparkPostMock.sendActivationEmail.reset();
                return seeder().dropCollectionsAndSeed(require('./fixtures/users.search.json')).then(function ()
                {
                    return userManager.create({user: {role: 'admin'}}, business).resendRegistrationEmail('55e81e67d1c49e2d1661f621');
                });
            });
            it('should resend email', function ()
            {
                expect(sparkPostMock.sendActivationEmail.callCount).to.equal(0);
            });
        });
    });
    describe('when user is not admin', function ()
    {
        it('should throw error', function ()
        {
            return seeder().dropCollectionsAndSeed(require('./fixtures/users.search.json')).then(function ()
            {
                return userManager.create({user: {role: 'normal'}}, business).resendRegistrationEmail('55e81a98d6152bb713a60d1c').catch(function (error)
                {
                    expect(error).to.eql({error: {message: 'FORBIDDEN', code: 403}, message: 'You don\'t have access to this page or resource'});
                });
            });
        });
    });
});
