'use strict';

var _ = require('lodash');
var expect = require('chai').expect;
var request = require('supertest-as-promised');
var sinon = require('sinon');
var sinonChai = require('sinon-chai');
var proxyquire = require('proxyquire');
require('chai').use(sinonChai);

var seeder = require('./seeder.js');
var dateHelper = require('../app/service/dateHelper.js');

function arrayMatcher(expectedArray)
{
    return sinon.match(function (value)
    {
        var valid = true;
        _.forEach(expectedArray, function (item, key)
        {
            var match = sinon.match(item);
            valid = valid && match.test(value[key]);
        });
        return valid;
    }, 'match(' + JSON.stringify(expectedArray) + ')');
}

describe('Vacancies endpoint', function ()
{
    describe('is-new', function ()
    {
        beforeEach(function ()
        {
            return seeder().dropCollectionsAndSeed(require('./fixtures/vacancy.isNew.json'));
        });

        describe('when user is authenticated', function ()
        {
            var authHeader;
            beforeEach(function ()
            {
                var credentials = {email: 'bernard.labno@example.com', password: 'password'};
                return request(this.baseUrl).post('/api/users/auth').send(credentials).expect(200).then(function (response)
                {
                    authHeader = 'Token ' + new Buffer(response.body.token).toString('base64');
                });
            });
            describe('when posted array contains unknown id', function ()
            {
                it('should respond with proper hash where vacancy is set  to true', function ()
                {
                    var payload = ['dddddddddddddddddddddddd'];
                    var expectedResult = {dddddddddddddddddddddddd: true};
                    return request(this.baseUrl).post('/api/vacancies/is-new').set('Authorization', authHeader).send(payload).expect(expectedResult);
                });
            });

            describe('when posted array contains only 2 ids, one seen and one new', function ()
            {
                it('should respond with proper hash where first vacancy is false and second is true', function ()
                {
                    var payload = ['aaaaaaaaaaaaaaaaaaaaaaaa', 'bbbbbbbbbbbbbbbbbbbbbbbb'];
                    var expectedResult = {aaaaaaaaaaaaaaaaaaaaaaaa: false, bbbbbbbbbbbbbbbbbbbbbbbb: true};
                    return request(this.baseUrl).post('/api/vacancies/is-new').set('Authorization', authHeader).send(payload).expect(expectedResult);
                });
            });

            describe('when I check once if vacancies are new', function ()
            {
                var payload = ['aaaaaaaaaaaaaaaaaaaaaaaa', 'bbbbbbbbbbbbbbbbbbbbbbbb', 'cccccccccccccccccccccccc'];
                beforeEach(function ()
                {
                    return request(this.baseUrl).post('/api/vacancies/is-new').set('Authorization', authHeader).send(payload);
                });
                it('should vacancies be seen at the second checking', function ()
                {
                    var expectedResult = {aaaaaaaaaaaaaaaaaaaaaaaa: false, bbbbbbbbbbbbbbbbbbbbbbbb: false, cccccccccccccccccccccccc: false};
                    return request(this.baseUrl).post('/api/vacancies/is-new').set('Authorization', authHeader).send(payload).expect(expectedResult);
                });
            });

        });

        describe('when user is NOT authenticated', function ()
        {
            describe('when posted array contains only 2 ids, one seen and one new', function ()
            {
                it('should always respond with hash composed of true values', function ()
                {
                    var payload = ['aaaaaaaaaaaaaaaaaaaaaaaa', 'bbbbbbbbbbbbbbbbbbbbbbbb'];
                    var expectedResult = {aaaaaaaaaaaaaaaaaaaaaaaa: true, bbbbbbbbbbbbbbbbbbbbbbbb: true};
                    return request(this.baseUrl).post('/api/vacancies/is-new').send(payload).expect(expectedResult);
                });
            });
        });
    });

    describe('POST vacancy', function ()
    {
        var vacancy;
        beforeEach(function ()
        {
            vacancy = {
                categories: ['55d7380df9253c03002ee99e'],
                cityId: '55d7380df9253c03002ee9dc',
                email: 'bernard.labno@yahoo.com',
                publishViaEmail: true,
                showEmailOnWebsite: true,
                companyName: 'ITC',
                salary: 123,
                title: 'ITC Lab',
                duty: '<p>1</p>',
                responsibility: '<p>22</p>',
                conditions: '<p>33</p>'
            };
        });

        describe('when vacancy has publishOnWebsite field equal true', function ()
        {
            beforeEach(function ()
            {
                vacancy.publishOnWebsite = true;
                return seeder().dropCollectionsAndSeed(require('./fixtures/vacancy.post.json'));
            });

            describe('when email is from blacklisted domain', function ()
            {
                it('should respond with 460 http status code', function ()
                {
                    return request(this.baseUrl).post('/api/vacancies').send(vacancy).expect(460);
                });
            });

            describe('when email is already registered', function ()
            {
                it('should respond with 412 http status code', function ()
                {
                    vacancy.email = 's4237@example.com';
                    return request(this.baseUrl).post('/api/vacancies').send(vacancy).expect(412);
                });
            });
        });

        describe('when vacancy has NOT publishOnWebsite field equal true', function ()
        {
            beforeEach(function ()
            {
                vacancy.publishOnWebsite = false;
                return seeder().dropCollectionsAndSeed(require('./fixtures/vacancy.post.json'));
            });

            describe('when email is from blacklisted domain', function ()
            {
                it('should respond with 460 http status code', function ()
                {
                    return request(this.baseUrl).post('/api/vacancies').send(vacancy).expect(460);
                });
            });

            describe('when email is already registered', function ()
            {
                it('should respond with 412 http status code', function ()
                {
                    vacancy.email = 's4237@example.com';
                    return request(this.baseUrl).post('/api/vacancies').send(vacancy).expect(412);
                });
            });
        });
    });

    describe('GET vacancies', function ()
    {
        var body;
        var authHeader;

        before(function ()
        {
            return seeder().dropCollectionsAndSeed(require('./fixtures/vacancy.search.json'));
        });

        describe('when user is not admin', function ()
        {
            before(function ()
            {
                var credentials = {email: 'bernard.labno@example.com', password: 'password'};
                var baseUrl = this.baseUrl;
                return request(baseUrl).post('/api/users/auth').send(credentials).expect(200).then(function (response)
                {
                    authHeader = 'Token ' + new Buffer(response.body.token).toString('base64');
                }).then(function ()
                {
                    return request(baseUrl).get('/api/vacancies?publishMode=website').set('Authorization', authHeader).expect(function (res)
                    {
                        body = res.body;
                    });
                });
            });

            it('should return 3 vacancies', function ()
            {
                expect(body.results).to.have.length(3);
            });

            it('should return only approved vacancies', function ()
            {
                _.forEach(body.results, function (item)
                {
                    // TODO: should count all results insted of checkig property
                    expect(item).to.have.property('approved', true);
                });
            });

            it('should return only vacancies published on website', function ()
            {
                _.forEach(body.results, function (item)
                {
                    expect(item).to.have.property('publishOnWebsite', true);
                });
            });

            it('should NOT return banned vacancies', function ()
            {
                _.forEach(body.results, function (item)
                {
                    // TODO: should count all results insted of checkig property
                    expect(item).to.have.property('banned', false);
                });
            });

        });

        describe('when user is anonymous', function ()
        {
            before(function ()
            {
                return request(this.baseUrl).get('/api/vacancies?publishMode=website').expect(function (res)
                {
                    body = res.body;
                });
            });

            it('should return 3 vacancies', function ()
            {
                expect(body.results).to.have.length(3);
            });

            it('should return only approved vacancies', function ()
            {
                _.forEach(body.results, function (item)
                {
                    // TODO: should count all results insted of checkig property
                    expect(item).to.have.property('approved', true);
                });
            });

            it('should return only vacancies published on website', function ()
            {
                _.forEach(body.results, function (item)
                {
                    expect(item).to.have.property('publishOnWebsite', true);
                });
            });

            it('should NOT return banned vacancies', function ()
            {
                _.forEach(body.results, function (item)
                {
                    // TODO: should count all results insted of checkig property
                    expect(item).to.have.property('banned', false);
                });
            });

            it('should NOT return archived vacancies', function ()
            {
                _.forEach(body.results, function (item)
                {
                    // TODO: should count all results insted of checkig property
                    expect(item).to.have.property('archived', false);
                });
            });
        });

        describe('when user is admin', function ()
        {
            before(function ()
            {
                var credentials = {email: 'admin@example.com', password: 'password'};
                var baseUrl = this.baseUrl;
                return request(baseUrl).post('/api/users/auth').send(credentials).expect(200).then(function (response)
                {
                    authHeader = 'Token ' + new Buffer(response.body.token).toString('base64');
                });
            });

            describe('when filtering NOT approved vacancies', function ()
            {
                beforeEach(function ()
                {
                    return request(this.baseUrl).get('/api/vacancies?approved=false&adminPanel=true').set('Authorization', authHeader).expect(function (res)
                    {
                        body = res.body;
                    });
                });
                it('should return also not approved vacancies', function ()
                {
                    expect(body.results).to.have.length(2);
                    _.forEach(body.results, function (item)
                    {
                        expect(item).to.have.property('approved', false);
                    });
                });
            });

            describe('when filtering approved vacancies', function ()
            {
                beforeEach(function ()
                {
                    return request(this.baseUrl).get('/api/vacancies?approved=true&adminPanel=true').set('Authorization', authHeader).expect(function (res)
                    {
                        body = res.body;
                    });

                });
                it('should return approved vacancies', function ()
                {
                    expect(body.results).to.have.length(6);
                    _.forEach(body.results, function (item)
                    {
                        expect(item).to.have.property('approved', true);
                    });
                });
            });

            describe('when no filter specified', function ()
            {
                beforeEach(function ()
                {
                    return request(this.baseUrl).get('/api/vacancies?adminPanel=true').set('Authorization', authHeader).expect(function (res)
                    {
                        body = res.body;
                    });

                });
                it('should return all vacancies', function ()
                {
                    expect(body.results).to.have.length(9);
                });
            });
        });

        describe('when user is employer', function ()
        {
            beforeEach(function ()
            {
                var credentials = {email: 'employer@example.com', password: 'password'};
                var baseUrl = this.baseUrl;
                return request(baseUrl).post('/api/users/auth').send(credentials).expect(200).then(function (response)
                {
                    authHeader = 'Token ' + new Buffer(response.body.token).toString('base64');
                }).then(function ()
                {
                    return request(baseUrl).get('/api/vacancies?myAll=true').set('Authorization', authHeader).expect(function (res)
                    {
                        body = res.body;
                    });
                });
            });

            it('should return 6 vacancies', function ()
            {
                expect(body.results).to.have.length(6);
            });

            it('should return only employers vacancies', function ()
            {
                _.forEach(body.results, function (item)
                {
                    expect(item).to.have.deep.property('userId._id', '88eae100c2e6b6c222ec3431');
                });
            });
        });
    });

    describe('GET vacancy', function ()
    {
        var authHeader;

        before(function ()
        {
            return seeder().dropCollectionsAndSeed(require('./fixtures/vacancy.search.json'));
        });

        describe('when user is NOT admin', function ()
        {
            before(function ()
            {
                var credentials = {email: 'bernard.labno@example.com', password: 'password'};
                var baseUrl = this.baseUrl;
                return request(baseUrl).post('/api/users/auth').send(credentials).expect(200).then(function (response)
                {
                    authHeader = 'Token ' + new Buffer(response.body.token).toString('base64');
                });
            });
            describe('but is vacancy owner', function ()
            {
                describe('and vacancy is NOT approved', function ()
                {
                    it('should return the vacancy', function ()
                    {
                        return request(this.baseUrl).get('/api/vacancies/55d7bf396048db400d53ca6d').set('Authorization',
                                authHeader).expect(200).expect(function (res)
                                {
                                    expect(res.body).to.have.property('id', '55d7bf396048db400d53ca6d');
                                });
                    });
                });
            });
            describe('and NOT vacancy owner', function ()
            {
                describe('and vacancy is NOT approved', function ()
                {
                    it('should return 403', function ()
                    {
                        return request(this.baseUrl).get('/api/vacancies/66d7bf396048db400d53ca6d').set('Authorization', authHeader).expect(403);
                    });
                });

                describe('and vacancy is approved', function ()
                {
                    it('should return the vacancy', function ()
                    {
                        return request(this.baseUrl).get('/api/vacancies/77d7bf396048db400d53ca6d').set('Authorization',
                                authHeader).expect(200).expect(function (res)
                                {
                                    expect(res.body).to.have.property('id', '77d7bf396048db400d53ca6d');
                                });
                    });
                });
            });
        });

        describe('when user is admin', function ()
        {
            before(function ()
            {
                var credentials = {email: 'admin@example.com', password: 'password'};
                var baseUrl = this.baseUrl;
                return request(baseUrl).post('/api/users/auth').send(credentials).expect(200).then(function (response)
                {
                    authHeader = 'Token ' + new Buffer(response.body.token).toString('base64');
                });
            });
            describe('and vacancy is NOT approved', function ()
            {
                it('should return the vacancy', function ()
                {
                    return request(this.baseUrl).get('/api/vacancies/55d7bf396048db400d53ca6d').set('Authorization',
                            authHeader).expect(200).expect(function (res)
                            {
                                expect(res.body).to.have.property('id', '55d7bf396048db400d53ca6d');
                            });
                });
            });
        });
    });
});

describe('Vacancy manager', function ()
{

    function standardTests(holder)
    {
        it('should not include banned', function ()
        {
            expect(holder.vacancies).to.have.length.above(0);
            _.forEach(holder.vacancies, function (item)
            {
                // TODO: should count all results insted of checkig property
                expect(item).to.have.property('banned', false);
            });
        });

        it('should not include archived', function ()
        {
            expect(holder.vacancies).to.have.length.above(0);
            _.forEach(holder.vacancies, function (item)
            {
                // TODO: should count all results insted of checkig property
                expect(item).to.have.property('archived', false);
            });
        });

        it('should include only approved', function ()
        {
            expect(holder.vacancies).to.have.length.above(0);
            _.forEach(holder.vacancies, function (item)
            {
                // TODO: should count all results insted of checkig property
                expect(item).to.have.property('approved', true);
            });
        });

        it('should include only published via email', function ()
        {
            expect(holder.vacancies).to.have.length.above(0);
            _.forEach(holder.vacancies, function (item)
            {
                expect(item).to.have.property('publishViaEmail', true);
            });
        });
    }

    function categoryTest(holder, category)
    {
        it('should return results from ' + category + ' category', function ()
        {
            _.forEach(holder.vacancies, function (item)
            {
                var categories = _.pluck(item.categories, '_id').map(function (item)
                {
                    return item.toString();
                });
                expect(categories).to.include(category);
            });
        });
    }

    function cityTest(holder, city)
    {
        it('should return results from ' + city + ' city', function ()
        {
            _.forEach(holder.vacancies, function (item)
            {
                expect(item).to.have.deep.property('cityId._id');
                expect(item.cityId._id.toString()).to.equal(city);
            });
        });
    }

    describe('getVacanciesToDispatch', function ()
    {
        var vacanciesManager = require('../app/business/vacancies.manager.js').create(null);
        var holder = {};

        before(function ()
        {
            dateHelper.set(1439532000000);
            return seeder().dropCollectionsAndSeed(require('./fixtures/vacancy.getVacanciesToDispatch.json'));
        });

        describe('for subscription without city', function ()
        {
            before(function ()
            {
                var subscription = {
                    categories: [
                        {id: 'aaaaaaaaaaaaaaaaaaaaaaaa'},
                        {id: 'bbbbbbbbbbbbbbbbbbbbbbbb'},
                        {id: '55d7bf396048db400d53ca6d'}
                    ]
                };
                return vacanciesManager.getVacanciesToDispatch(subscription).then(function (result)
                {
                    holder.vacancies = result;
                });
            });

            it('should return 2 results', function ()
            {
                expect(holder.vacancies).to.have.length(2);
            });

            standardTests(holder);
        });

        describe('for subscription with category 55d7bf396048db400d53ca6d,dddddddddddddddddddddddd', function ()
        {
            before(function ()
            {
                return vacanciesManager.getVacanciesToDispatch({
                    categories: [
                        {id: '55d7bf396048db400d53ca6d'},
                        {id: 'dddddddddddddddddddddddd'}
                    ]
                }).then(function (result)
                {
                    holder.vacancies = result;
                });
            });

            it('should return 1 result', function ()
            {
                expect(holder.vacancies).to.have.length(1);
            });

            categoryTest(holder, '55d7bf396048db400d53ca6d');
            standardTests(holder);
        });

        describe('for subscription with category aaaaaaaaaaaaaaaaaaaaaaaa', function ()
        {
            before(function ()
            {
                return vacanciesManager.getVacanciesToDispatch({
                    categories: [
                        {id: 'aaaaaaaaaaaaaaaaaaaaaaaa'}
                    ]
                }).then(function (result)
                {
                    holder.vacancies = result;
                });
            });

            it('should return 1 result', function ()
            {
                expect(holder.vacancies).to.have.length(1);
            });

            categoryTest(holder, 'aaaaaaaaaaaaaaaaaaaaaaaa');
            standardTests(holder);
        });

        describe('for subscription with category aaaaaaaaaaaaaaaaaaaaaaaa and city 67d7bf396048db400d53ca6d', function ()
        {
            before(function ()
            {
                return vacanciesManager.getVacanciesToDispatch({
                    categories: [
                        {id: 'aaaaaaaaaaaaaaaaaaaaaaaa'}
                    ],
                    city: {id: '67d7bf396048db400d53ca6d'}
                }).then(function (result)
                {
                    holder.vacancies = result;
                });
            });

            it('should return 1 result', function ()
            {
                expect(holder.vacancies).to.have.length(1);
            });

            categoryTest(holder, 'aaaaaaaaaaaaaaaaaaaaaaaa');
            cityTest(holder, '67d7bf396048db400d53ca6d');
            standardTests(holder);
        });

        describe('for subscription with category aaaaaaaaaaaaaaaaaaaaaaaa and city 51d7bf396048db400d53ca6d', function ()
        {
            before(function ()
            {
                return vacanciesManager.getVacanciesToDispatch({
                    categories: [
                        {id: 'aaaaaaaaaaaaaaaaaaaaaaaa'}
                    ],
                    city: {id: '51d7bf396048db400d53ca6d'}
                }).then(function (result)
                {
                    holder.vacancies = result;
                });
            });

            it('should return 0 results', function ()
            {
                expect(holder.vacancies).to.have.length(0);
            });
        });
    });

    describe('dispatchVacancies', function ()
    {

        var sparkPostMock = {dispatchVacancies: sinon.spy()};
        var business = require('../app/business/business.container.js');
        var vacanciesManager = proxyquire('../app/business/vacancies.manager.js', {'../service/sparkPost': sparkPostMock}).create({user: {role: 'admin'}},
                business);

        before(function ()
        {
            dateHelper.set(1439532000000);
            return seeder().dropCollectionsAndSeed(require('./fixtures/vacancy.dispatchVacancies.json')).then(function ()
            {
                return vacanciesManager.dispatchVacancies();
            });
        });

        it('should send vacancies to subscriber A', function ()
        {
            var expectedSubscription = {email: 'abc@example.com'};
            var expectedVacancies = [
                {
                    title: 'Everything ok #1'
                }
            ];
            var expectedCategories = [
                {name: {en: 'Cooking'}}
            ];

            expect(sparkPostMock.dispatchVacancies).to.have.been.calledWith(sinon.match(expectedSubscription), arrayMatcher(expectedVacancies),
                    arrayMatcher(expectedCategories));
        });

        it('should send vacancies to subscriber B', function ()
        {
            var expectedSubscription = {email: 'def@example.com'};
            var expectedVacancies = [
                {
                    title: 'Everything ok #2'
                }
            ];
            var expectedCategories = [
                {name: {en: 'Accounting'}}
            ];

            expect(sparkPostMock.dispatchVacancies).to.have.been.calledWith(sinon.match(expectedSubscription), arrayMatcher(expectedVacancies),
                    arrayMatcher(expectedCategories));
        });

        it('should send vacancies to subscriber C', function ()
        {
            var expectedSubscription = {email: 'ghi@example.com'};
            var expectedVacancies = [
                {
                    title: 'Everything ok #1'
                },
                {
                    title: 'Everything ok #2'
                }
            ];
            var expectedCategories = [
                {name: {en: 'Cooking'}},
                {name: {en: 'Accounting'}}
            ];

            expect(sparkPostMock.dispatchVacancies).to.have.been.calledWith(sinon.match(expectedSubscription), arrayMatcher(expectedVacancies),
                    arrayMatcher(expectedCategories));
        });

        describe('on subsequent dispatch when no new matching vacancies has been added', function ()
        {
            before(function ()
            {
                sparkPostMock.dispatchVacancies.reset();
                return vacanciesManager.dispatchVacancies();
            });

            it('should not send any subscriptions', function ()
            {
                expect(sparkPostMock.dispatchVacancies).to.have.been.callCount(0);
            });
        });

        describe('when new subscription is added', function ()
        {
            beforeEach(function ()
            {
                var subscriptionsDAO = require('../app/DAO/subscriptionsDAO');
                var subscription = {
                    email: 'aaa@example.com',
                    categories: ['aaaaaaaaaaaaaaaaaaaaaaaa'],
                    city: '67d7bf396048db400d53ca6d',
                    verified: true,
                    interval: 'daily',
                    subscribed: true,
                    desiredSalaryRange: '51eae100c2e6b6c222ec3433'
                };
                return subscriptionsDAO.createNewOrUpdate(subscription);
            });
            describe('on subsequent dispatch', function ()
            {
                beforeEach(function ()
                {
                    sparkPostMock.dispatchVacancies.reset();
                    return vacanciesManager.dispatchVacancies();
                });
                it('should send one subscription', function ()
                {
                    expect(sparkPostMock.dispatchVacancies).to.have.been.callCount(1);
                });
            });
        });
    });

    describe('calculateMatchingSubscriptions', function ()
    {
        var vacanciesManager = require('../app/business/vacancies.manager.js').create(null);

        beforeEach(function ()
        {
            return seeder().dropCollectionsAndSeed(require('./fixtures/vacancy.calculateMatchingSubscriptions.json'));
        });

        it('should set matchingSubscriptions to 2 for bbbbbbbbbbbbbbbbbbbbbbbb', function ()
        {
            return vacanciesManager.get('bbbbbbbbbbbbbbbbbbbbbbbb').then(function (result)
            {
                expect(result).to.have.property('matchingSubscriptions', 2);
            });
        });

        it('should set matchingSubscriptions to 1 for cccccccccccccccccccccccc', function ()
        {
            return vacanciesManager.get('cccccccccccccccccccccccc').then(function (result)
            {
                expect(result).to.have.property('matchingSubscriptions', 1);
            });
        });
    });

    describe('sendReminderVacancy', function ()
    {
        var sparkPostMock = {sendReminderToOldVacancy: sinon.spy()};
        var vacanciesManager = proxyquire('../app/business/vacancies.manager.js', {'../service/sparkPost': sparkPostMock}).create();
        before(function ()
        {
            return seeder().dropCollectionsAndSeed(require('./fixtures/sendReminderVacancy.json'));
        });
        describe('when there are 2 vacancies about to expire', function ()
        {
            beforeEach(function ()
            {
                dateHelper.set(1439873162468);
                return vacanciesManager.sendReminderVacancy();
            });
            it('should send email to employer', function ()
            {
                expect(sparkPostMock.sendReminderToOldVacancy.callCount).to.equal(2);
            });
        });
        describe('when no vacancy is about to expire',function(){
            beforeEach(function ()
            {
                sparkPostMock.sendReminderToOldVacancy.reset();
                dateHelper.set(1439993162468);
                return vacanciesManager.sendReminderVacancy();
            });
            it('should send email to employer', function ()
            {
                expect(sparkPostMock.sendReminderToOldVacancy.callCount).to.equal(0);
            });
        });
    });
});
