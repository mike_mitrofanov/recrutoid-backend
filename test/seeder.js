'use strict';

var _ = require('lodash');
var Promise = require('bluebird');
var mongoose = require('mongoose');
var objectId = require('mongodb').ObjectId;

module.exports = function ()
{

    function getConnection()
    {
        return mongoose.connections[0];
    }

    function drop(collectionName)
    {
        return new Promise(function (resolve)
        {
            var collection = getConnection().collections[collectionName];
            if (collection) {
                collection.drop(function ()
                {
                    resolve();
                });
            } else {
                resolve();
            }
        });
    }

    /**
     * Drop collections whose names are specified in vararg manner.
     *
     * @returns {*} array of collection names
     */
    function dropCollections()
    {
        var collectionNames = 0 === arguments.length ? _.keys(getConnection().db.collections) : arguments;
        var promises = _.map(collectionNames, function (collectionName)
        {
            return drop(collectionName);
        });
        return Promise.all(promises).then(function ()
        {
            return collectionNames;
        });
    }

    function dropCollectionsAndSeed()
    {
        var fixtures = arguments;
        var collectionNames = _.reduce(arguments, function (acc, fixture)
        {
            return _.merge(acc, _.keys(fixture));
        }, []);
        collectionNames = _.uniq(collectionNames);
        return dropCollections.apply(null, collectionNames).then(function ()
        {
            var promises = [];
            _.forEach(fixtures, function (fixture)
            {
                _.forEach(fixture, function (data, collectionName)
                {
                    if (!data.length) {
                        return;
                    }
                    var schema = mongoose.model(collectionName).schema;
                    promises.push(new Promise(function (resolve, reject)
                    {
                        data = _.cloneDeep(data);
                        _.forEach(data, function (row)
                        {
                            _.forEach(row, function (propertyValue, propertyName)
                            {
                                var path = schema.path(propertyName);
                                if (path && 'ObjectID' === path.instance) {
                                    row[propertyName] = objectId(propertyValue);
                                } else if (path && 'Array' === path.instance && path.caster && 'ObjectID' === path.caster.instance) {
                                    row[propertyName] = _.map(propertyValue, function (item)
                                    {
                                        return objectId(item);
                                    });
                                }
                            });
                        });
                        getConnection().collections[collectionName].insert(data, function (err)
                        {
                            if (err) {
                                reject(err);
                            } else {
                                resolve();
                            }
                        });
                    }));
                });
            });
            return Promise.all(promises);
        });
    }

    return {
        dropCollections: dropCollections,
        dropCollectionsAndSeed: dropCollectionsAndSeed
    };
};
