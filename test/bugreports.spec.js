'use strict';

var expect = require('chai').expect;
var request = require('supertest-as-promised');
var sinon = require('sinon');
var sinonChai = require('sinon-chai');
var proxyquire = require('proxyquire');
require('chai').use(sinonChai);


var seeder = require('./seeder.js');

describe('Bugreports endpoint', function ()
{
    describe('GET /api/bugerports', function ()
    {
        before(function ()
        {
            return seeder().dropCollectionsAndSeed(require('./fixtures/bugreports.search.json'));
        });

        describe('when user is admin', function ()
        {
            var authHeader;
            beforeEach(function ()
            {
                var credentials = {email: 'admin@example.com', password: 'password'};
                return request(this.baseUrl).post('/api/users/auth').send(credentials).expect(200).then(function (response)
                {
                    authHeader = 'Token ' + new Buffer(response.body.token).toString('base64');
                });
            });
            it('should respond with paginated bugreports list', function ()
            {
                return request(this.baseUrl).get('/api/bugreports').set('Authorization', authHeader).expect(200).expect(function (res)
                {
                    expect(res.body.results).to.have.length(3);
                    expect(res.body.total).to.equal(3);
                });
            });
        });

        describe('when user is authenticated as employer', function ()
        {
            var authHeader;
            beforeEach(function ()
            {
                var credentials = {email: 'employer@example.com', password: 'password'};
                return request(this.baseUrl).post('/api/users/auth').send(credentials).expect(200).then(function (response)
                {
                    authHeader = 'Token ' + new Buffer(response.body.token).toString('base64');
                });
            });
            it('should respond with 403', function ()
            {
                return request(this.baseUrl).get('/api/bugreports').set('Authorization', authHeader).expect(403);
            });
        });

        describe('when user is NOT authenticated as employer or admin', function ()
        {
            var authHeader;
            beforeEach(function ()
            {
                var credentials = {email: 'bernard.labno@example.com', password: 'password'};
                return request(this.baseUrl).post('/api/users/auth').send(credentials).expect(200).then(function (response)
                {
                    authHeader = 'Token ' + new Buffer(response.body.token).toString('base64');
                });
            });
            it('should respond with 403', function ()
            {
                return request(this.baseUrl).get('/api/bugreports').set('Authorization', authHeader).expect(403);
            });
        });

        describe('when user is NOT authenticated', function ()
        {
            it('should respond with 401', function ()
            {
                return request(this.baseUrl).get('/api/bugreports').expect(401);
            });
        });
    });
    describe('POST bugreports', function ()
    {
        var bugreport;
        beforeEach(function ()
        {
            bugreport = {
                email: 'bernard.labno@yahoo.com',
                message: 'this is new bugreport'
            };
        });

        describe('when user is NOT authenticated', function ()
        {
            it('should respond with 200', function ()
            {
                return request(this.baseUrl).post('/api/bugreports').send(bugreport).expect(200);
            });
        });
    });
});
