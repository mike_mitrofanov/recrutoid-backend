(function ()
{
    'use strict';

    module.exports = {
        url: process.env.MONGOLAB_URI || 'mongodb://localhost/recrutoid',
        frontendBaseUrl: process.env.FRONTEND || 'http://localhost:9000',
        backendBaseUrl: process.env.BACKEND || 'http://localhost:3000',
        backendEmailStatic: process.env.EMAIL_STATIC || 'http://localhost:3000/email-static/',
        sendgrid: {
            apiUser: process.env.SENDGRID_USER,
            apiKey: process.env.SENDGRID_KEY,
            addresses: {
                noReply: 'noreply@recrutoid.com',
                jobOffer: 'job@recrutoid.kz'
            }
        },
        sparkpost: {
            debug: false,
            addresses: {
                noReply: 'noreply@recrutoid.kz',
                jobOffer: 'job@recrutoid.kz',
                info: 'info@recrutoid.kz'
            }
        },
        solr: {
          bypass: process.env.SOLR_BYPASS || false,
          host: process.env.SOLR_HOST || '127.0.0.1',
          port: process.env.SOLR_PORT || '8983',
          core: process.env.SOLR_CORE || 'recrutoid',
          secure: process.env.SOLR_SECURE || false
        },
        pdfApiKey: process.env.PDF_API_KEY,
        FACEBOOK_SECRET: process.env.FACEBOOK_SECRET || 'dd9223daae351501801dab09ca86b9ad',
        LINKEDIN_SECRET: process.env.LINKEDIN_SECRET || 'yaq1PJgV8UwZkndp'
    };
})();
