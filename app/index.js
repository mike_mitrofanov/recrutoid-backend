(function ()
{
    'use strict';

    var app = require('./app.js');

    app(process.env.PORT || 3000);
})();
