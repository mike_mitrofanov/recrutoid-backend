(function ()
{
    'use strict';
    var Promise = require('bluebird');
    var config = require('../config/config.js');
    var sendgrid = require('sendgrid')(config.sendgrid.apiUser, config.sendgrid.apiKey);
    var sendGridPromise = Promise.promisify(sendgrid.send, sendgrid);
    var jade = require('jade');

    function createTokenAndSendEmail(token, email, type, vacancyObject, password)
    {
        if('vacancy' === type && password) {
            return sendActivationEmail(token, email, password, vacancyObject.publishViaEmail);
        }

        //resolve problem when is not given
        vacancyObject = vacancyObject || {};
        if (vacancyObject.categories) {
            vacancyObject.categories = vacancyObject.categories.map(function (category)
            {
                return category.name.en;
            });
        }
        var compiler = jade.compileFile(__dirname + '/../emailTemplates/veryfying-' + type + '.jade',
                {pretty: true, compileDebug: true});
        var html = compiler({
            context: config.frontendBaseUrl,
            token: token,
            vacancy: {
                details: vacancyObject.details,
                categories: vacancyObject.categories,
                companyName: vacancyObject.companyName,
                title: vacancyObject.title
            },
            resourcesBase: (config.backendBaseUrl) + '/email-static/'
        });
        var subject = '';
        if (vacancyObject.title) {
            subject = 'Confirm vacancy: ' + vacancyObject.title + ' at ' + vacancyObject.companyName;
        } else {
            subject = 'Recrutoid - Confirm your ' + type;
        }
        var emailPayload = {
            to: email,
            from: config.sendgrid.addresses.jobOffer,
            fromname: 'Recrutoid Kazakhstan',
            subject: subject,
            html: html
        };
        return sendGridPromise(emailPayload);
    }

    function approveVacancy(vacancyObject)
    {
        var compiler = jade.compileFile(__dirname + '/../emailTemplates/approve-vacancy.jade', {pretty: true});
        var html = compiler({
            context: config.frontendBaseUrl,
            vacancyId: vacancyObject.id,
            details: vacancyObject.details,
            categories: vacancyObject.categories,
            companyName: vacancyObject.companyName,
            title: vacancyObject.title,
            resourcesBase: (config.backendBaseUrl) + '/email-static/'
        });
        var emailPayload = {
            to: vacancyObject.email,
            from: config.sendgrid.addresses.noReply,
            fromname: 'Recrutoid Kazakhstan',
            subject: 'Your vacancy "' + vacancyObject.title + '" has been approved',
            html: html
        };
        return sendGridPromise(emailPayload);
    }

    function dispatchVacancies(subscription, vacancies, categories, language)
    {
        language = language || 'en';
        var htmlOut, emailPayload, promises = [];
        var compile = jade.compileFile(__dirname + '/../emailTemplates/job-offers.tpl.jade', {pretty: true});

        function sendWeeklyVacancies()
        {
            vacancies.map(function (vacancy)
            {
                vacancy.categories = vacancy.categories.map(function (category)
                {
                    return category.name[language];
                }).join(',');
            });
            htmlOut = compile({
                title: 'Recrutoid Job ' + subscription.interval + ' offers',
                categories: categories,
                vacancies: vacancies,
                daily: false,
                weekly: true,
                city: !!subscription.city ? subscription.city.en : null,
                context: (config.frontendBaseUrl) + '/subscription/edit/' + subscription.id,
                resourcesBase: (config.backendBaseUrl) + '/email-static/'
            });
            emailPayload = {
                to: subscription.email,
                from: config.sendgrid.addresses.jobOffer,
                subject: 'Your weekly vacancies matches',
                fromname: 'Recrutoid Kazakhstan',
                html: htmlOut
            };
            promises.push(sendGridPromise(emailPayload));
        }

        function sendDailyVacancy()
        {
            for (var i = 0; i < vacancies.length; i++) {
                vacancies[i].categories = vacancies[i].categories.map(function (category)
                {
                    return category.name[language];
                });

                var emailTitle = '[Recrutoid][' + vacancies[i].cityId.name +
                        (vacancies[i].salary.value ? ('][' + vacancies[i].salary.value + ' ' + vacancies[i].salary.currency) : '') + '] ' + vacancies[i].title;
                var fromname = vacancies[i].email.split('@')[0];

                htmlOut = compile({
                    title: emailTitle,
                    vacancy: vacancies[i],
                    categories: categories,
                    city: subscription.city ? {name: subscription.city.name[language]} : {name: 'All cities'},
                    daily: true,
                    weekly: false,
                    context: (config.frontendBaseUrl) + '/subscription/edit/' + subscription.id,
                    resourcesBase: (config.backendBaseUrl) + '/email-static/'
                });
                emailPayload = {
                    to: subscription.email,
                    replyto: vacancies[i].email,
                    from: config.sendgrid.addresses.jobOffer,
                    subject: emailTitle,
                    fromname: fromname + ' via ' + config.sendgrid.addresses.jobOffer,
                    html: htmlOut
                };
                promises.push(sendGridPromise(emailPayload));
            }
        }

        if ('daily' === subscription.interval) {
            sendDailyVacancy();
        } else {
            sendWeeklyVacancies();
        }
        return Promise.all(promises);
    }

    function sendActivationEmail(token, email, password, fromVacancy)
    {
        var compile = jade.compileFile(__dirname + '/../emailTemplates/activate.account.jade', {pretty: true});
        var htmlOut = compile({
            context: (config.frontendBaseUrl) + '/activate/' + token,
            credentials: {
                email: email,
                password: password
            },
            fromVacancy: fromVacancy || false,
            resourcesBase: (config.backendBaseUrl) + '/email-static/'
        });
        var emailPayload = {
            to: email,
            from: config.sendgrid.addresses.jobOffer,
            subject: 'Activate account',
            fromname: 'Recrutoid Kazakhstan',
            html: htmlOut
        };
        return sendGridPromise(emailPayload);
    }

    function passwordReset(userEmail, token)
    {
        var compile = jade.compileFile(__dirname + '/../emailTemplates/resetPassword.tpl.jade', {pretty: true});
        var htmlOut = compile({
            context: config.frontendBaseUrl + '/reset-password/' + token,
            resourcesBase: (config.backendBaseUrl) + '/email-static/'
        });
        var emailPayload = {
            to: userEmail,
            from: config.sendgrid.addresses.jobOffer,
            subject: 'Reset password',
            fromname: 'Recrutoid Kazakhstan',
            html: htmlOut
        };
        return sendGridPromise(emailPayload);
    }

    function sendReminderToOldVacancy(vacancy, token)
    {
        var compile = jade.compileFile(__dirname + '/../emailTemplates/remindVacancy.tpl.jade', {pretty: true});
        //TODO write view to extend vacancy.
        var htmlOut = compile({
            context: config.frontendBaseUrl + '/vacancy/extend/' + token,
            resourcesBase: (config.backendBaseUrl) + '/email-static/'
        });
        var emailPayload = {
            to: vacancy.email,
            from: config.sendgrid.addresses.noReply,
            subject: 'Your vacancy expires in 2 days',
            fromname: 'Recrutoid Kazakhstan',
            html: htmlOut
        };
        return sendGridPromise(emailPayload);
    }

    function sendReminderToOldProfile(user, token, timeToExpire)
    {
        var compile = jade.compileFile(__dirname + '/../emailTemplates/remindProfile.tpl.jade', {pretty: true});
        var htmlOut = compile({
            context:config.frontendBaseUrl+'/user/extend-profile/' + token,
            resourcesBase: (config.backendBaseUrl) + '/email-static/'
        });
        var emailPayload = {
            to: user.email,
            from: config.sendgrid.addresses.noReply,
            subject: 'Your profile expires in ' + timeToExpire + ' days',
            fromname: 'Recrutoid Kazakhstan',
            html: htmlOut
        };
        return sendGridPromise(emailPayload);
    }

    function applyNotification(vacancy, applier)
    {
        var compile = jade.compileFile(__dirname + '/../emailTemplates/apply-notification.tpl.jade', {pretty: true});
        var htmlOut = compile({
            vacancy: vacancy,
            applier: applier,
            link: config.frontendBaseUrl + '/user/settings/myVacancies?appliers=' + vacancy.id,
            resourcesBase: (config.backendBaseUrl) + '/email-static/'
        });
        var emailPayload = {
            to: vacancy.email,
            from: config.sendgrid.addresses.noReply,
            subject: 'Someone applied for your vacancy',
            fromname: 'Recrutoid Kazakhstan',
            html: htmlOut
        };
        return sendGridPromise(emailPayload);
    }

    function changeEmail(token, newEmail)
    {
        var compile = jade.compileFile(__dirname + '/../emailTemplates/change-email.tpl.jade', {pretty: true});
        var htmlOut = compile({
            link: config.frontendBaseUrl + '/user/confirm-email/' + token,
            resourcesBase: (config.backendBaseUrl) + '/email-static/'
        });
        var emailPayload = {
            to: newEmail,
            from: config.sendgrid.addresses.noReply,
            subject: 'Confirm your new email address',
            fromname: 'Recrutoid Kazakhstan',
            html: htmlOut
        };
        return sendGridPromise(emailPayload);
    }

    function customMessage(emails, emailTitle, emailContent)
    {
        var compile = jade.compileFile(__dirname + '/../emailTemplates/custom-message.tpl.jade', {pretty: true});
        var htmlOut = compile({
            emailTitle: emailTitle,
            emailContent: emailContent,
            resourcesBase: (config.backendBaseUrl) + '/email-static/'
        });
        var emailPayload = {
            bcc: emails,
            to: config.sendgrid.addresses.noReply,
            from: config.sendgrid.addresses.noReply,
            subject: emailTitle,
            fromname: 'Recrutoid Kazakhstan',
            html: htmlOut
        };
        return sendGridPromise(emailPayload);
    }

    function callForInterview(email, vacancyId)
    {
        var compile = jade.compileFile(__dirname + '/../emailTemplates/call-for-interview.tpl.jade', {pretty: true});
        var htmlOut = compile({
            link: config.frontendBaseUrl + '/vacancy/' + vacancyId,
            resourcesBase: (config.backendBaseUrl) + '/email-static/'
        });
        var emailPayload = {
            to: email,
            from: config.sendgrid.addresses.noReply,
            subject: 'You have just received offer',
            fromname: 'Recrutoid Kazakhstan',
            html: htmlOut
        };
        return sendGridPromise(emailPayload);
    }

    function forwardCv(email, user, pdf)
    {
        var name = (user.firstName || '') + ' ' + (user.lastName || '');
        var compile = jade.compileFile(__dirname + '/../emailTemplates/forward-cv.tpl.jade', {pretty: true});
        var htmlOut = compile({
            name: name,
            resourcesBase: (config.backendBaseUrl) + '/email-static/'
        });
        var emailPayload = {
            to: email,
            from: user.email,
            subject: 'CV - ' + user.resume.title,
            fromname: name,
            html: htmlOut,
            files: [
                {
                    filename: 'CV.pdf',
                    content: pdf
                }
            ]
        };
        return sendGridPromise(emailPayload);
    }

    module.exports = {
        sendReminderToOldProfile: sendReminderToOldProfile,
        approveVacancy: approveVacancy,
        sendReminderToOldVacancy: sendReminderToOldVacancy,
        createTokenAndSendEmail: createTokenAndSendEmail,
        dispatchVacancies: dispatchVacancies,
        sendActivationEmail: sendActivationEmail,
        passwordReset: passwordReset,
        applyNotification: applyNotification,
        changeEmail: changeEmail,
        customMessage: customMessage,
        callForInterview: callForInterview,
        forwardCv: forwardCv
    };
})
();
