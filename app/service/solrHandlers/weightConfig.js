(function ()
{
    'use strict';

    var config = {
        vacancy: {
            title: {
                wildcard: 4,
                field: 5,
                exact: 6,
                phonetic: 3,
                phoneticWildcard: 2,
                noSpaces: 4,
                phrase: 0.02,
                phraseExact: 1,
                companyField: 2,
                companyExact: 3,
                companyWildcard: 2,
                details: 0.01

            }
        },
        application: {
            title: {
                wildcard: 8,
                field: 9,
                exact: 100,
                phonetic: 7,
                phoneticWildcard: 6,
                noSpaces: 4,
                phrase: 0.2,
                phraseExact: 0.5
            },
            workTitle: {
                wildcard: 1,
                field: 1,
                exact: 6,
                phonetic: 3,
                phoneticWildcard: 2,
                phrase: 0.2,
                phraseExact: 0.5
            },
            info: {
                field: 6
            },
            company: {
                field: 6,
                exact: 8,
                wildcard: 2
            }
        }
    };

    var currentHandler, currentField;

    function getConfig(handler, field) {
        return config[handler][field];
    }


    function get(type, noString)
    {
        var w = getConfig(currentHandler, currentField);

        w = w[type];

        if (!noString) {
            w = '^' + w;
        }

        return w;
    }

    function setCurrent(cH, cF)
    {
        currentHandler = cH;
        currentField = cF;
    }

    function setCurrentField(cF)
    {
        currentField = cF;
    }

    function setCurrentHandler(cH)
    {
        currentHandler = cH;
    }

    module.exports = {
        getConfig: getConfig,
        setCurrent: setCurrent,
        setCurrentField: setCurrentField,
        setCurrentHandler: setCurrentHandler,
        get: get
    };

})();