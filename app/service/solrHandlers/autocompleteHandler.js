(function ()
{
    'use strict';
    var Promise = require('bluebird');
    var common = require('./common');
    var vacancyHandler = require('./vacancyHandler');
    var applicationHandler = require('./applicationHandler');

    function get(client) {


        function alphaSort(v1, v2)
        {
            if (v1 > v2) {
                return 1;
            } else if (v2 > v1) {
                return -1;
            } else {
                return 0;
            }
        }

        function processGrouped(obj, field)
        {
            
            var data = [];
            var docs = obj.grouped[field].groups;

            for (var i in docs) {
                data.push(docs[i].doclist.docs[0][field]);
            }

            return data;
        }

        function processVacancyResponse(obj, fieldName)
        {
            return processGrouped(obj, 'vacancyTitleExact');
        }


        function processCompanyResponse(obj, fieldName)
        {
            return processGrouped(obj, 'vacancyCompanyNameExact');
        }


        function vacancyQuery(text)
        {
            var query = client.createQuery()
                    .edismax();

            client.setSuggestHandler('suggest_vacancies');

            var q = '';

            q = 'vacancyTitleNgram:(' + text + ')';// vacancyCompanyNameNgram:('+text+')';

            query.q(q);
            query.parameters.push('group.ngroups=true&group=true&group.field=vacancyTitleExact');

            var filterQuery = vacancyHandler.get(client).filterQueryStr();
            query.parameters.push('fq=' + encodeURIComponent(filterQuery));

            common.get(client).processFL(query, 'vacancyTitleExact');

            var sort = {score: 'desc'};

            query.sort(sort);
            query.rows(10);

            return common.get(client).commenceQuery(query, null, processVacancyResponse, 'suggest');
        }

        function companyQuery(text)
        {

            var query = client.createQuery()
                    .edismax();

            client.setSuggestHandler('suggest_vacancies');

            common.get(client).processFL(query, 'vacancyCompanyNameExact');

            var q = '';

            q = 'vacancyCompanyNameNgram:(' + text + ')';


            query.q(q);
            query.parameters.push('group.ngroups=true&group=true&group.field=vacancyCompanyNameExact');

            var filterQuery = vacancyHandler.get(client).filterQueryStr();
            query.parameters.push('fq=' + encodeURIComponent(filterQuery));

            query.sort({score: 'desc'});
            query.rows(10);

            return common.get(client).commenceQuery(query, null, processCompanyResponse, 'suggest');
        }

        function vacancy(text)
        {

            return new Promise(function (resolve, reject)
            {

                var finalData = [];

                var vacPromise = vacancyQuery(text);
                var cmpPromise = companyQuery(text);

                vacPromise.then(function (data)
                {
                    finalData = finalData.concat(data);
                });

                cmpPromise.then(function (data)
                {
                    finalData = finalData.concat(data);
                });

                Promise.all([vacPromise, cmpPromise]).then(function () {
                    finalData.sort(alphaSort);

                    finalData = finalData.slice(0, 10);

                    resolve(finalData);
                });
            });

        }

        function processworkCompany(obj, field, text)
        {

            var data = [];
            var docs = obj.grouped.workCompanyNameExact.groups;

            for (var i in docs) {

                for (var n in docs[i].doclist.docs[0].workCompanyNameExact) {
                    var item = docs[i].doclist.docs[0].workCompanyNameExact[n];
                    if (item.toLowerCase().includes(text.toLowerCase())) {
                        data.push(item);
                    }
                }

            }

            return data;
        }


        function workCompanyQuery(text)
        {
            var query = client.createQuery()
                    .edismax();

            client.setSuggestHandler('suggest_applications');

            common.get(client).processFL(query, 'workCompanyNameExact');

            var q = '';

            q = 'workCompanyNameNgram:(' + text + ')';

            query.q(q);

            query.parameters.push('group.ngroups=true&group=true&group.field=workCompanyNameExact&group.sort=score+desc');
            query.parameters.push('fq=' + encodeURIComponent(applicationHandler.get(client).filterQueryStr()));

            query.sort({score: 'desc'});
            query.rows(10);

            return common.get(client).commenceQuery(query, null, function (obj, fieldName) {

                return processworkCompany(obj, null, text);
            }, 'suggest');
        }

        function cvQuery(text)
        {
            var query = client.createQuery()
                    .edismax();

            client.setSuggestHandler('suggest_applications');

            common.get(client).processFL(query, 'resumeTitleExact');

            var q = '';

            q = 'resumeTitleNgram:(' + text + ')';

            query.q(q);
            query.parameters.push('group.ngroups=true&group=true&group.field=resumeTitleExact');

            query.parameters.push('fq=' + encodeURIComponent(applicationHandler.get(client).filterQueryStr()));

            query.sort({score: 'desc'});
            query.rows(10);

            return common.get(client).commenceQuery(query, null, function (obj, fieldName) {
                return processGrouped(obj, 'resumeTitleExact');
            }, 'suggest');

        }

        function application(text) {
            return new Promise(function (resolve, reject)
            {
                var finalData = [];

                var cvQ = cvQuery(text).then(function (data)
                {
                    finalData = finalData.concat(data);
                });


//                var wcQ = workCompanyQuery(text).then(function (data)
//                {
//                    finalData = finalData.concat(data);
//                });

                Promise.all([cvQ]).then(function ()
                {
                    finalData.sort(alphaSort);

                    finalData = finalData.slice(0, 10);

                    resolve(finalData);
                });
            });
        }


        

         function processCVCompanyResponse(obj, field)
        {
            var data = [];
            var docs = obj.grouped.workCompanyNameExact.groups;
            
            for (var i in docs) {
                data = data.concat(docs[i].doclist.docs[0].workCompanyNameExact);
            }

            return data;
        }

        function company(text)
        {

            return new Promise(function (resolve, reject) {
                var query = client.createQuery()
                        .edismax();

                client.setSuggestHandler('suggest_companies');
                common.get(client).processFL(query, 'workCompanyNameExact');

                var q = '';

                q = 'workCompanyNameNgram:(' + text + ')';

                query.q(q);
                query.parameters.push('group.ngroups=true&group=true&group.field=workCompanyNameExact');
                query.parameters.push('fq=' + encodeURIComponent(applicationHandler.get(client).filterQueryStr()));
                query.sort({score: 'desc'});
                query.rows(10);

                common.get(client).commenceQuery(query, null, function (obj, fieldName) {
                    return processCVCompanyResponse(obj);
                }, 'suggest').then(function (data) {
                      data.sort(alphaSort);
                      resolve(data);
                });
            });

        }

        return {
            vacancy: vacancy,
            application: application,
            company: company
        };
    }



    module.exports = {
        get: get
    };
})();