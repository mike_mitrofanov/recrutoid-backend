(function ()
{
    'use strict';

    var Promise = require('bluebird');
    var extend = require('node.extend');
    var dateHelper = require('../../service/dateHelper');

    function get(client) {

        function commit()
        {
            return new Promise(function (resolve, reject) {
                client.commit(function (err, res) {
                    if (err) {
                        reject(err);
                    }
                    if (res) {
                        resolve(res);
                    }
                });
            });
        }

        function purge()
        {
            return new Promise(function (resolve, reject) {
                client.deleteByQuery('*:*', function (err, obj) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(obj);
                    }
                });
            });

        }

        function push(item, doCommit) {
            return new Promise(function (resolve, reject) {
                client.add([item], function (err, obj) {

                    if (err) {
                        reject(err);
                    } else {
                        if (doCommit) {
                            commit().then(function () {
                                resolve(obj);
                            });
                        } else {
                            resolve(obj);
                        }
                    }
                });
            });
        }

        function drop(id, contentType)
        {

            var q='id:' + id+' AND contentType:'+contentType;

            //TODO: maybe run optimize afterwards
            client.deleteByQuery(q, function (err, obj) {
                if (err) {
                    console.log('Drop error', err);
                } else {
                    console.log('Drop ok', obj);
                    commit();
                }
            });
        }

        function processQueryResponse(obj, fieldName) {
            
            var response = obj.response;

            if (fieldName) {
                var newDocs = [];
                for (var i in response.docs) {
                    newDocs.push(response.docs[i][fieldName]);
                }
                response.docs = newDocs;
            }



            return response;
        }

        function commenceQuery(query, fieldName, resultProcessCallback, clientMethod) {
            if (!resultProcessCallback) {
                resultProcessCallback = processQueryResponse;
            }
            
            if(!clientMethod) {
                clientMethod = 'search';
            }

            return new Promise(function (resolve, reject) {
                client[clientMethod](query, function (err, obj) {
                    if (err) {
                        reject(err);
                    } else {
                        var response = resultProcessCallback(obj, fieldName);
                        resolve(response);
                    }
                });
            });
        }

        function processFL(query, fieldName) {
            if (fieldName instanceof Object || fieldName instanceof Array) {
                for (var k in fieldName) {
                    query.fl(fieldName[k]);
                }
            } else {
                if (fieldName) {
                    query.fl(fieldName);
                }
            }
            
            return query;
        }

        function processSettings(settings)
        {
            if (!settings) {
                settings = {};
            }


            var defaultSettings = {
                fieldName: null,
                groupBy: null,
                resultProcessCallback: null,
                phrases: null,
                facet: null
            };

            settings = extend(defaultSettings, settings);

            return settings;
        }

        function filterJsonCallback(obj, fieldName) {
             //console.log('JC', obj);
            var response = obj.response;


            var data = [];

            for (var i in response.docs) {

                data.push(JSON.parse(response.docs[i].dataJson));
            }

            return {data: data, count: obj.response.numFound, obj: obj};
        }

        function makeAndSubquery(queryString, subQuery, glue) {

            if(!glue) {
                glue = 'AND';
            }

            var prefix = '';
            if (queryString) {
                prefix = '(' + queryString + ') '+glue+' ';
            }
            queryString = prefix + subQuery;

            return queryString;

        }

        function makePhrasesQS(phrases, original, fieldNameExact, fieldName, weights) {
            var qs = "";

            var defaultWeights = {
                exact: '^1',
                field: '^0.02'
            };

            weights = extend(defaultWeights, weights);

            for (var i in phrases) {
                var p = phrases[i];

                if (p.toLowerCase() === original.toLowerCase() || original.toLowerCase().includes(p.toLowerCase())) {
                    continue;
                }

                qs +=
                        ' ' + fieldNameExact + ':(' + p + weights.exact + ')' +
                        ' ' + fieldName + ':(' + p + weights.field + ')'
                        ;
            }

            return qs;
        }

        function makeCommonQuery(queryString, params, dateField)
        {
            
            if(!dateField) {
                dateField = 'createDate';
            }
            

            if (params.days) {
                var dateDays = dateHelper.getMoment().subtract(2, 'days').set({
                    hour: 0,
                    minute: 0,
                    second: 0,
                    millisecond: 0
                }).valueOf();

                queryString = makeAndSubquery(queryString, dateField+':[' + dateDays + ' TO *]');
            }
            
            if (params.city) {
                queryString = makeAndSubquery(queryString, 'cityId:(' + params.city + ')');
            }
           // console.log(queryString);
            return queryString;
        }


        return {
            commit: commit,
            purge: purge,
            push: push,
            drop: drop,
            processQueryResponse: processQueryResponse,
            commenceQuery: commenceQuery,
            processFL: processFL,
            processSettings: processSettings,
            filterJsonCallback: filterJsonCallback,
            makeAndSubquery: makeAndSubquery,
            makePhrasesQS: makePhrasesQS,
            makeCommonQuery: makeCommonQuery
        };


    }

    module.exports = {
        get: get
    };
})();


