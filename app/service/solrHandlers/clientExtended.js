(function ()
{
    'use strict';

    var solr = require('solr-client');


    var extend = require('node.extend');


    function createClient(host, port, core, path, agent, secure, bigint, solrVersion) {
        var client = solr.createClient(host, port, core, path, agent, secure, bigint, solrVersion);
        client.SUGGEST_HANDLER = 'suggest_vacancies';
        client.setSuggestHandler = function (suggestHandler) {
            this.SUGGEST_HANDLER = suggestHandler;
        };
        client.suggest = function (query, callback) {
            return this.get(this.SUGGEST_HANDLER, query, callback);
        };


        return client;
    }

    module.exports = {
        createClient: createClient
    };

})();