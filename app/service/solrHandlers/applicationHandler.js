(function () {
    'use strict';

    var common = require('./common');
    var usersDAO = require('../../DAO/usersDAO');
    var languagesDAO = require('../../DAO/languagesDAO');
    var WC = require('./weightConfig');
    var sanitizeHtml = require('sanitize-html');

    function get(client) {

        function prepareJsonData(inputData)
        {
            var data = inputData;

            for (var i in data.resume.workExperience) {

                data.resume.workExperience[i].description =
                        sanitizeHtml(data.resume.workExperience[i].description, {
                            allowedTags: []
                        }).replace(/\n/g, " ");
            }

            var jsonData = JSON.stringify(data);

            return jsonData;
        }

        function push(application, doCommit, noUpdateDate) {


            /**
             * Assumption: No resume title = no CV
             */

            //TODO: update date is separate from extdate
            if (typeof application === 'string') {
                return usersDAO.model.findOne({_id: application}).then(function (res) {
                    return push(res, doCommit);
                });
            }

            if (!application.resume || !application.resume.title) {
                return new Promise(function (resolve, reject) {
                    resolve();
                });
            }


            return new Promise(function (resolve, reject) {
                var degrees = application.resume.education.map(function (item) {
                    return item.degree;
                });


                var languageNames = application.resume.languages.map(function (item) {
                    return item.name;
                });

                languagesDAO.getCodesByNames(languageNames).then(function (langs) {

                    var languages = application.resume.languages.map(function (item) {

                        return langs[item.name] + item.level;
                    });


                    var companyNames = [];

                    var workTitles = application.resume.workExperience.map(function (item) {
                        companyNames.push(item.company);
                        return item.title;
                    });

                    var userId;

                    if (application._id) {
                        userId = application._id;
                    } else {
                        userId = application.id;
                    }

                    var updateDate = application.extendDate;

                    if (!noUpdateDate) {
                        updateDate = (new Date()).getTime();
                    }

                    usersDAO.get(userId, {}, true).then(function (result) {

                        var jsonData = prepareJsonData(result);
                        
                        var pushedApplication = {
                            id: application.id,
                            verified: application.verified,
                            hidden: application.hidden,
                            hiddenOnSearch: application.hiddenOnSearch,
                            banned: result.banned,
                            createDate: application.createDate,
                            updateDate: updateDate,
                            resumeTitle: application.resume.title,
                            resumeTitleNoSpaces: application.resume.title.replace(/ /g, ''),
                            foreignDegree: application.resume.foreignDegree,
                            expat: application.resume.expat,
                            workExperience: parseInt(application.workExperience),
                            salaryRangeId: application.resume.desiredSalaryRange,
                            role: application.role,
                            categories: application.categories,
                            cityId: application.city,
                            degree: degrees,
                            language: languages,
                            workTitle: workTitles,
                            workCompanyName: companyNames,
                            additionalInfo: application.resume.additionalInfo,
                            contentType: 'application',
                            dataJson: jsonData
                        };

                        common.get(client).push(pushedApplication, doCommit).then(function () {
                            resolve();
                        });
                    });


                });
            });
        }

        function makeTitleQuery(params, settings)
        {
            var queryString = '';

            if (params.resumeTitle) {

                WC.setCurrent('application', 'title');

                queryString += 'resumeTitle:(*' + params.resumeTitle + '*' + WC.get('wildcard') + ') ' +
                        'resumeTitle:("' + params.resumeTitle + '"' + WC.get('field') + ') ' +
                        'resumeTitleExact:("' + params.resumeTitle + '"' + WC.get('exact') + ') ' +
                        'resumeTitlePhonetic:("' + params.resumeTitle + '"' + WC.get('phonetic') + ') ' +
                        'resumeTitlePhonetic:(*' + params.resumeTitle + '*' + WC.get('phoneticWildcard') + ') ' 
                        +'resumeTitleNoSpaces:("' + params.resumeTitle + '"' + WC.get('noSpaces') + ') ' 
                        ;


                if (settings.phrases) {
                    var phrasesQS = common.get(client).makePhrasesQS(
                            settings.phrases,
                            params.resumeTitle,
                            'resumeTitleExact',
                            'resumeTitle', {
                                exact: WC.get('phraseExact'),
                                field: WC.get('phrase')
                            });
                    queryString += phrasesQS;
                }


                WC.setCurrentField('workTitle');

                queryString +=
                        ' '
                        + 'workTitle:("' + params.resumeTitle + '"' + WC.get('field') + ') '
                        + 'workTitle:(*' + params.resumeTitle + '*' + WC.get('wildcard') + ') '
                        + 'workTitleExact:("' + params.resumeTitle + '"' + WC.get('exact') + ') '
                        + 'workTitlePhonetic:("' + params.resumeTitle + '"' + WC.get('phonetic') + ') '
                        + 'workTitlePhonetic:(*' + params.resumeTitle + '*' + WC.get('phoneticWildcard') + ') '
                        ;


                if (settings.phrases) {
                    var phrasesQS = common.get(client).makePhrasesQS(
                            settings.phrases,
                            params.resumeTitle,
                            'workTitleExact',
                            'workTitle', {
                                exact: WC.get('phraseExact'),
                                field: WC.get('phrase')
                            });
                    queryString += phrasesQS;
                }

                WC.setCurrentField('info');

                queryString +=
                        ' '
                        + 'additionalInfo:(' + params.resumeTitle + WC.get('field') + ') '
                        ;


            }

            return queryString;
        }


        function query(params, settings) {

            settings = common.get(client).processSettings(settings);

            var queryString = '';

            var query = client.createQuery()
                    .edismax()
                    .start(params.from || 0)
                    .rows(params.size || 20)
                    ;


            queryString += makeTitleQuery(params, settings);



            if (params.category) {
                var glue = 'AND';
                if (params.resumeTitle) {
                    glue = 'OR';
                }
                queryString = common.get(client).makeAndSubquery(queryString, 'categories:(' + params.category + ')', glue);
            }

            if (params.companyName) {
                WC.setCurrent('application', 'company');

                var companiesQS =
                        ' '
                        + 'workCompanyName:("' + params.companyName + '"' + WC.get('field') + ') '
                        + 'workCompanyNameExact:(' + params.companyName + WC.get('exact') + ') '
                        + 'workCompanyName:(*' + params.companyName + '*' + WC.get('wildcard') + ') '
                        ;

                queryString = common.get(client).makeAndSubquery(queryString, companiesQS);

            }

            if (params.workExperienceGTE) {
                queryString = common.get(client).makeAndSubquery(queryString, ' workExperience:[' + params.workExperienceGTE + ' TO *]');
            }

            if (params.workExperienceLTE) {
                queryString = common.get(client).makeAndSubquery(queryString, ' workExperience:[* TO ' + params.workExperienceLTE + ']');
            }

            if (params.expat) {
                queryString = common.get(client).makeAndSubquery(queryString, 'expat:' + params.expat);
            }

            if (params.foreignDegree) {
                queryString = common.get(client).makeAndSubquery(queryString, 'foreignDegree:' + params.foreignDegree);
            }

            if (params.education) {

                params.education = Array.isArray(params.education) ? params.education : [params.education];

                for (var i in params.education) {
                    var edu = params.education[i];

                    //queryString += ' degree:(' + edu + ') ';
                    queryString = common.get(client).makeAndSubquery(queryString, ' degree:(' + edu + ') ');
                }

            }

            if (settings.langcodes) {

                var lQS = "", lQ = "language:(";
                var prefix = "", lPrefix = "";
                for (var i in settings.langcodes) {
                    lQS = "";
                    prefix = "";
                    for (var c in settings.langcodes[i]) {
                        var code = settings.langcodes[i][c];
                        lQS += prefix + i + code;
                        prefix = " OR ";
                    }

                    lQ += lPrefix + '(' + lQS + ')';
                    lPrefix = ' AND ';
                }

                lQ += ")";

                queryString = common.get(client).makeAndSubquery(queryString, lQ);

            }


            queryString = common.get(client).makeCommonQuery(queryString, params, 'updateDate');

            if (queryString === '') {
                queryString = '*:*';
            }

            query.q(queryString);

            var filterQuery = filterQueryStr();

            query.parameters.push('fq=' + encodeURIComponent(filterQuery));

            common.get(client).processFL(query, settings.fieldName);

            query.sort({score: 'desc', updateDate: 'desc'});

            return common.get(client).commenceQuery(query, settings.fieldName, settings.resultProcessCallback);
        }

        function drop(id)
        {
            return common.get(client).drop(id, 'application');
        }

        function filterQueryStr() {
            var filterQuery = 'contentType:application AND hidden:false AND hiddenOnSearch:false' +
                    ' AND role:normal AND verified:true AND banned:false AND verified:true';
            return filterQuery;
        }

        return {
            push: push,
            query: query,
            drop: drop,
            filterQueryStr: filterQueryStr
        };
    }

    module.exports = {
        get: get
    };

})();