(function () {
    'use strict';

    var VacanciesDAO = require('../../DAO/vacanciesDAO');
    var UsersDAO = require('../../DAO/usersDAO');
    var solrHelper = require('../solrHelper');
    var Promise = require('bluebird');
    var mongoose = require('mongoose-bird')(require('mongoose'));

    var configDB = require('../../config/config.js');


    var https = require('https');
    var http = require('http');

    function limitHttp() {
        https.globalAgent.maxSockets = 5;
        http.globalAgent.maxSockets = 5;
    }

    function connectDB()
    {
        // mongoose.set('debug', true);
        mongoose.connect(configDB.url, function (error)
        {
            if (error) {
                console.error(error);
            }
        });
        // If the Node process ends, close the Mongoose connection
        process.on('SIGINT', function ()
        {
            mongoose.connection.close(function ()
            {
                console.error('Mongoose default connection disconnected through app termination');
                process.exit(0);
            });
        });
    }


    function syncVacancies(filter) {
        return solrHelper.vacancyHandler.sync(filter);
    }


    function syncApplications() {
        console.log('syncApplications');
        return new Promise(function (resolve, reject) {
            UsersDAO.model.find({}, function (err, result) {
                var count = result.length;
                var index = 0;

                for (var i in result) {
                    solrHelper.pushApplication(result[i], false, true).then(function () {
                        index++;
                       // console.log(index + '/' + count);
                        if (index === count) {
                            resolve(index);
                        }
                    });
                }

            });

        });
    }

    module.exports = {
        limitHttp: limitHttp,
        connectDB: connectDB,
        syncVacancies: syncVacancies,
        syncApplications: syncApplications
    };
})();
