(function () {
    'use strict';

    var sanitizeHtml = require('sanitize-html');
    var Promise = require('bluebird');

    var vacanciesDAO = require('../../DAO/vacanciesDAO');
    var WC = require('./weightConfig');

    require('../../DAO/categoriesDAO');
    require('../../DAO/citiesDAO');
    require('../../DAO/salaryRangesDAO');


    var common = require('./common.js');
    var extend = require('node.extend');

    function get(client) {


        function sync(filter) {
            if (!filter) {
                filter = {};
            }

            return new Promise(function (resolve, reject) {
                vacanciesDAO.model.find(filter, function (err, result) {


                    var count = result.length;
                    var index = 0;
                    for (var i in result) {

                        push(result[i]).then(function () {
                            index++;
                            console.log(index + '/' + count);
                            if (index === count) {
                                resolve(index);
                            }
                        });
                    }


                });
            });
        }

        function prepareJsonData(inputData)
        {

            var data = {};

            var categories = [], cities = [];

            for (var i in inputData.categories) {
                var inputCategory = inputData.categories[i];
                categories.push({name: inputCategory.name, _id: inputCategory.id});
            }

            data['categories'] = categories;
            data['cityId'] = {
                name: inputData.cityId.name,
                _id: inputData.cityId.id
            };

            data['salaryRangeId'] = {
                order: inputData.salaryRangeId.order,
                name: inputData.salaryRangeId.name,
                _id: inputData.salaryRangeId.id
            };

            var copyFields = [
                'title',
                'publishOnWebsite',
                'publishViaEmail',
                'approved',
                'banned',
                'publishedDate',
                "archived",
                "verified",
                "sentEmails",
                "createDate",
                "extendDate",
                "applicants",
                "userId",
                "details",
                "showEmailOnWebsite",
                "email",
                "companyName",
                "id",
                "matchingSubscriptions",
                "expDate"
            ];

            for (var i in copyFields) {
                data[copyFields[i]] = inputData[copyFields[i]];
            }

            var jsonData = JSON.stringify(data);

            return jsonData;

        }

        function pushByUser(user)
        {
            sync({userId: user.id}).then(function () {
                common.get(client).commit();
            });

        }

        function push(vacancy, doCommit)
        {
            if (typeof vacancy === 'string') {
                return vacanciesDAO.model.findOne({_id: vacancy}).then(function (res) {

                    new Promise(function (resolve, reject) {
                        push(res, doCommit).then(function () {
                            resolve();
                        });

                    });

                });
            }

            return new Promise(function (resolve, reject) {


                var details = sanitizeHtml(vacancy.details, {
                    allowedTags: []
                }).replace(/\n/g, " ");


                var vacancyId;

                if (vacancy.id) {
                    vacancyId = vacancy.id;
                } else {
                    vacancyId = vacancy._id;
                }


                vacanciesDAO.get(vacancyId).then(function (result) {

                    var dataJson = prepareJsonData(result);

                    var pushedVacancy = {
                        id: vacancyId,
                        vacancyTitle: vacancy.title,
                        vacancyTitleNoSpaces: vacancy.title.replace(/ /g, ''),
                        vacancyCompanyName: vacancy.companyName,
                        categories: vacancy.categories,
                        cityId: vacancy.cityId,
                        salaryRangeId: vacancy.salaryRangeId,
                        createDate: vacancy.createDate,
                        vacancyDetails: details,
                        contentType: 'vacancy',
                        publishViaEmail: vacancy.publishViaEmail,
                        publishOnWebsite: vacancy.publishOnWebsite,
                        verified: vacancy.verified,
                        approved: vacancy.approved,
                        banned: result.banned,
                        archived: vacancy.archived,
                        dataJson: dataJson
                    };

                    common.get(client).push(pushedVacancy, doCommit).then(function () {
                        resolve();
                    });
                }).catch(function (err) {
                    console.log('ERRX', err);
                    reject(err);
                });

            });

        }

        function query(params, settings)
        {

            settings = common.get(client).processSettings(settings);

            var query = client.createQuery()
                    .edismax();

            query.start(params.from || 0)
                    .rows(params.size || 20);

            var queryString = "";

            if (params.vacancyTitle) {

                WC.setCurrent('vacancy', 'title');

                queryString +=
                        'vacancyTitle:(*' + params.vacancyTitle + '*' + WC.get('wildcard') + ') ' +
                        'vacancyTitle:("' + params.vacancyTitle + '"'+ WC.get('field') + ') ' +
                        'vacancyTitleExact:("' + params.vacancyTitle + '"' + WC.get('exact') + ') ' +
                        'vacancyTitlePhonetic:("' + params.vacancyTitle + '"' + WC.get('phonetic') + ') ' +
                        'vacancyTitlePhonetic:(*' + params.vacancyTitle + '*' + WC.get('phoneticWildcard') + ') ' +
                        'vacancyTitleNoSpaces:("' + params.vacancyTitle + '"' + WC.get('noSpaces') + ') ' +
                        'vacancyCompanyName:(*' + params.vacancyTitle + '*' + WC.get('companyWildcard') + ') ' +
                        'vacancyCompanyName:(' + params.vacancyTitle + WC.get('companyField') + ') ' +
                        'vacancyCompanyNameExact:(' + params.vacancyTitle + WC.get('companyExact') + ') ' +
                        'vacancyDetails:(' + params.vacancyTitle + WC.get('details') + ')'
                        ;

                if (settings.phrases) {
                    var phrasesQS = common.get(client).makePhrasesQS(
                            settings.phrases,
                            params.vacancyTitle,
                            'vacancyTitleExact',
                            'vacancyTitle', {
                                exact: WC.get('phraseExact'),
                                field: WC.get('phrase')
                            });
                    queryString += phrasesQS;
                }
            }



            if (params.category) {

                if (!params.vacancyTitle) {
                    queryString = common.get(client).makeAndSubquery(queryString, 'categories:(' + params.category + ')');
                } else {
                    queryString = common.get(client).makeAndSubquery(queryString, 'categories:(' + params.category + ')', 'OR');
                }

            }

            if (params.companyName) {
                 queryString = common.get(client).makeAndSubquery(queryString, 'vacancyCompanyNameExact:("' + params.companyName+'")');
                
               // queryString = '(' + queryString + ')' + ' AND vacancyCompanyNameExact:("' + params.companyName + '")';
            }

            if (params.salaryRange) {
                var glue = 'AND';

                queryString = common.get(client).makeAndSubquery(queryString, "salaryRangeId:" + params.salaryRange, glue);

            }

            queryString = common.get(client).makeCommonQuery(queryString, params);

            if (queryString == "") {
                queryString = "*:*";
            }

            query.q(queryString);

            var filterQuery = filterQueryStr();

            query.parameters.push('fq=' + encodeURIComponent(filterQuery));

            if (settings.facet) {
                query.parameters.push('facet=true&facet.field=' + settings.facet);
            }

            if (settings.groupBy) {
                query.parameters.push('group.ngroups=true&group=true&group.field='
                        + encodeURIComponent(settings.groupBy));
            }

            common.get(client).processFL(query, settings.fieldName);

            query.sort({score: 'desc', createDate: 'desc'});

            return common.get(client).commenceQuery(query, settings.fieldName, settings.resultProcessCallback);

        }


        function filterQueryStr() {
            var filterQuery = '' +
                    'contentType:vacancy AND banned:false AND ' +
                    'verified:true AND approved:true AND archived:false AND ' +
                    'publishOnWebsite:true';

            return filterQuery;
        }

        function drop(id)
        {
            return common.get(client).drop(id, 'vacancy');
        }

        return {
            sync: sync,
            push: push,
            pushByUser: pushByUser,
            query: query,
            drop: drop,
            filterQueryStr: filterQueryStr
        };

    }

    module.exports = {
        get: get
    };
})();