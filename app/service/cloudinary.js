(function ()
{
    'use strict';
    var formidable = require('formidable');
    var cloudinary = require('cloudinary').v2;
    var applicationException = require('../service/applicationException');
    var Promise = require('bluebird');


    function upload(options, request, createOptionsFn)
    {
        /*jshint camelcase:false*/
        var form = new formidable.IncomingForm();
        return Promise.promisify(form.parse, form)(request).spread(function (fields, files)
        {
            if(createOptionsFn) {
                options = createOptionsFn(fields, files);
            }
            return cloudinary.uploader.upload(files.file.path, options);
        }).then(function (result)
        {
            if (result.error) {
                return Promise.reject(applicationException.new(applicationException.CONFLICT));
            } else {
                //var url = cloudinary.url(result.public_id, {format: 'jpg'});
                return {url: result.url};
            }
        });
    }

    module.exports = {
        upload: upload
    };
})();
