(function ()
{
    'use strict';

    var solr = require('./solrHandlers/clientExtended'); //require('solr-client');
   
    var config = require('../config/config.js');
    var vacancyHandler = require('./solrHandlers/vacancyHandler');
    var applicationHandler = require('./solrHandlers/applicationHandler');
    var common = require('./solrHandlers/common');
    var autocompleteHandler = require('./solrHandlers/autocompleteHandler');

    var client = solr.createClient(config.solr);
    client.autoCommit = false;


    module.exports = {
        pushVacancy: vacancyHandler.get(client).push,
        pushApplication: applicationHandler.get(client).push,
        dropVacancy: vacancyHandler.get(client).drop,
        dropApplication: applicationHandler.get(client).drop,
        queryVacancy: vacancyHandler.get(client).query,
        queryApplication: applicationHandler.get(client).query,
        suggest: autocompleteHandler.get(client),
        purge: common.get(client).purge,
        commit: common.get(client).commit,
        client: client,
        common: common.get(client),
        vacancyHandler: vacancyHandler.get(client)
    };
    
})();
