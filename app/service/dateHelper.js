(function ()
{
    'use strict';
    var moment = require('moment-timezone'),
            date = null;
    //kz timezone
    moment.tz.setDefault('Etc/GMT+6');
    function get()
    {
        return date || new Date().getTime();
    }

    function set(timestamp)
    {
        date = timestamp;
    }

    function getMoment()
    {
        return moment(get());
    }

    module.exports =
    {
        set: set,
        get: get,
        getMoment: getMoment,
        duration: moment.duration,
        moment: moment
    };
})();
