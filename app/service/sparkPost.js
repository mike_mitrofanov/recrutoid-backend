(function ()
{
    'use strict';
    var Promise = require('bluebird');
    var config = require('../config/config.js');
    var debugMail = config.sparkpost.debug?'.sink.sparkpostmail.com':'';
    var SparkPost = require('sparkpost');
    var sp = new SparkPost();
    var sparkPostPromise = Promise.promisify(sp.transmissions.send, sp.transmissions);
    var jade = require('jade');
    var dateHelper = require('../service/dateHelper');

    function createTokenAndSendEmail(token, email, type, vacancyObject, password)
    {
        if('vacancy' === type && password) {
            return sendActivationEmail(token, email, password, vacancyObject.publishViaEmail);
        }

        //resolve problem when is not given
        vacancyObject = vacancyObject || {};
        if (vacancyObject.categories) {
            vacancyObject.categories = vacancyObject.categories.map(function (category)
            {
                return category.name.en;
            });
        }
        var compiler = jade.compileFile(__dirname + '/../emailTemplates/veryfying-' + type + '.jade',
                {pretty: true, compileDebug: true});
        var html = compiler({
            context: config.frontendBaseUrl,
            token: token,
            vacancy: {
                details: vacancyObject.details,
                categories: vacancyObject.categories,
                companyName: vacancyObject.companyName,
                title: vacancyObject.title
            },
            resourcesBase: config.backendEmailStatic
        });
        var subject = '';
        if (vacancyObject.title) {
            subject = 'Confirm vacancy: ' + vacancyObject.title + ' at ' + vacancyObject.companyName;
        } else {
            subject = 'Recrutoid - Confirm your ' + type;
        }
        var emailPayload = {
            transmissionBody: {
                content: {
                    from: {
                        name: "Recrutoid Kazakhstan",
                        email: config.sparkpost.addresses.jobOffer
                    },
                    subject: subject,
                    html:html
                },
                recipients: [
                    {address: email+debugMail}
                ]
            }
        };
        return sparkPostPromise(emailPayload);
    }

    function sendNewVacancyNotification(vacancyObject, emails)
    {
        //resolve problem when is not given
        vacancyObject = vacancyObject || {};
        if (vacancyObject.categories) {
            vacancyObject.categories = vacancyObject.categories.map(function (category)
            {
                return category.name.en;
            });
        }
        var compiler = jade.compileFile(__dirname + '/../emailTemplates/new-waiting-for-approval-vacancy.jade',
            {pretty: true, compileDebug: true});
        var html = compiler({
            context: config.frontendBaseUrl,
            vacancy: {
                details: vacancyObject.details,
                categories: vacancyObject.categories,
                companyName: vacancyObject.companyName,
                title: vacancyObject.title,
                city: "test",
                salaryRange: "test1"
            },
            resourcesBase: config.backendEmailStatic
        });
        var subject = 'New vacancy is waiting for approval: ' + vacancyObject.title + ' at ' + vacancyObject.companyName;
        var emailPayload = {
            transmissionBody: {
                content: {
                    from: {
                        name: "Recrutoid Kazakhstan",
                        email: config.sparkpost.addresses.info
                    },
                    subject: subject,
                    html:html
                },
                recipients: []
            }
        };
        emails.forEach(function(email){
            emailPayload.transmissionBody.recipients.push({
                address: email+debugMail
            });
        });

        return sparkPostPromise(emailPayload);
    }

    function newCvCheckUpNotification(emails) {
        var compiler = jade.compileFile(__dirname + '/../emailTemplates/cv-check-up.tpl.jade',
            {pretty: true, compileDebug: true});
        var html = compiler({
            context: config.frontendBaseUrl,
            resourcesBase: config.backendEmailStatic,
            link: config.frontendBaseUrl + '/admin/cvcheckup',
        });

        var subject = 'New CV is waiting for approval';
        var emailPayload = {
            transmissionBody: {
                content: {
                    from: {
                        name: "Recrutoid Kazakhstan",
                        email: config.sparkpost.addresses.info
                    },
                    subject: subject,
                    html:html
                },
                recipients: []
            }
        };
        emails.forEach(function(email){
            emailPayload.transmissionBody.recipients.push({
                address: email+debugMail
            });
        });

        return sparkPostPromise(emailPayload);
    }

    function CvActivationNotification(email) {
        var compiler = jade.compileFile(__dirname + '/../emailTemplates/cv-has-been-activated.tpl.jade',
            {pretty: true, compileDebug: true});
        var html = compiler({
            context: config.frontendBaseUrl,
            resourcesBase: config.backendEmailStatic,
            link: config.frontendBaseUrl + '/user/settings/cv-page'
        });

        var subject = 'Your CV is now active';
        var emailPayload = {
            transmissionBody: {
                content: {
                    from: {
                        name: "Recrutoid Kazakhstan",
                        email: config.sparkpost.addresses.info
                    },
                    subject: subject,
                    html:html
                },
                recipients: [
                    {address: email+debugMail}
                ]
            }
        };

        return sparkPostPromise(emailPayload);
    }

    function approveVacancy(vacancyObject)
    {
        var compiler = jade.compileFile(__dirname + '/../emailTemplates/approve-vacancy.jade', {pretty: true});
        var html = compiler({
            context: config.frontendBaseUrl,
            vacancyId: vacancyObject.id,
            details: vacancyObject.details,
            categories: vacancyObject.categories,
            companyName: vacancyObject.companyName,
            title: vacancyObject.title,
            city: vacancyObject.cityId,
            salaryRange: vacancyObject.salaryRangeId,
            resourcesBase: config.backendEmailStatic
        });
        var emailPayload = {
            transmissionBody: {
                content: {
                    from: {
                        name: "Recrutoid Kazakhstan",
                        email: config.sparkpost.addresses.noReply
                    },
                    subject: 'Your vacancy "' + vacancyObject.title + '" has been approved',
                    html:html
                },
                recipients: [
                    {address: vacancyObject.email+debugMail}
                ]
            }
        };
        return sparkPostPromise(emailPayload);
    }

    function dispatchVacancies(subscription, vacancies, categories, language)
    {
        language = language || 'en';
        var htmlOut, emailPayload, promises = [];
        var compile = jade.compileFile(__dirname + '/../emailTemplates/job-offers.tpl.jade', {pretty: true});

        var categoriesJoined = '';
        for (var i = 0; i < categories.length; i++) {
            categoriesJoined += categories[i].name.en;
            if (i < categories.length - 1)
                categoriesJoined += ', ';
        }

        function sendWeeklyVacancies()
        {
            vacancies.map(function (vacancy)
            {
                if (vacancy.categories) {
                    vacancy.categories = vacancy.categories.map(function (category)
                    {
                        return category.name.en;
                    });
                }

                if (vacancy.createDate) {
                    vacancy.createDate = dateHelper.moment(vacancy.createDate).format('MM/DD/YYYY');
                }
            });

            htmlOut = compile({
                title: 'Recrutoid Job ' + subscription.interval + ' offers',
                categories: categories,
                vacancies: vacancies,
                daily: false,
                weekly: true,
                city: !!subscription.city ? subscription.city.name.en : null,
                salary: !!subscription.desiredSalaryRange ? subscription.desiredSalaryRange.name.en : null,
                context: (config.frontendBaseUrl) + '/subscription/edit/' + subscription.id,
                resourcesBase: config.backendEmailStatic,
                categoriesJoined: categoriesJoined
            });
            emailPayload = {
                transmissionBody: {
                    content: {
                        from: {
                            name: "Recrutoid Kazakhstan",
                            email: config.sparkpost.addresses.jobOffer
                        },
                        subject: 'Weekly vacancies matches',
                        html:htmlOut
                    },
                    recipients: [
                        {address: subscription.email+debugMail}
                    ]
                }
            };

            promises.push(sparkPostPromise(emailPayload));
        }

        function sendDailyVacancy()
        {
            for (var i = 0; i < vacancies.length; i++) {
                vacancies[i].categories = vacancies[i].categories.map(function (category)
                {
                    return category.name[language];
                });
                var emailTitle = '[Recrutoid]' + vacancies[i].title + ' [' + vacancies[i].cityId.name + '][' + vacancies[i].salaryRangeId.name[language] + ' KZT] ';
                var fromname = vacancies[i].email.split('@')[0];

                htmlOut = compile({
                    title: emailTitle,
                    vacancy: vacancies[i],
                    categories: categories,
                    city: !!subscription.city ? subscription.city.name.en : null,
                    salary: !!subscription.desiredSalaryRange ? subscription.desiredSalaryRange.name.en : null,
                    daily: true,
                    weekly: false,
                    context: (config.frontendBaseUrl) + '/subscription/edit/' + subscription.id,
                    resourcesBase: config.backendEmailStatic,
                    categoriesJoined: categoriesJoined
                });
                emailPayload = {
                    transmissionBody: {
                        content: {
                            from: {
                                name: fromname + ' via ' + config.sparkpost.addresses.jobOffer,
                                email: config.sparkpost.addresses.jobOffer
                            },
                            subject: emailTitle,
                            reply_to: vacancies[i].email,
                            html:htmlOut
                        },
                        recipients: [
                            {address: subscription.email+debugMail}
                        ]
                    }
                };
                promises.push(sparkPostPromise(emailPayload));
            }
        }

        if ('daily' === subscription.interval) {
            sendDailyVacancy();
        } else {
            sendWeeklyVacancies();
        }
        return Promise.all(promises);
    }

    function sendActivationEmail(token, email, password, fromVacancy)
    {
        var compile = jade.compileFile(__dirname + '/../emailTemplates/activate.account.jade', {pretty: true});
        var htmlOut = compile({
            context: (config.frontendBaseUrl) + '/activate/' + token,
            credentials: {
                email: email,
                password: password
            },
            fromVacancy: fromVacancy || false,
            resourcesBase: config.backendEmailStatic
        });
        var emailPayload = {
            transmissionBody: {
                content: {
                    from: {
                        name: 'Recrutoid Kazakhstan',
                        email: config.sparkpost.addresses.jobOffer
                    },
                    subject: 'Activate your account',
                    html:htmlOut
                },
                recipients: [
                    {address: email+debugMail}
                ]
            }
        };
        return sparkPostPromise(emailPayload);
    }

    function passwordChange(email, password)
    {
        var compile = jade.compileFile(__dirname + '/../emailTemplates/change-password.jade', {pretty: true});
        var htmlOut = compile({
            credentials: {
                email: email,
                password: password
            },
            resourcesBase: config.backendEmailStatic
        });
        var emailPayload = {
            transmissionBody: {
                content: {
                    from: {
                        name: 'Recrutoid Kazakhstan',
                        email: config.sparkpost.addresses.jobOffer
                    },
                    subject: 'Your password has been changed',
                    html:htmlOut
                },
                recipients: [
                    {address: email+debugMail}
                ]
            }
        };
        return sparkPostPromise(emailPayload);
    }

    function accountDeactivation(user, token){

        var compile = jade.compileFile(__dirname + '/../emailTemplates/deactivateProfile.tpl.jade', {pretty: true});
        var htmlOut = compile({
            context:config.frontendBaseUrl+'/user/extend-profile/' + token,
            resourcesBase: config.backendEmailStatic
        });
        var emailPayload = {
            transmissionBody: {
                content: {
                    from: {
                        name: 'Recrutoid Kazakhstan',
                        email: config.sparkpost.addresses.noReply
                    },
                    subject: 'Your account deactivation',
                    html:htmlOut
                },
                recipients: [
                    {address: user.email+debugMail}
                ]
            }
        };
        return sparkPostPromise(emailPayload);
    }

    function passwordReset(userEmail, token)
    {
        var compile = jade.compileFile(__dirname + '/../emailTemplates/resetPassword.tpl.jade', {pretty: true});
        var htmlOut = compile({
            context: config.frontendBaseUrl + '/reset-password/' + token,
            resourcesBase: config.backendEmailStatic
        });
        var emailPayload = {
            transmissionBody: {
                content: {
                    from: {
                        name: 'Recrutoid Kazakhstan',
                        email: config.sparkpost.addresses.jobOffer
                    },
                    subject: 'Reset password',
                    html:htmlOut
                },
                recipients: [
                    {address: userEmail+debugMail}
                ]
            }
        };
        return sparkPostPromise(emailPayload);
    }

    function sendReminderToOldVacancy(vacancy, token)
    {
        var compile = jade.compileFile(__dirname + '/../emailTemplates/remindVacancy.tpl.jade', {pretty: true});
        //TODO write view to extend vacancy.
        var htmlOut = compile({
            context: config.frontendBaseUrl + '/vacancy/extend/' + token,
            resourcesBase: config.backendEmailStatic
        });
        var emailPayload = {
            transmissionBody: {
                content: {
                    from: {
                        name: 'Recrutoid Kazakhstan',
                        email: config.sparkpost.addresses.noReply
                    },
                    subject: 'Your vacancy expires in 2 days',
                    html:htmlOut
                },
                recipients: [
                    {address: vacancy.email+debugMail}
                ]
            }
        };
        return sparkPostPromise(emailPayload);
    }

    function sendReminderToOldProfile(user, token, timeToExpire)
    {
        var compile = jade.compileFile(__dirname + '/../emailTemplates/remindProfile.tpl.jade', {pretty: true});
        var htmlOut = compile({
            context:config.frontendBaseUrl+'/user/extend-profile/' + token,
            resourcesBase: config.backendEmailStatic
        });
        var emailPayload = {
            transmissionBody: {
                content: {
                    from: {
                        name: 'Recrutoid Kazakhstan',
                        email: config.sparkpost.addresses.noReply
                    },
                    subject: 'Your profile expires in ' + timeToExpire + ' days',
                    html:htmlOut
                },
                recipients: [
                    {address: user.email+debugMail}
                ]
            }
        };
        return sparkPostPromise(emailPayload);
    }

    function applyNotification(vacancy, applier)
    {
        var compile = jade.compileFile(__dirname + '/../emailTemplates/apply-notification.tpl.jade', {pretty: true});
        var htmlOut = compile({
            vacancy: vacancy,
            applier: applier,
            link: config.frontendBaseUrl + '/user/settings/myVacancies?appliers=' + vacancy.id,
            resourcesBase: config.backendEmailStatic
        });
        var emailPayload = {
            transmissionBody: {
                content: {
                    from: {
                        name: 'Recrutoid Kazakhstan',
                        email: config.sparkpost.addresses.noReply
                    },
                    subject: 'Someone applied for your vacancy',
                    html:htmlOut
                },
                recipients: [
                    {address: vacancy.email+debugMail}
                ]
            }
        };
        return sparkPostPromise(emailPayload);
    }

    function changeEmail(token, newEmail)
    {
        var compile = jade.compileFile(__dirname + '/../emailTemplates/change-email.tpl.jade', {pretty: true});
        var htmlOut = compile({
            link: config.frontendBaseUrl + '/user/confirm-email/' + token,
            resourcesBase: config.backendEmailStatic
        });
        var emailPayload = {
            transmissionBody: {
                content: {
                    from: {
                        name: 'Recrutoid Kazakhstan',
                        email: config.sparkpost.addresses.noReply
                    },
                    subject: 'Confirm your new email address',
                    html:htmlOut
                },
                recipients: [
                    {address: newEmail+debugMail}
                ]
            }
        };
        return sparkPostPromise(emailPayload);
    }

    function customMessage(emails, emailTitle, emailContent)
    {
        var compile = jade.compileFile(__dirname + '/../emailTemplates/custom-message.tpl.jade', {pretty: true});
        var htmlOut = compile({
            emailTitle: emailTitle,
            emailContent: emailContent,
            resourcesBase: config.backendEmailStatic
        });

        var emailPayload = {
            transmissionBody: {
                content: {
                    from: {
                        name: 'Recrutoid Kazakhstan',
                        email: config.sparkpost.addresses.info
                    },
                    subject: emailTitle,
                    html:htmlOut
                },
                recipients: [
                    {address: config.sparkpost.addresses.noReply+debugMail}
                ]
            }
        };

        //prepare BCC
        for (var i = 0; i < emails.length; i++) {
            emailPayload.transmissionBody.recipients.push({
                address: emails[i],
                "header_to": config.sparkpost.addresses.noReply
            });
        }


        return sparkPostPromise(emailPayload);
    }

    function callForInterview(email, vacancyId)
    {
        var compile = jade.compileFile(__dirname + '/../emailTemplates/call-for-interview.tpl.jade', {pretty: true});
        var htmlOut = compile({
            link: config.frontendBaseUrl + '/vacancy/' + vacancyId,
            resourcesBase: config.backendEmailStatic
        });
        var emailPayload = {
            transmissionBody: {
                content: {
                    from: {
                        name: 'Recrutoid Kazakhstan',
                        email: config.sparkpost.addresses.noReply
                    },
                    subject: 'You have just received offer',
                    html:htmlOut
                },
                recipients: [
                    {address: email+debugMail}
                ]
            }
        };
        return sparkPostPromise(emailPayload);
    }

    function forwardCv(email, user, pdf)
    {
        var name = (user.firstName || '') + ' ' + (user.lastName || '');
        var compile = jade.compileFile(__dirname + '/../emailTemplates/forward-cv.tpl.jade', {pretty: true});
        var htmlOut = compile({
            name: name,
            resourcesBase: config.backendEmailStatic
        });
        var emailPayload = {
            transmissionBody: {
                content: {
                    from: {
                        name: name,
                        email: config.sparkpost.addresses.noReply
                    },
                    subject: 'CV - ' + user.resume.title,
                    html:htmlOut,
                    attachments: [
                        {
                            type: "application/pdf",
                            name: "CV.pdf",
                            data: pdf
                        }
                    ]
                },
                recipients: [
                    {address: email+debugMail}
                ]
            }
        };

        return sparkPostPromise(emailPayload);
    }

    module.exports = {
        sendReminderToOldProfile: sendReminderToOldProfile,
        approveVacancy: approveVacancy,
        sendReminderToOldVacancy: sendReminderToOldVacancy,
        createTokenAndSendEmail: createTokenAndSendEmail,
        dispatchVacancies: dispatchVacancies,
        sendActivationEmail: sendActivationEmail,
        passwordReset: passwordReset,
        applyNotification: applyNotification,
        changeEmail: changeEmail,
        customMessage: customMessage,
        callForInterview: callForInterview,
        forwardCv: forwardCv,
        sendNewVacancyNotification: sendNewVacancyNotification,
        passwordChange: passwordChange,
        accountDeactivation: accountDeactivation,
        newCvCheckUpNotification: newCvCheckUpNotification,
        CvActivationNotification: CvActivationNotification
    };
})
();
