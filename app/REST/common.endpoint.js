(function ()
{
    'use strict';

    var applicationException = require('../service/applicationException');
    var business = require('../business/business.container');

    module.exports = function (router)
    {
        router.route('/api/home').get(function (request, response)
        {
            business.getCommonManager(request).getHomeData().then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/nav-bar').get(function (request, response)
        {
            business.getCommonManager(request).getNavBarData().then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/is-profile-active').get(function (request, response)
        {
            business.getCommonManager(request).isProfileActive().then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/edit-resume').get(function (request, response)
        {
            business.getCommonManager(request).getEditResumeData().then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/email/send').post(function (request, response)
        {
            business.getCommonManager(request).sendCustomEmail(request.body).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                business.getEmailErrorsManager(request).logErrors(error);
                applicationException.errorHandler(error, respond);
            });
        });

        router.route('/api/search-vacancies').get(function (request, response)
        {
            business.getCommonManager(request).getSearchVacanciesData().then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/search-users').get(function (request, response)
        {
            business.getCommonManager(request).getSearchUsersData().then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });
    };
})();
