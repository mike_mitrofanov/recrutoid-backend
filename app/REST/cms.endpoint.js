(function ()
{
    'use strict';

    var applicationException = require('../service/applicationException');
    var business = require('../business/business.container');

    module.exports = function (router)
    {
        router.route('/api/cms').get(function (request, respond)
        {
            business.getCmsManager(request).query(request.query).then(function (results)
            {
                respond.status(200).send(results);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, respond);
            });
        });

        router.route('/api/cms').post(function (request, respond)
        {
            business.getCmsManager(request).createNewOrUpdate(request.body).then(function (result)
            {
                respond.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, respond);
            });
        });

        router.route('/api/cms/list').get(function (request, respond)
        {
            business.getCmsManager(request).getPagesList().then(function (results)
            {
                respond.status(200).send(results);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, respond);
            });
        });

        router.route('/api/cms/news').get(function (request, respond)
        {
            business.getCmsManager(request).getNews().then(function (results)
            {
                respond.status(200).send(results);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, respond);
            });
        });

        router.route('/api/cms/news/image').post(function (request, respond)
        {
            business.getCmsManager(request).uploadImage(request).then(function ()
            {
                respond.sendStatus(200);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, respond);
            });
        });

        router.route('/api/cms/:id').get(function (request, respond)
        {
            business.getCmsManager(request).get(request.params.id).then(function (result)
            {
                respond.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, respond);
            });
        });

        router.route('/api/cms/:id').delete(function (request, respond)
        {
            business.getCmsManager(request).remove(request.params.id).then(function ()
            {
                respond.sendStatus(200);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, respond);
            });
        });
    };
})();
