(function ()
{
    'use strict';

    var applicationException = require('../service/applicationException');
    var business = require('../business/business.container');

    module.exports = function (router)
    {
        router.route('/api/banners').get(function (request, response)
        {
            business.getBannersManager(request).query(request.query).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/banners').post(function (request, response)
        {
            business.getBannersManager(request).createNewOrUpdate(request.body).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/banners/upload').post(function (request, response)
        {
            business.getBannersManager(request).upload(request).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });
        router.route('/api/banners/published').get(function (request, response)
        {
            business.getBannersManager(request).getPublished().then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/banners/:id').get(function (request, response)
        {
            business.getBannersManager(request).get(request.params.id).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/banners/:id').delete(function (request, response)
        {
            business.getBannersManager(request).remove(request.params.id).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/banners/:id/activate').post(function (request, response)
        {
            business.getBannersManager(request).activate(request.params.id, request.body).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/banners/:id/deactivate').post(function (request, response)
        {
            business.getBannersManager(request).deactivate(request.params.id).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });
    };
})();
