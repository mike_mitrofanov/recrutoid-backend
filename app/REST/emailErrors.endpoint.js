(function ()
{
    'use strict';

    var applicationException = require('../service/applicationException');
    var business = require('../business/business.container');

    module.exports = function (router)
    {
        router.route('/api/errors/email').get(function (request, response)
        {
            business.getEmailErrorsManager(request).search(request.query).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });
    };
})();
