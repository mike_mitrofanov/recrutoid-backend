(function ()
{
    'use strict';

    var applicationException = require('../service/applicationException');
    var business = require('../business/business.container');

    module.exports = function (router)
    {
        router.route('/api/bugreports').get(function (request, response)
        {
            business.getBugreportsManager(request).search(request.query).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        }).post(function (request, response)
        {
            business.getBugreportsManager(request).createNew(request.body).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/bugreports/:id').delete(function (request, response)
        {
            business.getBugreportsManager(request).remove(request.params.id).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });
    };
})();
