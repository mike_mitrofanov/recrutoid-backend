(function ()
{
    'use strict';

    var solrHelper = require('../service/solrHelper');

    module.exports = function (router)
    {
        router.route('/api/solr_vacancies').get(function (request, response)
        {
            solrHelper.queryVacancy(request.query).then(function (vacancies) {
                response.status(200).send(vacancies);
            });
        });

         router.route('/api/solr_applications').get(function (request, response)
        {
            solrHelper.queryApplication(request.query, true).then(function (applications) {
                response.status(200).send(applications);
            });
        });

    };
})();
