(function ()
{
    'use strict';
    var applicationException = require('../service/applicationException');
    var business = require('../business/business.container');

    module.exports = function (router)
    {
        router.route('/api/distributors').get(function (request, respond)
        {
            business.getDistributorsManager(request).query().then(function (result)
            {
                respond.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, respond);
            });
        });
        router.route('/api/distributors').post(function (request, respond)
        {
            business.getDistributorsManager(request).verifyDistributorsByAdmin(request.body.user).then(function ()
            {
                respond.sendStatus(200);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, respond);
            });
        });

        router.route('/api/distributors/resend').post(function (request, respond)
        {
            business.getDistributorsManager(request).resendEmail(request.body.id).then(function ()
            {
                respond.sendStatus(200);
            }).catch(function (error)
            {
                business.getEmailErrorsManager(request).logErrors(error);
                applicationException.errorHandler(error, respond);
            });
        });

        router.route('/api/distributors/:token/confirm').post(function (request, respond)
        {
            business.getDistributorsManager(request).verifyDistributorsByToken(request.params.token).then(function ()
            {
                respond.sendStatus(200);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, respond);
            });
        });

        router.route('/api/distributors/:id').delete(function (request, respond)
        {
            business.getDistributorsManager(request).remove(request.params.id).then(function ()
            {
                respond.sendStatus(200);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, respond);
            });
        });
    };

})();
