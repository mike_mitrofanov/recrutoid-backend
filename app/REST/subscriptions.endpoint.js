(function ()
{
    'use strict';

    var applicationException = require('../service/applicationException');
    var business = require('../business/business.container');

    module.exports = function (router)
    {
        router.route('/api/subscriptions').get(function (request, response)
        {
            business.getSubscriptionsManager(request).getAllSubscriptions(request.query).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });
        router.route('/api/subscriptions').post(function (request, response)
        {
            business.getSubscriptionsManager(request).createNewOrUpdate(request.body).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/subscriptions/:id').get(function (request, response)
        {
            business.getSubscriptionsManager(request).get(request.params.id).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/subscriptions/:id/remove').delete(function (request, response)
        {
            business.getSubscriptionsManager(request).deleteSubscription(request.params.id).then(function ()
            {
                response.sendStatus(200);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/subscriptions/:id/unsubscribe').delete(function (request, response)
        {
            business.getSubscriptionsManager(request).unSubscribe(request.params.id).then(function ()
            {
                response.sendStatus(200);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/subscriptions/:id/ban').post(function (request, response)
        {
            business.getSubscriptionsManager(request).ban(request.params.id).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/subscriptions/:id/un-ban').post(function (request, response)
        {
            business.getSubscriptionsManager(request).unBan(request.params.id).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/subscriptions/:id/:action').post(function (request, response)
        {
            business.getSubscriptionsManager(request).verify(request.params.id).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });
    };
})();
