(function ()
{
    'use strict';

    var business = require('../business/business.container');
    var applicationException = require('../service/applicationException');
    var config = require('../config/config.js');

    var dummyResponse = {data: []};

    module.exports = function (router)
    {
        router.route('/api/autocomplete/vacancies').get(function (request, response) {
            if (config.solr.bypass == 'true') {
                response.status(200).send(dummyResponse);
            } else {
                business.getAutocompleteManager(request).vacancy(request.query).then(function (result) {
                    response.status(200).send(result);
                }).catch(function (error)
                {
                    applicationException.errorHandler(error, response);
                });
            }
        });

        router.route('/api/autocomplete/cv').get(function (request, response) {
            if (config.solr.bypass == 'true') {
                response.status(200).send(dummyResponse);
            } else {
                business.getAutocompleteManager(request).application(request.query).then(function (result) {
                    response.status(200).send(result);
                }).catch(function (error)
                {
                    applicationException.errorHandler(error, response);
                });
            }
        });
        
        router.route('/api/autocomplete/companies').get(function (request, response) {
            if (config.solr.bypass == 'true') {
                response.status(200).send(dummyResponse);
            } else {
                business.getAutocompleteManager(request).company(request.query).then(function (result) {
                    response.status(200).send(result);
                }).catch(function (error)
                {
                    applicationException.errorHandler(error, response);
                });
            }
        });
      
    };

})();