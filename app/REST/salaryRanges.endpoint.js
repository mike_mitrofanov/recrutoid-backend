(function ()
{
    'use strict';

    var applicationException = require('../service/applicationException');
    var business = require('../business/business.container');

    module.exports = function (router)
    {
        router.route('/api/salary-ranges').get(function (request, response)
        {
            business.getSalaryRangesManager(request).getAllSalaryRanges().then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        }).post(function (request, response)
        {
            business.getSalaryRangesManager(request).createNewOrUpdate(request.body).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/salary-ranges/:id/:replaceId').delete(function (request, response)
        {
            business.getSalaryRangesManager(request).remove(request.params.id, request.params.replaceId).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });
    };
})();
