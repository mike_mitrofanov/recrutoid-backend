(function ()
{
    'use strict';

    var applicationException = require('../service/applicationException');
    var business = require('../business/business.container');

    module.exports = function (router)
    {
        router.route('/sitemap.xml').get(function (request, response)
        {
            business.getSeoManager(request).createSiteMap().then(function (result)
            {
                response.set('Content-Type', 'text/xml');
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });
    };
})();
