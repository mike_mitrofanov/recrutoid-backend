(function ()
{
    'use strict';

    var applicationException = require('../service/applicationException');
    var business = require('../business/business.container');

    module.exports = function (router)
    {
        router.route('/api/settings').get(function (request, response)
        {
            business.getSettingsManager(request).query().then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/settings').post(function (request, response)
        {
            business.getSettingsManager(request).set(request.body).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/settings/user-expiration').get(function (request, response)
        {
            business.getSettingsManager(request).get('userExpirationTime').then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/settings/site-settings').get(function (request, response)
        {
            business.getSettingsManager(request).getPublicSettings().then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/settings/statistics').get(function (request, response)
        {
            business.getSettingsManager(request).getStatistics().then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });
    };
})();
