(function ()
{
    'use strict';

    var applicationException = require('../service/applicationException');
    var business = require('../business/business.container');

    module.exports = function (router)
    {


        router.route('/api/phrases/add').post(function (request, response)
        {
            business.getPhrasesManager(request).add(request.body).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/phrases').get(function (request, response)
        {
            business.getPhrasesManager(request).search(request.query).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        }).post(function (request, response)
        {
            business.getPhrasesManager(request).update(request.body).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        
        });


            





        router.route('/api/phrases/:id').delete(function (request, response)
        {
            business.getPhrasesManager(request).remove(request.params.id).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });


    };
})();
