(function ()
{
    'use strict';
    var applicationException = require('../service/applicationException');
    var business = require('../business/business.container');

    module.exports = function (route)
    {
        route.route('/api/email/subscribe').post(function (request, respond)
        {
            business.getEmailManager(request).resendEmailToActiveSubscribe(request.body.id).then(function ()
            {
                respond.sendStatus(200);
            }).catch(function (error)
            {
                business.getEmailErrorsManager(request).logErrors(error);
                applicationException.errorHandler(error, respond);
            });
        });

        route.route('/api/email/vacancy').post(function (request, respond)
        {
            business.getEmailManager(request).resendEmailToActiveVacancy(request.body.id).then(function ()
            {
                respond.sendStatus(200);
            }).catch(function (error)
            {
                business.getEmailErrorsManager(request).logErrors(error);
                applicationException.errorHandler(error, respond);
            });
        });

        route.route('/api/email/test').get(function (request, respond)
        {
            var jade = require('jade');
            var config = require('../config/config.js');

            var token = 's0m3t0k3n';

            var email = 'some@email.com';
            // var password = 'mYpAssw0rd';
            // var fromVacancy = false;

            var applier = {
                firstName: "John",
                lastName: "Doe"
            };

            var vacancyObject = {
                id: token,
                title: "Some vacancy title",
                details: "<p>"+
                "<strong>Lorem ipsum dolor sit amet</strong>, consectetur adipiscing elit. Praesent interdum ligula urna, nec mollis neque auctor maximus. Donec nec ante et enim scelerisque efficitur. Curabitur erat sapien, commodo at cursus a, mattis in ex. Praesent ut luctus arcu. Quisque a nulla et ipsum tincidunt posuere eu vel sapien. Phasellus congue lorem augue, laoreet facilisis tortor porta non. Praesent non sapien nisl. Mauris odio nisi, ullamcorper ac eros in, porttitor feugiat nibh. Donec ultrices lacus libero, quis elementum diam blandit a. Aenean sit amet sollicitudin sem. Pellentesque risus nisi, lobortis quis lorem ac, auctor dictum odio."+
                "</p>"+
                "<p>"+
                "Maecenas quis porttitor enim. Etiam aliquet eros vitae nunc auctor hendrerit. Curabitur vestibulum scelerisque metus, at mattis justo fringilla ac. Pellentesque orci mauris, bibendum sed ex ac, porta facilisis leo. Cras malesuada tortor sit amet erat posuere mattis. Fusce at quam mauris. Vivamus elementum sodales neque nec bibendum. Phasellus vel quam pellentesque, malesuada odio quis, blandit felis. Donec porta ligula vel ante placerat euismod. Nulla in est sed urna venenatis tempor. Aliquam nisi metus, sollicitudin at nibh nec, eleifend interdum ipsum. Vestibulum imperdiet malesuada aliquet. Praesent arcu leo, finibus at eros quis, hendrerit mattis elit. Sed tincidunt egestas hendrerit. Duis venenatis finibus libero et luctus."+
                "</p>",
                email: "john.bob.doe@somelongdomain.com",
                categories: [
                    {
                        name: {
                            en: "Some category",
                            ru: "Russian category"
                        }
                    },
                    {
                        name: {
                            en: "Another category",
                            ru: "Another Russian category"
                        }
                    }
                ],
                companyName: "Some great company LCC",
                cityId: {
                    name: {
                        en: "Almaty",
                        ru: "Russian Almaty"
                    }
                },
                salaryRangeId: {
                    name: {
                        en: "100 000 - 200 000",
                        ru: "100 000 - 200 000"
                    }
                },
                createDate: 1482345219330.0,
                context: '/someContext'
            };

            var emailTitle = "Some email title";
            var emailContent = vacancyObject.details;

            var vacancies = [];
            vacancies.push(vacancyObject);
            vacancies.push(Object.assign({}, vacancyObject));
            vacancies.push(Object.assign({}, vacancyObject));

            var language = 'en';

            var categories = [
                {
                    name: {
                        en: "Some category",
                        ru: "Russian category"
                    }
                },
                {
                    name: {
                        en: "Another category",
                        ru: "Another Russian category"
                    }
                },
                {
                    name: {
                        en: "Yet another category",
                        ru: "Yet another Russian category"
                    }
                },
                {
                    name: {
                        en: "Some category",
                        ru: "Russian category"
                    }
                },
                {
                    name: {
                        en: "Another category",
                        ru: "Another Russian category"
                    }
                },
                {
                    name: {
                        en: "Yet another category",
                        ru: "Yet another Russian category"
                    }
                },
                {
                    name: {
                        en: "Some category",
                        ru: "Russian category"
                    }
                },
                {
                    name: {
                        en: "Another category",
                        ru: "Another Russian category"
                    }
                },
                {
                    name: {
                        en: "Yet another category",
                        ru: "Yet another Russian category"
                    }
                }
            ];

            var user = {
                id: 's0m3us3r1d',
                firstName: 'John',
                lastName: 'Doe'
            };

            var dateHelper = require('../service/dateHelper');

            vacancies.map(function (vacancy)
            {
                if (vacancy.categories) {
                    vacancy.categories = vacancy.categories.map(function (category)
                    {
                        return category.name.en;
                    });
                }

                if (vacancy.createDate) {
                    vacancy.createDate = dateHelper.moment(vacancy.createDate).format('D.M.YYYY');
                }
            });

            var subscription = {
                categories: [
                    {
                        name: {
                            'en': 'CategoryName'
                        }
                    }
                ],
                banned: false,
                createDate: 1484924095265,
                subscribed: true,
                verified: true,
                desiredSalaryRange: { order: 2,
                    name: { ru: '400 001 - 600 000', en: '400 001 - 600 000' },
                    _id: '51eae100c2e6b6c222ec3433' },
                city: null,
                email: 'michal.strychalski+666@polcode.net',
                interval: 'daily',
                id: '588224bfa76e7f0422f09623' };

            var categoriesJoined = '';
            for (var i = 0; i < categories.length; i++) {
                categoriesJoined += categories[i].name.en;
                if (i < categories.length - 1)
                    categoriesJoined += ', ';
            }

            var compile = jade.compileFile(__dirname + '/../emailTemplates/activate.account.jade', {pretty: true});
            var htmlOut = compile({
                context: (config.frontendBaseUrl) + '/activate/' + token,
                credentials: {
                    email: 'asdas@asd.pl',
                    password: 'teasdasdasd'
                },
                fromVacancy: false,
                resourcesBase: config.backendEmailStatic
            });

            respond.send(htmlOut);
        });
    };
})();
