(function ()
{
    'use strict';

    var applicationException = require('../service/applicationException');
    var business = require('../business/business.container');

    module.exports = function (router)
    {
        router.route('/api/banned-domains').get(function (request, response)
        {
            business.getBlacklistedDomainsManager(request).getAllDomains(request.query).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        }).post(function (request, response)
        {
            business.getBlacklistedDomainsManager(request).createNewOrUpdate(request.body).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/banned-domains/:id').delete(function (request, response)
        {
            business.getBlacklistedDomainsManager(request).remove(request.params.id).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/banned-domains/check-email').post(function (request, respond)
        {
            business.getBlacklistedDomainsManager(request).rejectIfBlacklisted(request.body.email).then(function ()
            {
                respond.sendStatus(200);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, respond);
            });
        });
    };
})();
