(function ()
{
    'use strict';

    var applicationException = require('../service/applicationException');
    var business = require('../business/business.container.js');
    var config = require('../config/config.js');
    var fs = require('fs');

    module.exports = function (router)
    {
        router.route('/api/users').get(function (request, response)
        {
            business.getUsersManager(request).search(request.query).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/offers').delete(function (request, response)
        {
            business.getUsersManager(request).removeOffers(request.query.ids).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/offers/:id').delete(function (request, response)
        {
            business.getUsersManager(request).removeOffers(request.params.id).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/auth').post(function (request, response)
        {
            business.getUsersManager(request).authenticate(request.body.email, request.body.password).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/auth/facebook').post(function (request, response)
        {
            business.getUsersManager(request).authenticateByFacebook(request.body).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/auth/linkedin').post(function (request, response)
        {
            business.getUsersManager(request).authenticateByLinkedIn(request.body).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/impersonate').post(function (request, response)
        {
            business.getUsersManager(request).impersonate(request.body.userId).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/find').get(function (request, response)
        {
            if (config.solr.bypass == 'true') {
                console.log('a');
                business.getUsersManager(request).find(request.query).then(function (result)
                {
                    response.status(200).send(result);
                }).catch(function (error)
                {
                    applicationException.errorHandler(error, response);
                });
            } else {
                console.log('b');
                business.getUsersSearchManager(request).find(request.query).then(function (result) {
                    response.status(200).send(result);
                }).catch(function (error)
                {
                    applicationException.errorHandler(error, response);
                });
            }



        });

        router.route('/api/users/find_old').get(function (request, response)
        {
            business.getUsersManager(request).find(request.query).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/is-new').post(function (request, response)
        {
            business.getUsersManager(request).isNew(request.body).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/:id').delete(function (request, response)
        {
            business.getUsersManager(request).remove(request.params.id).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/:id/revert').delete(function (request, response)
        {
            business.getUsersManager(request).unremove(request.params.id).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/my-profile').get(function (request, response)
        {
            business.getUsersManager(request).getProfile().then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/my-profile').post(function (request, response)
        {
            business.getUsersManager(request).updateProfile(request.body).then(function ()
            {
                response.sendStatus(200);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/avatar').post(function (request, response)
        {
            business.getUsersManager(request).uploadAvatar(request).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/applications-notifications').post(function (request, response)
        {
            business.getUsersManager(request).changeApplicationsNotifications(request.body).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/five-employers').get(function (request, response)
        {
            business.getUsersManager(request).getFiveMostActiveOnSiteEmployers().then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/admin/new').post(function (request, response)
        {
            business.getUsersManager(request).registerAdmin(request.body).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/:id/resend-registration-email').post(function (request, response)
        {
            business.getUsersManager(request).resendRegistrationEmail(request.params.id).then(function ()
            {
                response.sendStatus(200);
            }).catch(function (error)
            {
                business.getEmailErrorsManager(request).logErrors(error);
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/:id/verify').post(function (request, response)
        {
            business.getUsersManager(request).verify(request.params.id).then(function ()
            {
                response.sendStatus(200);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/:id/ban').post(function (request, response)
        {
            business.getUsersManager(request).ban(request.params.id).then(function ()
            {
                response.sendStatus(200);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/:id/unban').post(function (request, response)
        {
            business.getUsersManager(request).unban(request.params.id).then(function ()
            {
                response.sendStatus(200);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/:id/grant/:permission').post(function (request, response)
        {
            business.getUsersManager(request).changePermission(request.params.id, request.params.permission, true).then(function ()
            {
                response.sendStatus(200);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/:id/deny/:permission').post(function (request, response)
        {
            business.getUsersManager(request).changePermission(request.params.id, request.params.permission, false).then(function ()
            {
                response.sendStatus(200);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/offers').get(function (request, response)
        {
            business.getUsersManager(request).getOffers(request.query).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/offers/mark-all-seen').get(function (request, response)
        {
            business.getUsersManager(request).markAllOffersSeen().then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/offers/fresh-offers-views').get(function (request, response)
        {
            business.getUsersManager(request).getNewOffersViewsCount().then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/offers/:id/seen').post(function (request, response)
        {
            business.getUsersManager(request).markOfferSeen(request.params.id).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/resume/forward').post(function (request, response)
        {
            business.getUsersManager(request).forwardCv(request.body.email).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/:id/resume').get(function (request, response)
        {
            business.getUsersManager(request).getResume(request.params.id).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/:id/resume').post(function (request, response)
        {
            business.getUsersManager(request).updateResume(request.body).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/:id/resume/pdf').get(function (request, response)
        {
            business.getUsersManager(request).getResumeAsPDF(request.params.id).then(function (result)
            {
                var filepath = result;

                fs.exists(filepath, function (exists) {
                    if (exists) {
                        response.writeHead(200, {
                            'Content-Type': 'application/pdf',
                            'Content-Disposition': 'attachment; filename=cv-' + request.params.id + '.pdf'
                        });
                        fs.createReadStream(filepath).pipe(response);
                    } else {
                        response.writeHead(400, {"Content-Type": "text/plain"});
                        response.end("ERROR File does NOT Exists");
                    }
                });
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/:id/resume/views').get(function (request, response)
        {
            business.getUsersManager(request).queryResumeViews(request.params.id, request.query).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/:id/resume/mark-seen').post(function (request, response)
        {
            business.getUsersManager(request).markResumeViewsSeen(request.params.id, request.body).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/resume/upload').post(function (request, response)
        {
            business.getUsersManager(request).uploadCv(request).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/resume/notify-active').post(function (request, response)
        {
            business.getUsersManager(request).cvActivationNotification(request.body).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/resume/cancel').post(function (request, response)
        {
            business.getUsersManager(request).cancelCvCheckup(request).then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });


        router.route('/api/users/fresh-resume-views').get(function (request, response)
        {
            business.getUsersManager(request).getNewResumeViewsCount(request.params.id).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/:id/resume/show-on-home').post(function (request, response)
        {
            business.getUsersManager(request).showCvOnHome(request.params.id, request.body.showCvOnHome).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/:id/call-for-interview').post(function (request, response)
        {
            business.getUsersManager(request).callForInterview(request.params.id, request.body).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/search-visited').post(function (request, respond)
        {
            business.getUsersManager(request).searchVisited(request.body).then(function (result)
            {
                respond.status(200).send("OK");
            }).catch(function (error)
            {
                applicationException.errorHandler(error, respond);
            });
        });

        router.route('/api/users/register').post(function (request, respond)
        {
            business.getUsersManager(request).register(request.body).then(function (result)
            {
                respond.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, respond);
            });
        });

        router.route('/api/users/check-email').post(function (request, respond)
        {
            business.getUsersManager(request).checkEmailAvailability(request.body.email).then(function (result)
            {
                respond.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, respond);
            });
        });

        router.route('/api/users/activate').post(function (request, respond)
        {
            business.getUsersManager(request).activate(request.body.token).then(function (result)
            {
                respond.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, respond);
            });
        });

        router.route('/api/users/confirm-email').post(function (request, respond)
        {
            business.getUsersManager(request).confirmEmail(request.body.token).then(function (result)
            {
                respond.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, respond);
            });
        });

        router.route('/api/users/init-password-reset').post(function (request, respond)
        {
            business.getUsersManager(request).passwordReset(request.body.email).then(function ()
            {
                respond.sendStatus(200);
            }).catch(function (error)
            {
                business.getEmailErrorsManager(request).logErrors(error);
                applicationException.errorHandler(error, respond);
            });
        });

        router.route('/api/users/set-password').post(function (request, respond)
        {
            business.getUsersManager(request).setPassword(request.body.token, request.body.password, request.body.confirmPassword).then(function ()
            {
                respond.sendStatus(200);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, respond);
            });
        });

        router.route('/api/users/token/:token').get(function (request, respond)
        {
            business.getUsersManager(request).checkToken(request.params.token).then(function ()
            {
                respond.sendStatus(200);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, respond);
            });
        });

        router.route('/api/users/extend/:token').post(function (request, respond)
        {
            business.getUsersManager(request).extendProfile(request.params.token).then(function ()
            {
                respond.sendStatus(200);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, respond);
            });
        });

        router.route('/api/users/me/optional').get(function (request, response)
        {
            /*jshint -W030*/
            request.user ? response.status(200).send(request.user).end() : response.status(200).send({}).end();
        });

        router.route('/api/users/me').get(function (request, response)
        {
            /*jshint -W030*/
            request.user ? response.status(200).send(request.user).end() : response.sendStatus(401);
        });

        router.route('/api/users/carousel').get(function (request, response)
        {
            business.getUsersManager(request).carousel().then(function (result)
            {
                response.status(200).send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/is-visited-list').post(function (request, response)
        {
            business.getUsersManager(request).isVisitedList(request.body, true).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/users/is-visited').post(function (request, response)
        {
       
            business.getUsersManager(request).isVisited(request.body).then(function (result)
            {
                response.send(result);
            }).catch(function (error)
            {
                applicationException.errorHandler(error, response);
            });
        });
    };
})();
