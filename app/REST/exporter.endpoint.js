(function ()
{
    'use strict';

    var applicationException = require('../service/applicationException');
    var business = require('../business/business.container');
    var fs = require('fs');
    var path = require('path');
    var process = require('process');

    module.exports = function (router)
    {
        router.route('/api/exporter/vacancies/xls').post(function (request, response)
        {
            business.getExporterManager(request).vacanciesXls(request.body).then(function (result) {
                var xlsPath = result.fullPath;

                fs.exists(xlsPath, function(exists) {
                    if (exists) {
                        response.download(xlsPath, 'text.xlsx');
                    } else {
                        response.writeHead(400, {"Content-Type": "text/plain"});
                        response.end("ERROR File does NOT Exists");
                    }
                });
            }).catch(function (error) {
                applicationException.errorHandler(error, response);
            });
        });
        
        router.route('/api/exporter/vacancies/pdf').post(function (request, response)
        {
            business.getExporterManager(request).vacanciesPdf(request.body).then(function (result) {                
                var filepath = result;

                fs.exists(filepath, function(exists){
                    if (exists) {
                        response.writeHead(200, {
                            'Content-Type': 'application/pdf',
                            'Content-Disposition': 'attachment; filename=export.pdf'
                        });
                        fs.createReadStream(filepath).pipe(response);
                    } else {
                        response.writeHead(400, {"Content-Type": "text/plain"});
                        response.end("ERROR File does NOT Exists");
                    }
                });

            }).catch(function (error) {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/exporter/subscriptions/xls').post(function (request, response)
        {
            business.getExporterManager(request).subscriptionsXls(request.body).then(function (result) {
                var xlsPath = result.fullPath;

                fs.exists(xlsPath, function(exists) {
                    if (exists) {
                        response.download(xlsPath, 'text.xlsx');
                    } else {
                        response.writeHead(400, {"Content-Type": "text/plain"});
                        response.end("ERROR File does NOT Exists");
                    }
                });
            }).catch(function (error) {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/exporter/subscriptions/pdf').post(function (request, response)
        {
            business.getExporterManager(request).subscriptionsPdf(request.body).then(function (result) {
                var filepath = result;

                fs.exists(filepath, function(exists){
                    if (exists) {
                        response.writeHead(200, {
                            'Content-Type': 'application/pdf',
                            'Content-Disposition': 'attachment; filename=subscriptions.pdf'
                        });
                        fs.createReadStream(filepath).pipe(response);
                    } else {
                        response.writeHead(400, {"Content-Type": "text/plain"});
                        response.end("ERROR File does NOT Exists");
                    }
                });

            }).catch(function (error) {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/exporter/users/xls').post(function (request, response)
        {
            business.getExporterManager(request).usersXls(request.body).then(function (result) {
                var xlsPath = result.fullPath;

                fs.exists(xlsPath, function(exists) {
                    if (exists) {
                        response.download(xlsPath, 'text.xlsx');
                    } else {
                        response.writeHead(400, {"Content-Type": "text/plain"});
                        response.end("ERROR File does NOT Exists");
                    }
                });
            }).catch(function (error) {
                applicationException.errorHandler(error, response);
            });
        });

        router.route('/api/exporter/users/pdf').post(function (request, response)
        {
            business.getExporterManager(request).usersPdf(request.body).then(function (result) {
                var filepath = result;

                fs.exists(filepath, function(exists){
                    if (exists) {
                        response.writeHead(200, {
                            'Content-Type': 'application/pdf',
                            'Content-Disposition': 'attachment; filename=users.pdf'
                        });
                        fs.createReadStream(filepath).pipe(response);
                    } else {
                        response.writeHead(400, {"Content-Type": "text/plain"});
                        response.end("ERROR File does NOT Exists");
                    }
                });

            }).catch(function (error) {
                applicationException.errorHandler(error, response);
            });
        });
    };
})();
