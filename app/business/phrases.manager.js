(function ()
{
    'use strict';

    var phrasesDAO = require('../DAO/phrasesDAO');
    var daoFilterHelper = require('../util/daoFilterHelper');
    var security = require('./security');
    var _ = require('lodash');

    function create(context)
    {
        function search(filter)
        {
            var query = {};
            daoFilterHelper.copy(query, filter, 'action');

            query.from = filter.from;
            query.size = filter.size;

            return phrasesDAO.query(query);
        }

        function update(translation)
        {
            return phrasesDAO.update(translation);
        }
        function add(translation)
        {
            return phrasesDAO.add(translation);
        }

        function remove(id)
        {
            return security.requireAdmin(context).then(function ()
            {
                return phrasesDAO.remove(id);
            });
        }

        return {   
            update: update,
            add: add,
            search: search,
            remove: remove
        };
    }

    module.exports = {
        create: create
    };
})();
