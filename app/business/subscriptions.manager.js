(function ()
{
    'use strict';

    var _ = require('lodash');
    var mongodb = require('mongodb');
    var daoFilterHelper = require('../util/daoFilterHelper');
    var subscriptionsDAO = require('../DAO/subscriptionsDAO');
    var vacanciesDAO = require('../DAO/vacanciesDAO');
    var sparkPost = require('../service/sparkPost');
    var security = require('./security');
    var sha1 = require('sha1');
    var getDate = require('../service/dateHelper').get;
    var applicationException = require('./../service/applicationException');
    var Promise = require('bluebird');


    function create(context, business)
    {
        function createTokenAndSendEmail(subscription)
        {
            return sparkPost.createTokenAndSendEmail(subscription.token, subscription.email, 'subscription');
        }

        function createNewOrUpdate(subscriptionData)
        {
            if (!security.isAuthenticated(context) && !subscriptionData.id) {
                subscriptionData.token = sha1(getDate());
            }
            if (!subscriptionData.id) {
                subscriptionData.verified = security.isAuthenticated(context);
                subscriptionData.banned = false;
                subscriptionData.subscribed = true;
                return subscriptionsDAO.createNewOrUpdate(subscriptionData).then(function (subscription)
                {
                    // for this case recalculating will be done after verification
                    if(!security.isAuthenticated(context)) {
                        createTokenAndSendEmail(subscription).catch(business.getEmailErrorsManager(context).logErrors);
                    }
                    subscriptionData = subscription;
                });
            } else {
                return subscriptionsDAO.update({_id: subscriptionData.id}, subscriptionData, true);
            }
        }

        function getAllSubscriptions(filter)
        {
            var query = {};

            daoFilterHelper.copy(query, filter, 'email');
            daoFilterHelper.copy(query, filter, 'city');
            daoFilterHelper.copy(query, filter, 'categories');
            daoFilterHelper.copy(query, filter, 'interval');
            daoFilterHelper.copyBoolean(query, filter, 'verified');
            daoFilterHelper.copyBoolean(query, filter, 'banned');
            daoFilterHelper.copyBoolean(query, filter, 'subscribed');
            daoFilterHelper.copy(query, filter, 'desiredSalaryRange');

            query.size = Math.max(filter.size, 0) || 20;
            query.from = filter.from;
            
            return security.requireUser(context).then(function ()
            {
                return subscriptionsDAO.query(query);
            });
        }

        function getAll(filter)
        {
            return security.requireAdmin(context).then(function ()
            {
                return subscriptionsDAO.getAll(filter);
            });
        }

        function get(id)
        {
            var filter = mongodb.ObjectID.isValid(id.toString()) ? id : {token: id};
            return subscriptionsDAO.get(filter);
        }

        function getSubscriptionsForDispatch(query, countOnly)
        {
            var filter = {};
            filter.interval = {$in: query};
            filter.verified = {$ne: false};
            filter.banned = {$ne: true};
            filter.subscribed = {$ne: false};

            return subscriptionsDAO.query(filter);
        }

        function verify(id)
        {
            function doVerify()
            {
                var filter;
                if (!security.isAdmin(context)) {
                    filter = mongodb.ObjectID.isValid(id) ? {_id: id} : {token: id};
                    return subscriptionsDAO.get(filter).then(function (sub)
                    {
                        return subscriptionsDAO.update({_id: sub.id}, {
                            verified: true
                        });
                    });
                } else {
                    filter = mongodb.ObjectID.isValid(id) ? {_id: id} : {token: id};
                    return subscriptionsDAO.update(filter, {
                        verified: true
                    });
                }
            }

            return doVerify().then(function (result)
            {
                
                return result;
            });
        }

        function deleteSubscription(id)
        {
            var filter = mongodb.ObjectID.isValid(id.toString()) ? {_id: id} : {token: id};
            return subscriptionsDAO.get(filter).then(function (subscription)
            {
                function doRemove()
                {
                    return subscriptionsDAO.remove(filter).then(function (result)
                    {

                        return result;
                    });
                }

                if (security.isAdmin(context)) {
                    return doRemove();
                } else {
                    throw applicationException.new(applicationException.FORBIDDEN);
                }
            });
        }

        function unSubscribe(id)
        {
            var filter = mongodb.ObjectID.isValid(id.toString()) ? {_id: id} : {token: id};
            return subscriptionsDAO.get(filter).then(function (subscription)
            {
                subscription.subscribed = false;
                return subscriptionsDAO.model.update({_id:subscription.id},{$set: {subscribed: false}}).then(function (result)
                {
                    return result;
                });
            });
        }

        function ban(idSubscription)
        {
            return security.requireAdmin(context).then(function ()
            {
                return subscriptionsDAO.update({_id: idSubscription}, {$set: {banned: true}});
            });
        }

        function unBan(idSubscription)
        {
            return security.requireAdmin(context).then(function ()
            {
                return subscriptionsDAO.update({_id: idSubscription}, {$set: {banned: false}});
            });
        }

        return {
            ban: ban,
            unBan: unBan,
            createNewOrUpdate: createNewOrUpdate,
            getAllSubscriptions: getAllSubscriptions,
            getAll: getAll,
            get: get,
            getSubscriptionsForDispatch: getSubscriptionsForDispatch,
            verify: verify,
            deleteSubscription: deleteSubscription,
            unSubscribe: unSubscribe
        };
    }

    module.exports = {
        create: create
    };
})();
