(function ()
{
    'use strict';
    var security = require('./security');
    var SubscribeDAO = require('../DAO/subscriptionsDAO');
    var VacancyDAO = require('../DAO/vacanciesDAO');
    var EmailsTokensDAO = require('../DAO/emailTokensDAO');
    var sparkPost = require('../service/sparkPost');

    function create(context, business)
    {
        function resendEmailToActiveSubscribe(id)
        {
            return security.requireAdmin(context).then(function ()
            {
                return SubscribeDAO.get(id).then(function (subscribe)
                {
                    return sparkPost.createTokenAndSendEmail(subscribe.token, subscribe.email, 'subscription');
                });
            });

        }

        function resendEmailToActiveVacancy(id)
        {
            return security.requireAdmin(context).then(function ()
            {
                return VacancyDAO.get(id).then(function (vacancy)
                {
                    return EmailsTokensDAO.getByOriginId(id).then(function (results)
                    {
                        return sparkPost.createTokenAndSendEmail(results.id, vacancy.email, 'vacancy', vacancy);
                    });
                });
            });
        }

        return {
            resendEmailToActiveSubscribe: resendEmailToActiveSubscribe,
            resendEmailToActiveVacancy: resendEmailToActiveVacancy
        };
    }

    module.exports = {
        create: create
    };
})();
