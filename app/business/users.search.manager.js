(function ()
{
    'use strict';

    var Promise = require('bluebird');
    var solrHelper = require('../service/solrHelper');
    var languagesDAO = require('../DAO/languagesDAO');
    var phrasesDAO = require('../DAO/phrasesDAO');

    function create(context, business)
    {

        function addLangCode(langcodes, code, level)
        {
            for (var i = 2; i >= level; i--) {
                if(!langcodes[code]) {
                    langcodes[code]=[];
                }
                langcodes[code].push(i);
            }

            return langcodes;
        }

        function singleLanguage(langcodes, langs, level)
        {
            langs = Array.isArray(langs) ? langs : [langs];

            return languagesDAO.getCodesByIds(langs).then(function (results) {
                for (var i in results) {
                    langcodes = addLangCode(langcodes, results[i], level);
                }
            });
        }

        function prepareLanguages(query)
        {

            var langcodes = {}
            var promises = [];

            return new Promise(function (resolve, reject) {
                if (!(query.l0 || query.l1 || query.l2)) {
                    resolve();
                    return;
                }

                if (query.l0) {
                    promises.push(singleLanguage(langcodes, query.l0, 0));
                }

                if (query.l1) {
                    promises.push(singleLanguage(langcodes, query.l1, 1));
                }

                if (query.l2) {
                    promises.push(singleLanguage(langcodes, query.l2, 2));
                }

                Promise.all(promises).then(function () {
                    resolve(langcodes);
                });

            });
        }

        function find(query) {

            var finalResults = {};
            return new Promise(function (resolve, reject) {
                phrasesDAO.getTranslationsForTitle(query.resumeTitle).then(function (phrases) {
                   
                    prepareLanguages(query).then(function (langcodes) {
                        solrHelper.queryApplication(query, {
                            fieldName: 'dataJson',
                            resultProcessCallback: solrHelper.common.filterJsonCallback,
                            langcodes: langcodes,
                            phrases: phrases
                        }).then(function (solrData) {
                            finalResults['results'] = solrData.data;
                            finalResults['total'] = solrData.count;
                            resolve(finalResults);
                        });
                    });
                });


            });
        }

        return {
            find: find
        };

    }
    module.exports = {
        create: create
    };
})();

