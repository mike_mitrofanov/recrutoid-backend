(function ()
{
    'use strict';

    var applicationException = require('../service/applicationException'),
            VacanciesDAO = require('../DAO/vacanciesDAO'),
            security = require('./security'),
            UserDAO = require('../DAO/usersDAO'),
            sparkPost = require('../service/sparkPost'),
            emailTokensDAO = require('../DAO/emailTokensDAO');

    function create(context, business)
    {
        function query()
        {
            return VacanciesDAO.queryDistributors().then(function (arrayUserId)
            {
                return UserDAO.queryUsersByArrayId({$in: arrayUserId});
            });
        }

        function verifyDistributorsByAdmin(user)
        {
            return security.requireAdmin(context).then(function ()
            {
                return UserDAO.createNewOrUpdate(user);
            });
        }

        function remove(userId)
        {
            function removeAccount()
            {
                return UserDAO.remove(userId).then(function ()
                {
                    return VacanciesDAO.removeByUserId(userId);
                });
            }

            return security.requireUser(context).then(function ()
            {
                if (security.isAdmin(context) || context.user.id === userId) {
                    return removeAccount();
                } else {
                    throw applicationException.new(applicationException.FORBIDDEN);
                }
            });
        }

        function createTokenAndSendEmail(user)
        {
            return emailTokensDAO.create(user.id, 'user').then(function (token)
            {
                return sparkPost.createTokenAndSendEmail(token.token, user.email, 'account');
            });
        }

        function resendEmail(userId)
        {
            return security.requireAdmin(context).then(function ()
            {
                return UserDAO.get({_id: userId}).then(function (user)
                {
                    return createTokenAndSendEmail(user);
                });
            });
        }

        function verifyDistributorsByToken(token)
        {
            return emailTokensDAO.get(token).then(function (results)
            {
                return UserDAO.activateAccount(results.originId).then(function (user)
                {
                    if (!user) {
                        emailTokensDAO.remove(token);
                        throw applicationException.new(applicationException.NOT_FOUND, 'Account not exist');
                    }
                    return emailTokensDAO.remove(token);
                });
            });
        }

        return {
            query: query,
            remove: remove,
            resendEmail: resendEmail,
            verifyDistributorsByAdmin: verifyDistributorsByAdmin,
            verifyDistributorsByToken: verifyDistributorsByToken
        };
    }

    module.exports = {
        create: create
    };
})();
