(function ()
{
    'use strict';

    var security = require('./security');
    var applicationException = require('../service/applicationException');
    var Promise = require('bluebird');
    var _ = require('lodash');
    var sparkPost = require('../service/sparkPost');

    function create(context, business)
    {
        function getHomeData()
        {
            var result = {};
            return business.getCategoriesManager(context).getAllCategories().then(function (data)
            {
                result.categories = data;
                return business.getCitiesManager(context).getAllCities(true);
            }).then(function (data)
            {
                result.cities = data;
                return business.getCitiesManager(context).getAllCities();
            }).then(function (data)
            {
                result.citiesForSubscription = data;
                return business.getSalaryRangesManager(context).getAllSalaryRanges();
            }).then(function (data)
            {
                result.salaryRanges = data;
                return business.getSettingsManager(context).getStatistics();
            }).then(function (data)
            {
                result.statistics = data;
                return business.getCmsManager(context).getNews();
            }).then(function (data)
            {
                result.news = data;
                return result;
            });
        }

        function getNavBarData()
        {
            var result = {};
            return business.getCmsManager(context).getPagesList().then(function (data)
            {
                result.pages = data;
                if(security.isEmployer(context)) {
                    return business.getVacanciesManager(context).getNumberOfNewApplications();
                }
                else if(security.isJobSeeker(context)) {
                    return business.getUsersManager(context).getNewResumeViewsCount();
                }
            }).then(function (sum)
            {
                if(sum && sum.hasOwnProperty('count')) {
                    result.newApplicationsOrCvViews = sum.count;
                }

                if(security.isJobSeeker(context)) {
                    return business.getUsersManager(context).getOffers({seen: true}).then(function(offers){
                        result.newOffers = 0;
                        _.each(offers.results, function(offer){
                            if (!offer.seen){
                                result.newOffers++;
                            }
                        });
                        return getNotifications(result);
                    });
                }

                if(security.isEmployer(context)) {
                    return business.getUsersManager(context).getOffers({seen: true}).then(function(offers){
                        result.newOffers = 0;
                        _.each(offers.results, function(offer){
                            if (!offer.seen){
                                result.newOffers++;
                            }
                        });
                        return getNotifications(result);
                    });
                }

                if(security.isEmployer(context)) {
                    return getNotifications(result);
                }
                return result;
            })
        }

        function getNotifications(result) {
            return business.getUsersManager(context).getNotifications().then(function(notifications){
                result.notifications = notifications;
                return result;
            });
        }

        function isProfileActive()
        {
            var result = {};

            return security.requireUser(context).then(function () {
                if (security.isEmployer(context)) {
                    result.isActive = Boolean(business.getUsersManager(context).isEmployerProfileActive(context.user));
                }
                if (security.isJobSeeker(context)) {
                    result.isActive = Boolean(business.getUsersManager(context).isSeekerProfileActive(context.user));
                    result.isInModeration = Boolean(business.getUsersManager(context).isSeekerProfileInModeration(context.user));
                }

                return result;
            });
        }

        function getEditResumeData()
        {
            var result = {};
            return security.requireUser(context).then(function ()
            {
                return business.getLanguagesManager(context).query();
            }).then(function (languages)
            {
                result.languages = languages;
                return business.getCategoriesManager(context).getAllCategories();
            }).then(function (categories)
            {
                result.categories = categories;
                return business.getSalaryRangesManager(context).getAllSalaryRanges();
            }).then(function (salaryRanges)
            {
                result.salary_ranges = salaryRanges;
                return business.getCitiesManager(context).getAllCities();
            }).then(function (cities)
            {
                result.cities = cities;
                return business.getUsersManager(context).getResume(context.user.id);
            }).then(function (cv)
            {
                result.cv = cv;
                return result;
            });
        }

        function sendCustomEmail(emailBody)
        {
            var allCollections = [];
            return Promise.resolve().then(function ()
            {
                switch(emailBody.addressees)
                {
                    case 'subscriber': return business.getSubscriptionsManager(context).getAll({verified: true});
                    case 'employer': return business.getUsersManager(context).getAll({role: 'employer', verified: true, banned: false});
                    case 'normal': return business.getUsersManager(context).getAll({role: 'normal', verified: true, banned: false});
                    case 'admin': return business.getUsersManager(context).getAll({role: 'admin', verified: true, banned: false});
                    case 'all':
                        return business.getSubscriptionsManager(context).getAll({verified: true, banned: false}).then(function (collection)
                        {
                            allCollections = collection;
                            return business.getUsersManager(context).getAll({verified: true, banned: false})
                        });
                }

                throw applicationException.new(applicationException.METHOD_NOT_ALLOWED, 'Wrong addressees');
            }).then(function (collection)
            {
                allCollections = allCollections.concat(collection);
                // each type of collections must contain property email
                if(allCollections.length) {

                    // ensure that emails are unique
                    allCollections = _.pluck(allCollections, 'email').filter(function(elem, index, self) {
                        return index == self.indexOf(elem);
                    });

                    return sparkPost.customMessage(allCollections, emailBody.title, emailBody.content);
                }
            });
        }

        function getSearchVacanciesData()
        {
            var result = {};
            return business.getCategoriesManager(context).getAllCategories().then(function (data)
            {
                result.categories = data;
                return business.getCitiesManager(context).getAllCities(true);
            }).then(function (data)
            {
                result.cities = data;
                return result;
            });
        }

        function getSearchUsersData()
        {
            var result = {};
            return business.getCategoriesManager(context).getAllCategories().then(function (data)
            {
                result.categories = data;
                return business.getCitiesManager(context).getAllCities();
            }).then(function (data)
            {
                result.cities = data;
                return business.getLanguagesManager(context).query();
            }).then(function (data)
            {
                result.languages = data.results;
                return result;
            });
        }

        return {
            getHomeData: getHomeData,
            getNavBarData: getNavBarData,
            isProfileActive: isProfileActive,
            getEditResumeData: getEditResumeData,
            sendCustomEmail: sendCustomEmail,
            getSearchVacanciesData: getSearchVacanciesData,
            getSearchUsersData: getSearchUsersData
        };
    }

    module.exports = {
        create: create
    };
})();
