(function ()
{
    'use strict';

    var salaryRangesDAO = require('../DAO/salaryRangesDAO');
    var vacanciesDAO = require('../DAO/vacanciesDAO');
    var subscriptionsDAO = require('../DAO/subscriptionsDAO');
    var usersDAO = require('../DAO/usersDAO');
    var applicationException = require('../service/applicationException');
    var Promise = require('bluebird');
    var security = require('./security');
    var _ = require('lodash');

    function create(context)
    {
        function getAllSalaryRanges()
        {
            return salaryRangesDAO.query();
        }

        function createNewOrUpdate(salaryRange)
        {
            return security.requireAdmin(context).then(function ()
            {
                if (!salaryRange.id) {
                    return salaryRangesDAO.get({name: salaryRange.name}).then(function (result)
                    {
                        if (result) {
                            return Promise.reject(applicationException.CONFLICT);
                        }

                        return salaryRangesDAO.createNewOrUpdate(salaryRange);
                    });
                }
                return salaryRangesDAO.createNewOrUpdate(salaryRange);
            });
        }

        function updateSalaryRangeEverywhere(id, toReplace)
        {
            var promises;
            return vacanciesDAO.query({salaryRangeId: id}).then(function (vacancies)
            {
                promises = [];
                _.map(vacancies.results, function (elem)
                {
                    promises.push(vacanciesDAO.update({_id: elem.id}, {salaryRangeId: toReplace}));
                });

                return Promise.all(promises).then(function ()
                {
                    return subscriptionsDAO.search({salaryRange: id});
                });
            }).then(function (subscriptions)
            {
                promises = [];
                _.map(subscriptions, function (elem)
                {
                    elem.salaryRange = toReplace;
                    promises.push(subscriptionsDAO.update({_id: elem.id}, elem));
                });

                return Promise.all(promises).then(function ()
                {
                    return usersDAO.search({salaryRange: id});
                });
            }).then(function (users)
            {
                promises = [];
                _.map(users, function (elem)
                {
                    promises.push(usersDAO.update({_id: elem.id}, {salaryRange: toReplace}));
                });

                return Promise.all(promises);
            });
        }

        function remove(id, toReplace)
        {
            return security.requireAdmin(context).then(function ()
            {
                return updateSalaryRangeEverywhere(id, toReplace);
            }).then(function ()
            {
                return salaryRangesDAO.remove(id);
            });
        }

        return {
            createNewOrUpdate: createNewOrUpdate,
            getAllSalaryRanges: getAllSalaryRanges,
            remove: remove
        };
    }

    module.exports = {
        create: create
    };
})();
