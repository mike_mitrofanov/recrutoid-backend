(function ()
{
    'use strict';
    var dateHelper = require('../service/dateHelper');
    var emailErrorsDAO = require('../DAO/emailErrorsDAO');

    function create(context, business)
    {
        function startInterval()
        {
            setTimeout(runInterval, dateHelper.getMoment().startOf('day').add(1, 'day').add(12, 'hours').diff(dateHelper.getMoment()));
            function runInterval() {
                setInterval(function ()
                {
                    business.getVacanciesManager(context).removeOldDistributions();
                    business.getVacanciesManager(context).removeOldVacancy();
                    business.getVacanciesManager(context).dispatchVacancies(false);
                    business.getVacanciesManager(context).sendReminderVacancy().catch(business.getEmailErrorsManager(context).logErrors);
                    business.getUsersManager(context).sendApplyNotifications().catch(business.getEmailErrorsManager(context).logErrors);
                    business.getUsersManager(context).removeOldProfile();
                    business.getUsersManager(context).sendReminderProfile().catch(business.getEmailErrorsManager(context).logErrors);
                }, 86400000);
            }
        }

        return {
            startInterval: startInterval
        };
    }

    module.exports = {
        create: create
    };

})();
