(function ()
{
    'use strict';

    var citiesDAO = require('../DAO/citiesDAO');
    var vacanciesDAO = require('../DAO/vacanciesDAO');
    var subscriptionsDAO = require('../DAO/subscriptionsDAO');
    var usersDAO = require('../DAO/usersDAO');
    var applicationException = require('../service/applicationException');
    var Promise = require('bluebird');
    var security = require('./security');
    var _ = require('lodash');

    function create(context)
    {
        function getAllCities(showOilGasCamp)
        {
            return citiesDAO.query(showOilGasCamp);
        }

        function createNewOrUpdate(city)
        {
            return security.requireAdmin(context).then(function ()
            {
                if (!city.id) {
                    return citiesDAO.get({name: city.name}).then(function (result)
                    {
                        if (result) {
                            return Promise.reject(applicationException.CONFLICT);
                        }

                        return citiesDAO.createNewOrUpdate(city);
                    });
                }
                return citiesDAO.createNewOrUpdate(city);
            });
        }

        function updateCityEverywhere(id, toReplace)
        {
            var promises;
            return vacanciesDAO.query({cityId: id}).then(function (vacancies)
            {
                promises = [];
                _.map(vacancies.results, function (elem)
                {
                    promises.push(vacanciesDAO.update({_id: elem.id}, {cityId: toReplace}));
                });

                return Promise.all(promises).then(function ()
                {
                    return subscriptionsDAO.search({city: id});
                });
            }).then(function (subscriptions)
            {
                promises = [];
                _.map(subscriptions, function (elem)
                {
                    elem.city = toReplace;
                    promises.push(subscriptionsDAO.update({_id: elem.id}, elem));
                });

                return Promise.all(promises).then(function ()
                {
                    return usersDAO.search({city: id});
                });
            }).then(function (users)
            {
                promises = [];
                _.map(users, function (elem)
                {
                    promises.push(usersDAO.update({_id: elem.id}, {city: toReplace}));
                });

                return Promise.all(promises);
            });
        }

        function remove(id, toReplace)
        {
            return security.requireAdmin(context).then(function ()
            {
                return updateCityEverywhere(id, toReplace);
            }).then(function ()
            {
                return citiesDAO.remove(id);
            });
        }

        return {
            createNewOrUpdate: createNewOrUpdate,
            getAllCities: getAllCities,
            remove: remove
        };
    }

    module.exports = {
        create: create
    };
})();
