(function ()
{
    'use strict';

    var bugreportsDAO = require('../DAO/bugreportsDAO');
    var vacanciesDAO = require('../DAO/vacanciesDAO');
    var subscriptionsDAO = require('../DAO/subscriptionsDAO');
    var usersDAO = require('../DAO/usersDAO');
    var daoFilterHelper = require('../util/daoFilterHelper');
    var applicationException = require('../service/applicationException');
    var Promise = require('bluebird');
    var security = require('./security');
    var _ = require('lodash');

    function create(context)
    {
        function search(filter)
        {
            return security.requireAdmin(context).then(function ()
            {
                var query = {};
                daoFilterHelper.copy(query, filter, 'email');

                query.from = filter.from;
                query.size = filter.size;

                return bugreportsDAO.query(query);
            });
        }

        function createNew(bugreport)
        {
            return bugreportsDAO.createNew(bugreport);
        }

        function remove(id)
        {
            return security.requireAdmin(context).then(function ()
            {
                return bugreportsDAO.remove(id);
            });
        }

        return {
            createNew: createNew,
            search: search,
            remove: remove
        };
    }

    module.exports = {
        create: create
    };
})();
