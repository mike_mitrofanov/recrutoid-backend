(function ()
{
    'use strict';

    var emailErrorsDAO = require('../DAO/emailErrorsDAO');
    var security = require('./security');

    function create(context)
    {
        function search(filter)
        {
            return security.requireAdmin(context).then(function ()
            {
                var query = {};

                query.from = filter.from;
                query.size = filter.size;

                return emailErrorsDAO.query(query);
            });
        }

        function remove(id)
        {
            return security.requireAdmin(context).then(function ()
            {
                return emailErrorsDAO.remove(id);
            });
        }

        function logErrors(error) {
            if (error.name == 'SparkPostError') {
                emailErrorsDAO.createNew({
                    name: error.name,
                    message: error.message,
                    description: error.errors[0].description,
                    code: error.errors[0].code,
                    stack: error.stack
                });
            }else{
                emailErrorsDAO.createNew({
                    name: 'ServerError',
                    message: error.message,
                    description: error.name,
                    code: 500,
                    stack: error.stack
                });
            }
        }

        return {
            search: search,
            remove: remove,
            logErrors: logErrors
        };
    }

    module.exports = {
        create: create
    };
})();
