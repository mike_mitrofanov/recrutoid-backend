(function ()
{
    'use strict';

    var sha1 = require('sha1');
    var sizeOf = require('image-size');
    var applicationException = require('../service/applicationException');
    var _ = require('lodash');
    var Promise = require('bluebird');
    var security = require('./security.js');
    var resetPasswordTokenDAO = require('../DAO/resetPasswordTokenDAO');
    var activateAccountTokenDAO = require('../DAO/actiavateAccountToken');
    var EmailTokensDAO = require('../DAO/emailTokensDAO');
    var VacancyDAO = require('../DAO/vacanciesDAO');
    var SubscriptionsDAO = require('../DAO/subscriptionsDAO');
    var usersDAO = require('../DAO/usersDAO');
    var passwordsDAO = require('../DAO/passwordsDAO');
    var tokensDAO = require('../DAO/tokensDAO');
    var UsersPermissionsDAO = require('../DAO/usersPermissionsDAO');
    var daoFilterHelper = require('../util/daoFilterHelper.js');
    var cloudinary = require('../service/cloudinary.js');
    var sparkPost = require('../service/sparkPost');
    var dateHelper = require('../service/dateHelper');
    var request = require('request-promise');
    var config = require('../config/config');
    var jade = require('jade');
    var pdf = require('html-pdf');
    var fs = require('fs');
    var solrHelper = require('../service/solrHelper');
    var cvfilesDAO = require('../DAO/cvfilesDAO');

    function create(context, business)
    {
        function authenticate(email, password)
        {
            return usersDAO.get({email: email}, {email: 1, banned: 1, firstLogin: 1, verified: 1, socials: 1, hidden: 1}).then(function (user)
            {
                var token;

                if (!user) {
                    throw applicationException.new(applicationException.BAD_REQUEST, 'User does not exist');
                }

                if (user.hasOwnProperty('socials')) {
                    user.socialOnly = user.socials.socialOnly;
                }

                if (user.socialOnly) {
                    throw applicationException.new(applicationException.CONFLICT, 'Registered using social account!');
                }

                if (user.banned) {
                    throw applicationException.new(applicationException.METHOD_NOT_ALLOWED, 'banned');
                }

                if (user.hidden) {
                    throw applicationException.new(applicationException.METHOD_NOT_ALLOWED, 'deleted');
                }

                if (!user.verified) {
                    throw applicationException.new(applicationException.PRECONDITION_FAILED, 'Account not activated');
                }
                return passwordsDAO.checkUserPassword(user.id, sha1(password)).then(function (results)
                {
                    if (results) {
                        token = tokensDAO.create(user.id);
                    } else {
                        throw applicationException.new(applicationException.UNAUTHORIZED, 'User and password does not match');
                    }

                }).then(function(){
                    if (user.firstLogin){
                        usersDAO.update({_id: user.id}, {
                            firstLogin: false
                        })
                    }

                    return token;
                });
            });
        }

        function getOrCreateAccountFromSocialAuthentication(profile, socialType)
        {
            var query = {$or: [{}]};
            query.$or[0]['socials.' + socialType] = profile.id;
            if (profile.email) {
                query.$or.push({email: profile.email});
            }

            var userId;
            return usersDAO.get(query, {socials: 1, firstLogin: 1}).then(function (existingUser)
            {
                if (!existingUser) {

                    var newUser = {
                        /*jshint camelcase: false */
                        email: profile.email,
                        firstName: profile.firstName,
                        lastName: profile.lastName,
                        thumbnail: profile.thumbnail,
                        role: 'normal',
                        socials: {socialOnly: true},
                        verified: true
                    };
                    newUser.socials[socialType] = profile.id;

                    return registerAnyUser(newUser, true).then(function (user)
                    {
                        userId = user.id;
                    });
                }

                userId = existingUser.id;
                existingUser.socials = existingUser.socials || {};

                if (!existingUser.socials.hasOwnProperty(socialType) || !existingUser.verified) {
                    existingUser.socials[socialType] = profile.id;
                    return usersDAO.update({_id: existingUser.id}, {
                        socials: existingUser.socials,
                        verified: true
                    }).then(function(){
                        console.log(existingUser);
                        if (existingUser.firstLogin){
                            usersDAO.update({_id: existingUser.id}, {
                                firstLogin: false
                            })
                        }
                    });
                }

            }).then(function ()
            {
                return userId;
            });
        }

        function authenticateByFacebook(data)
        {
            var requestQuery = {
                url: 'https://graph.facebook.com/v2.4/oauth/access_token',
                qs: {
                    /*jshint camelcase: false */
                    code: data.code,
                    client_id: data.clientId,
                    client_secret: config.FACEBOOK_SECRET,
                    redirect_uri: data.redirectUri
                },
                json: true
            };

            return request.get(requestQuery).then(function (accessToken)
            {
                requestQuery = {
                    url: 'https://graph.facebook.com/v2.4/me?fields=id,email,first_name,last_name',
                    qs: accessToken,
                    json: true
                };

                return request.get(requestQuery);

            }).catch(function ()
            {
                return Promise.reject(applicationException.new(applicationException.PRECONDITION_FAILED));
            }).then(function (profile)
            {
                profile.firstName = profile.first_name;
                profile.lastName = profile.last_name;
                profile.thumbnail = 'http://graph.facebook.com/' + profile.id + '/picture?width=200&height=200';

                return getOrCreateAccountFromSocialAuthentication(profile, 'facebook');
            }).then(function (userId)
            {
                return usersDAO.get({_id: userId}).then(function (user)
                {
                    if (user.banned) {
                        throw applicationException.new(applicationException.METHOD_NOT_ALLOWED, 'You have been banned. Contact support to get more info.');
                    }
                    return tokensDAO.create(user.id);
                });
            });
        }

        function authenticateByLinkedIn(data)
        {
            var params = {
                /*jshint camelcase: false */
                code: data.code,
                client_id: data.clientId,
                client_secret: config.LINKEDIN_SECRET,
                redirect_uri: data.redirectUri, // TODO: unneeded?
                grant_type: 'authorization_code'
            };

            return request.post('https://www.linkedin.com/uas/oauth2/accessToken', {form: params, json: true}).then(function (token)
            {
                var requestQuery = {
                    url: 'https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address,picture-url)',
                    qs: {
                        oauth2_access_token: token.access_token,
                        format: 'json'
                    },
                    json: true
                };

                return request.get(requestQuery);

            }).catch(function ()
            {
                return Promise.reject(applicationException.new(applicationException.PRECONDITION_FAILED));
            }).then(function (profile)
            {
                profile.email = profile.emailAddress;
                profile.thumbnail = profile.pictureUrl;

                return getOrCreateAccountFromSocialAuthentication(profile, 'linkedin');
            }).then(function (userId)
            {
                return usersDAO.get({_id: userId}).then(function (user)
                {
                    if (user.banned) {
                        throw applicationException.new(applicationException.METHOD_NOT_ALLOWED, 'You have been banned. Contact support to get more info.');
                    }
                    return tokensDAO.create(user.id);
                });
            });
        }

        function impersonate(userId)
        {
            return security.requireAdmin(context).then(function ()
            {
                return usersDAO.get({_id: userId});
            }).then(function (user)
            {
                if (!user) {
                    throw applicationException.new(applicationException.NOT_FOUND, 'User does not exist');
                }

                return tokensDAO.create(user.id);
            });
        }

        function getProfile()
        {
            return security.requireUser(context).then(function ()
            {
                var userProjection = {
                    firstName: 1,
                    lastName: 1,
                    email: 1,
                    newEmail: 1,
                    hidden: 1,
                    hiddenOnSearch: 1,
                    phone: 1,
                    thumbnail: 1,
                    city: 1,
                    role: 1,
                    banned: 1,
                    socials: 1,
                    showOnHomePage: 1
                };

                var subscriptionProjection = {
                    categories: 1,
                    city: 1,
                    desiredSalaryRange: 1,
                    interval: 1
                };

                return usersDAO.get({_id: context.user.id}, userProjection).then(function (data)
                {
                    if (!data) {
                        return Promise.reject(applicationException.new(applicationException.NOT_FOUND));
                    }
                    if (data.banned && !security.isAdmin(context)) {
                        //TODO: maybe should return 403 (FORBIDDEN)
                        return Promise.reject(applicationException.new(applicationException.CONFLICT));
                    }

                    if (data.hasOwnProperty('socials')) {
                        data.socialOnly = data.socials.socialOnly;
                    }

                    delete data.banned;
                    delete data.resume;

                    return SubscriptionsDAO.get({email: data.email}, subscriptionProjection).then(function (subscription)
                    {
                        data.subscription = subscription;
                        return data;
                    }).catch(function ()
                    {
                        return Promise.resolve(data);
                    });


                });
            });
        }

        function checkIfTriedToChangeEmail(profile)
        {
            var newEmail;
            return Promise.resolve().then(function ()
            {
                if (context.user.email !== profile.email) {
                    newEmail = profile.email;
                    profile.email = context.user.email;
                    return usersDAO.get({email: newEmail}).then(function (existingUser)
                    {
                        if (existingUser) {
                            throw applicationException.new(applicationException.CONFLICT, 'Email already registered');
                        }
                    });
                }
            }).then(function ()
            {
                if (newEmail && context.user.role === 'employer') {
                    return business.getBlacklistedDomainsManager(context).rejectIfBlacklisted(newEmail);
                }
            }).then(function ()
            {
                if (newEmail) {
                    return EmailTokensDAO.create(context.user.id, 'emailChange', null, newEmail).then(function (token)
                    {
                        sparkPost.changeEmail(token.token, newEmail).catch(business.getEmailErrorsManager(context).logErrors).catch(business.getEmailErrorsManager(context).logErrors);
                        return usersDAO.update(context.user.id, {$set: {newEmail: newEmail}});
                    });
                }
            });
        }

        function updateProfile(payload)
        {
            function update()
            {
                var subscription;
                return usersDAO.get({email: context.user.email}).then(function (user)
                {
                    if (user && context.user.id.toString() !== user.id.toString()) {
                        return Promise.reject(applicationException.new(applicationException.CONFLICT));
                    }

                    if (payload.profile.subscription) {
                        subscription = payload.profile.subscription;
                        delete payload.profile.subscription;
                    }

                }).then(function ()
                {
                    return checkIfTriedToChangeEmail(payload.profile);
                }).then(function ()
                {
                    if (context.user.role === 'employer') {
                        // email has been checked in order to be blacklisted in checkIfTriedToChangeEmail method
                        var data = {};
                        if (context.user.firstName !== payload.profile.firstName && payload.profile.firstName && payload.profile.firstName.length) {
                            data.companyName = payload.profile.firstName;
                        }
                        if (context.user.lastName !== payload.profile.lastName && payload.profile.lastName && payload.profile.lastName.length) {
                            data.shortDescription = payload.profile.lastName;
                        }
                        if (data.companyName || data.shortDescription) {
                            return VacancyDAO.changeCompanyNameAndDescription(data, payload.profile.id).then(function ()
                            {
                                solrHelper.vacancyHandler.pushByUser(context.user);
                            });
                        }
                    }
                }).then(function ()
                {
                    if (payload.hasOwnProperty('password')) {
                        return passwordsDAO.checkUserPassword(payload.profile.id, sha1(payload.password.current)).then(function (results)
                        {
                            if (!results) {
                                return Promise.reject(applicationException.new(applicationException.PRECONDITION_FAILED,
                                        'User and password does not match'));
                            }

                            return passwordsDAO.createOrUpdate(payload.profile.id, sha1(payload.password.new)).then(function () {
                                return sparkPost.passwordChange(context.user.email, payload.password.new).catch(business.getEmailErrorsManager(context).logErrors);
                            });
                        });
                    }
                }).then(function ()
                {
                    // this deletion stands for prevention from unwanted erase user resume data while updating profile
                    delete payload.profile.resume;
                    return usersDAO.createNewOrUpdate(payload.profile);
                }).then(function ()
                {
                    if (subscription) {
                        subscription.categories = _.map(subscription.categories, 'id');

                        subscription.city = subscription.city?subscription.city.id:null;
                        subscription.desiredSalaryRange = subscription.desiredSalaryRange?subscription.desiredSalaryRange.id:null;
                        return SubscriptionsDAO.createNewOrUpdate(subscription, true).then(function (oldSub)
                        {
                            oldSub.categories = _.map(oldSub.categories, function (elem)
                            {
                                return {id: elem};
                            });
                            oldSub.city = {id: oldSub.city};

                            return SubscriptionsDAO.get({_id: oldSub.id});
                        }).then(function (newSub)
                        {

                            return newSub;
                        });
                    }
                }).then(function () {
                    solrHelper.pushApplication(context.user.id+"", true);

                });
            }

            return security.requireAdmin(context).catch(function ()
            {
                return security.requireUser(context).then(function ()
                {
                    if (payload.profile.id !== context.user.id.toString()) {
                        return Promise.reject(applicationException.new(applicationException.FORBIDDEN));
                    }
                });
            }).then(update);

        }

        function remove(id)
        {
            var removedUser;
            return security.requireAdmin(context).then(function ()
            {
                solrHelper.dropApplication(id);
                return usersDAO.remove(id, context);
            }).then(function ()
            {
                return passwordsDAO.remove(id);
            }).then(function (user)
            {
                removedUser = user;
                return VacancyDAO.removeAllApplications(id);
            }).then(function ()
            {
                return VacancyDAO.removeByUserId(id);
            }).then(function ()
            {
                return usersDAO.removeAllViewsOfUser(id);
            }).then(function ()
            {
                return usersDAO.removeFromSeenByUserId(id);
            }).then(function ()
            {
                return SubscriptionsDAO.remove({'$or': [{_id: id}, {email: removedUser.email}]});
            }).catch(function ()
            {
                if (!security.isAuthenticated(context) || id !== context.user.id.toString()) {
                    return Promise.reject(applicationException.new(applicationException.FORBIDDEN));
                }
                return removeByUser(id);
            });
        }

        function unremove(id)
        {
            return security.requireUser(context).then(function ()
            {
                return usersDAO.update(id, {$set: {extendDate: dateHelper.get()}});
            });
        }

        function removeByUser(id) {
            var oneDay = (24 * 3600000);

            var userExpirationTime;
            return business.getSettingsManager(context).get('userExpirationTime').then(function (settings) {
                userExpirationTime = settings.userExpirationTime;

                return usersDAO.update(id, {$set: {extendDate: (dateHelper.get() - userExpirationTime) + (oneDay * 8)}}).then(function () {
                    return usersDAO.createExtendToken(context.user.id).then(function (token)
                    {
                        return sparkPost.accountDeactivation(context.user, token.id).catch(business.getEmailErrorsManager(context).logErrors);
                    })
                });
            });
        }

        function uploadAvatar(request)
        {
            var urlData;
            return security.requireUser(context).then(function ()
            {
                return cloudinary.upload(null, request, function (fields, files)
                {
                    var transformation = {
                        crop: 'scale'
                    };

                    var dimensions = sizeOf(files.file.path);
                    var transformationType = dimensions.width < dimensions.height ? 'height' : 'width';
                    transformation[transformationType] = 400;

                    /*jshint -W106*/
                    return {
                        overwrite: true,
                        public_id: 'avatar-' + context.user.id,
                        resource_type: 'image',
                        transformation: transformation
                    };
                });
            }).then(function (data)
            {
                urlData = data;
                return usersDAO.update(context.user.id, {$set: {thumbnail: data.url}});
            }).then(function ()
            {
                return urlData;
            });
        }

        function uploadCv(request)
        {
            var urlData;
            var fileName;
            var cv;
            var fileExtension;
            return security.requireUser(context).then(function ()
            {
                return cloudinary.upload(null, request, function (fields)
                {
                    cv = fields.cv;
                    fileExtension = fields.fileExtension

                    fileName = 'cv-' + context.user.id;
                    /*jshint -W106*/
                    return {
                        overwrite: true,
                        resource_type: 'auto',
                        public_id: fileName
                    }
                });
                // return usersDAO.model.find({role:'admin'});
            }).then(function (data)
            {
                urlData = data;

                return usersDAO.model.find({role: 'admin'}).then(function (results) {
                    var emails = _.pluck(results, 'email');
                    sparkPost.newCvCheckUpNotification(emails);
                });
            }).then(function ()
            {
                return cvfilesDAO.model.createAsync({
                    email: context.user.email,
                    file: {
                        name: fileName + '.' + fileExtension,
                        url: urlData.url,
                        extension: fileExtension
                    }
                }).then(function (res) {
                    parseCvInfoAndUpdateProfile(JSON.parse(cv), res._id);
                });
            });
        }

        function parseCvInfoAndUpdateProfile(cv, cvFileId) {
            var user = {resume: {}, uploadedResume: {}};
            user.categories = [cv.category.id];
            user.city = cv.city.id;
            user.resume.title = [cv.title];
            user.resume.desiredSalaryRange = [cv.desiredSalaryRange.id];
            user.cancelCvExpireTime = dateHelper.get() + (5 * 60000); //5 minutes expiration time;
            user.uploadedResume.data = cvFileId;
            user.uploadedResume.messageSent = false;

            return usersDAO.update({_id: context.user.id}, user);
        }

        function cancelCvCheckup(request) {
            var isImpersonated = request.body.is_impersonated;
            return security.requireUser(context).then(function ()
            {
                if (context.user.cancelCvExpireTime >= dateHelper.get() || isImpersonated) {
                    var userData = {};
                    if (!isImpersonated) {
                        userData = {
                            city: null,
                            categories: [],
                            uploadedResume: null,
                            resume: {
                                desiredSalaryRange: null,
                                title: null
                            }
                        };

                        return cvfilesDAO.model.remove({email: context.user.email}).then(function () {
                            return usersDAO.update({_id: context.user.id}, userData);
                        });
                    } else {
                        return usersDAO.update({_id: context.user.id}, {$set: {'uploadedResume.data': null, 'uploadedResume.messageSent': false}});
                    }

                } else {
                    throw applicationException.new(applicationException.PRECONDITION_FAILED, 'Expiration time reached');
                }
            });
        }

        function changeApplicationsNotifications(value)
        {
            return security.requireEmployer(context).then(function ()
            {
                return usersDAO.update({_id: context.user.id}, {applicationsNotificationsInterval: value.applicationsNotifications});
            });
        }

        function sendApplyNotifications()
        {
            // applicationsNotificationsInterval
            var ANIValues = ['daily'];

            var dayOfWeek = parseInt(dateHelper.getMoment().format('E'), 10);
            if (6 === dayOfWeek) {
                ANIValues.push('weekly');
            }

            return security.requireAdmin(context).then(function ()
            {
                return usersDAO.search({
                    role: 'employer',
                    applicationsNotificationsInterval: {$in: ANIValues}
                }, {});
            }).then(function (users)
            {
                users = _.pluck(users, 'id');
                for (var index in users)
                {
                    if (users.hasOwnProperty(index)) {
                        users[index] = users[index].toString();
                    }
                }
                return VacancyDAO.query({
                    publishOnWebsite: true,
                    applicants: {$not: {$size: 0}, $elemMatch: {seen: false}},
                    userId: {$in: users}
                });
            }).then(function (vacancies)
            {
                _.forEach(vacancies.results, function (vacancy)
                {
                    _.forEach(vacancy.applicants, function (applicant)
                    {
                        usersDAO.get({_id: applicant}).then(function (user)
                        {
                            sparkPost.applyNotification(vacancy, user).catch(business.getEmailErrorsManager(context).logErrors);
                        });
                    });
                });
            });
        }

        function resendRegistrationEmail(id)
        {
            return security.requireAdmin(context).then(function () {
                resendEmail(id).catch(function (error)
                {
                    business.getEmailErrorsManager(context).logErrors(error);
                    if (applicationException.is(error, applicationException.NOT_FOUND) && security.isAdmin(context)) {
                        return Promise.resolve();
                    } else {
                        throw error;
                    }
                });
            }).catch(function () {
                return security.requireUser(context).then(function () {
                    var newEmail = context.user.newEmail;
                    return EmailTokensDAO.create(context.user.id, 'emailChange', null, newEmail).then(function (token)
                    {
                        sparkPost.changeEmail(token.token, newEmail).catch(business.getEmailErrorsManager(context).logErrors);
                        return usersDAO.update(context.user.id, {$set: {newEmail: newEmail}});
                    });
                });
            });
        }

        function resendEmail(id)
        {
            var email, password;
            return usersDAO.get({_id: id}).then(function (userFromDAO)
            {
                if (!userFromDAO) {
                    return Promise.reject(applicationException.new(applicationException.NOT_FOUND, 'User doesn\'t exist'));
                }
                email = userFromDAO.email;
                return activateAccountTokenDAO.get({userId: id});
            }).then(function (token)
            {
                var promise;
                if (dateHelper.get() > token.expire) {
                    //extend token when expired
                    promise = activateAccountTokenDAO.create(id);
                } else {
                    promise = Promise.resolve(token.token);
                }
                return promise;
            }).then(function (token)
            {
                return sparkPost.sendActivationEmail(token, email);
            });
        }

        function verify(id)
        {
            return security.requireAdmin(context).then(function ()
            {
                verifyVacanciesOfUser(id);
                return usersDAO.update(id, {verified: true}).then(function () {
                    // usersDAO.markAllAsNotNew(id); 
                    solrHelper.pushApplication(id, true);
                });
            });
        }

        function ban(id)
        {
            return security.requireAdmin(context).then(function ()
            {

                return usersDAO.update(id, {banned: true}).then(function () {
                    solrHelper.pushApplication(id, true);
                });
            }).then(function ()
            {
                return tokensDAO.removeAllByUserId(id);
            });
        }

        function unban(id)
        {
            return security.requireAdmin(context).then(function ()
            {

                return usersDAO.update(id, {banned: false}).then(function () {
                    solrHelper.pushApplication(id, true);
                });
            });
        }

        function verifyVacanciesOfUser(userId)
        {
            return business.getVacanciesManager(context).getSpecific('notverified', {userId: userId}).then(function (data)
            {
                _.map(data.results, function (elem)
                {
                    business.getVacanciesManager(context).performAction(elem.id, 'verify');
                });
            });
        }

        function changePermission(userId, permission, value)
        {
            return security.requireAdmin(context).then(function ()
            {
                if (-1 === ['vacancyAdding', 'cvSearching'].indexOf(permission)) {
                    return Promise.reject(applicationException.new(applicationException.CONFLICT));
                }

                var body = {};
                body[permission] = value;
                return UsersPermissionsDAO.updateByUserId(userId, body);
            }).then(function ()
            {
                // if user has vacancies witch are not verified (vacancies added with creating account)
                if (value) {
                    return usersDAO.get({_id: userId}).then(function (user)
                    {
                        if (user.verified) {
                            return verifyVacanciesOfUser(userId);
                        }
                    });
                }
            });
        }

        function isNew(userIds)
        {
            if (security.isAuthenticated(context)) {
                return usersDAO.isNew(context.user.email, userIds);
            } else {
                var result = {};
                _.forEach(userIds, function (value)
                {
                    result[value] = true;
                });
                return Promise.resolve(result);
            }
        }

        function isVisitedList(userIds, shouldMarkAsSeen)
        {
            return security.requireUser(context).then(function ()
            {
                return usersDAO.isVisitedList(context.user.email, userIds).then(function (result)
                {

                    if (shouldMarkAsSeen) {
                        var notSeen = [];
                        _.map(result, function (elem, prop)
                        {
                            notSeen.push(prop);
                        });


                        usersDAO.markManySeen(context.user.email, notSeen);
                    }

                    return result;
                });
            }).catch(function ()
            {
                var result = {};
                _.forEach(userIds, function (value)
                {
                    result[value] = false;
                });
                return Promise.resolve(result);
            });
        }

        function isVisited(params)
        {
            return security.requireUser(context).then(function ()
            {
                return usersDAO.isVisited(params).then(function (result)
                {
//                    if(shouldMarkAsSeen) {
//                        var notSeen = [];
//                        _.map(result, function(elem, prop)
//                        {
//                            if(elem) {
//                                notSeen.push(prop);
//                            }
//                        });
//                        usersDAO.markManySeen(context.user.email, notSeen);
//                    }

                    return result;
                });
            }).catch(function ()
            {
                var result = {};
                result[params.visitedUserId] = false;

                return Promise.resolve(result);
            });
        }

        function registerEmployer(user)
        {
            user.role = 'employer';
            user.applicationsNotificationsInterval = 'never';
            return business.getBlacklistedDomainsManager(context).rejectIfBlacklisted(user.email).then(function ()
            {
                return registerAnyUser(user);
            }).then(function (registeredUser)
            {
                user = registeredUser;

                // permissions are automatically setting to false by default (on schema level)
                return UsersPermissionsDAO.createNewOrUpdate({
                    userId: user.id
                });
            }).then(function ()
            {
                return user;
            });
        }

        function registerAdmin(user)
        {
            var pass = user.password;
            return security.requireAdmin(context).then(function ()
            {
                user.role = 'admin';
                return registerAnyUser(user);
            }).then(function (newUser)
            {
                return createTokenAndSendActivationEmail(newUser, pass);
            });
        }

        function registerAnyUser(user, social)
        {
            if (user.hasOwnProperty('password')) {
                var password = user.password;
                delete user.password;
            }
            var newUserData;

            return usersDAO.get({email: user.email}).then(function (existingUser)
            {
                if (null != existingUser) {
                    throw applicationException.new(applicationException.PRECONDITION_FAILED, 'Email already registered');
                }
                return usersDAO.createNewOrUpdate(user, social);
            }).then(function (user)
            {
                newUserData = user;
                if (!social) {
                    return createPassword(user, password);
                }
            }).then(function ()
            {
                return newUserData;
            });
        }

        function getUserByToken(token)
        {
            return tokensDAO.getByToken(token).then(function (tokenObj)
            {
                return usersDAO.get({_id: tokenObj.userId}).then(function (result)
                {
                    if (null == result) {
                        throw applicationException.new(applicationException.UNAUTHORIZED, 'Your session token has expired');
                    }

                    if ('employer' === result.role) {
                        return UsersPermissionsDAO.get(result.id).then(function (permissions)
                        {
                            result.permissions = permissions;
                            return result;
                        });
                    }

                    return result;
                });
            });
        }

        function findByEmailAndActivate(email)
        {
            return usersDAO.get({email: email}).then(function (user)
            {
                if (user.verified) {
                    return Promise.reject(applicationException.new(applicationException.CONFLICT));
                }

                user.verified = true;
                return usersDAO.update(user.id, user);
            });
        }

        function search(filter)
        {
            if (!security.isAdmin(context) && !security.isEmployer(context)) {
                return Promise.reject(applicationException.new(applicationException.FORBIDDEN));
            }
            var query = {};
            query.from = filter.from;
            query.size = filter.size;
            daoFilterHelper.copy(query, filter, 'searchQuery');
            daoFilterHelper.copy(query, filter, 'email');
            daoFilterHelper.copy(query, filter, 'title');
            daoFilterHelper.copy(query, filter, 'nameOrCvTitle');
            daoFilterHelper.copy(query, filter, 'workgte');
            daoFilterHelper.copy(query, filter, 'worklte');
            daoFilterHelper.copy(query, filter, 'days');
            daoFilterHelper.copy(query, filter, 'categories');
            daoFilterHelper.copy(query, filter, 'city');
            daoFilterHelper.copy(query, filter, 'role');
            daoFilterHelper.copy(query, filter, 'showOnHomePage');
            daoFilterHelper.copy(query, filter, 'appNotifications');
            daoFilterHelper.copy(query, filter, 'salaryRange', 'resume.desiredSalaryRange');
            daoFilterHelper.copyBoolean(query, filter, 'dbAccess');
            daoFilterHelper.copyBoolean(query, filter, 'addVacancies');
            daoFilterHelper.copyBoolean(query, filter, 'isSubscriber');
            daoFilterHelper.copyBoolean(query, filter, 'cvActive');
            daoFilterHelper.copyBoolean(query, filter, 'cvUploaded');


            if ('true' === filter.foreignDegree) {
                query['resume.foreignDegree'] = true;
            }
            if ('true' === filter.expat) {
                query['resume.expat'] = true;
            }
            if (filter.language) {
                query['resume.languages.name'] = filter.language;
            }

            if (filter.proficiency) {
                query['resume.languages.level'] = filter.proficiency;
            }

            var vacancyDAO;
            if (!security.isAdmin(context)) {
                query.hidden = false;
                query.role = 'normal';
                query.verified = true;
                query.banned = false;
                query['resume.title'] = {$ne: null};
            } else {
                daoFilterHelper.copyBoolean(query, filter, 'verified');
                daoFilterHelper.copyBoolean(query, filter, 'banned');
                daoFilterHelper.copyBoolean(query, filter, 'hidden');
                daoFilterHelper.copy(query, filter, 'type');
                vacancyDAO = require('../DAO/vacanciesDAO');
            }

            var promise = usersDAO.query(query, vacancyDAO);

            return promise;
        }

        function find(query)
        {
            var filter = {};

            daoFilterHelper.copy(filter, query, 'resumeTitle');
            daoFilterHelper.copy(filter, query, 'category', 'categories');
            daoFilterHelper.copy(filter, query, 'city');
            daoFilterHelper.copy(filter, query, 'workExperienceGTE');
            daoFilterHelper.copy(filter, query, 'workExperienceLTE');
            daoFilterHelper.copy(filter, query, 'language');
            daoFilterHelper.copy(filter, query, 'languageProficiency');
            daoFilterHelper.copy(filter, query, 'education');
            daoFilterHelper.copy(filter, query, 'days');
            daoFilterHelper.copyBoolean(filter, query, 'foreignDegree');
            daoFilterHelper.copyBoolean(filter, query, 'expat');
            daoFilterHelper.copy(filter, query, 'from');
            daoFilterHelper.copy(filter, query, 'size');

            var langs = {};
            return Promise.resolve().then(function ()
            {
                // TODO: language
                // if(query.l0 || query.l1 || query.l2) {
                //     return business.getLanguagesManager().query().then(function (languages)
                //     {
                //         languages.results.map(function (lang)
                //         {
                //             langs[lang.id] = lang;
                //         });
                //     }).then(function ()
                //     {
                //         filter.languages = ['l0', 'l1', 'l2'].reduce(function (resultFilter, curr)
                //         {
                //             (Array.isArray(query[curr]) ? query[curr] : [query[curr]]).map(function (langId)
                //             {
                //                 if(langs[langId]) {
                //                     resultFilter[langs[langId].language] = curr[1];
                //                 }
                //             });
                //             return resultFilter;
                //         }, {});
                //     });
                // }
            }).then(function ()
            {
                return usersDAO.find(filter);
            });
        }

        function createPassword(user, password)
        {
            return passwordsDAO.createOrUpdate(user.id, sha1(password));
        }

        function createTokenAndSendActivationEmail(user, pass)
        {
            return activateAccountTokenDAO.create(user.id).then(function (token)
            {
                return sparkPost.sendActivationEmail(token, user.email, pass);
            });
        }

        function searchVisited(user)
        {
            return usersDAO.searchVisited(user);
        }

        function register(user)
        {
            if (user.password !== user.confirmPassword) {
                throw applicationException.new(applicationException.PRECONDITION_FAILED, 'Password and confirm password not match');
            }

            var pass = user.password;

            if (user.employer) {
                return registerEmployer(user).then(function (userFromDAO)
                {
                    return createTokenAndSendActivationEmail(userFromDAO, pass).catch(business.getEmailErrorsManager(context).logErrors);
                });
            } else {
                return registerAnyUser(user).then(function (userFromDAO)
                {
                    return createTokenAndSendActivationEmail(userFromDAO, pass).catch(business.getEmailErrorsManager(context).logErrors);
                });
            }
        }

        function checkEmailAvailability(email)
        {
            return usersDAO.get({email: email}).then(function (user)
            {
                if (!user) {
                    return business.getBlacklistedDomainsManager(context).rejectIfBlacklisted(email);
                }

                if ('normal' === user.role) {
                    return Promise.reject(applicationException.new(applicationException.PRECONDITION_FAILED));
                }

                return Promise.reject(applicationException.new(applicationException.CONFLICT));
            });
        }

        function activate(token)
        {
            var userId, vacancyActivation = false, publishOnWebsite = void 0;
            return activateAccountTokenDAO.remove(token).then(function (results)
            {
                if (dateHelper.get() + activateAccountTokenDAO.offset < results.expire) {
                    throw applicationException.new(applicationException.UNAUTHORIZED, 'Token has been expired');
                }
                userId = results.userId;
                return usersDAO.activateAccount(userId);
            }).catch(function (error)
            {
                function getVacancyInfoAndActivateAccount(data)
                {
                    userId = data.userId;
                    vacancyActivation = true;
                    publishOnWebsite = data.publishOnWebsite;
                    return usersDAO.activateAccount(userId);
                }

                if (404 === error.error.code) {
                    return business.getVacanciesManager(context).getUserIdByVacancyToken(token).then(function (id)
                    {
                        userId = id;
                        return business.getSettingsManager(context).getPublicSettings();
                    }).then(function (settings)
                    {
                        if (settings.paymentModeEnabled) {
                            return UsersPermissionsDAO.get(userId).then(function (permissions)
                            {
                                if (permissions.vacancyAdding) {
                                    return business.getVacanciesManager(context).performAction(token, 'verifyByToken')
                                            .then(getVacancyInfoAndActivateAccount);
                                } else {
                                    return usersDAO.activateAccount(userId);
                                }
                            });
                        } else {
                            return business.getVacanciesManager(context).performAction(token, 'verifyByToken')
                                    .then(getVacancyInfoAndActivateAccount);
                        }
                    });
                }

                return Promise.reject(error);
            }).then(function ()
            {
                return usersDAO.get({_id: userId}).then(function (user)
                {
                    if (user.banned) {
                        throw applicationException.new(applicationException.METHOD_NOT_ALLOWED, 'You have been banned. Contact support to get more info.');
                    }
                    return tokensDAO.create(userId);
                }).then(function (token)
                {
                    return {
                        vacancyActivation: vacancyActivation,
                        publishOnWebsite: publishOnWebsite,
                        token: token.token
                    };
                });
            });
        }

        function confirmEmail(tokenValue)
        {
            var token;
            return EmailTokensDAO.get(tokenValue).then(function (result)
            {
                token = result;
                return usersDAO.get({_id: token.originId});
            }).then(function ()
            {
                return usersDAO.update({_id: token.originId}, {email: token.additionalData, newEmail: null});
            });
        }

        function passwordReset(email)
        {
            return usersDAO.get({email: email, verified: true}).then(function (user)
            {
                if (!user) {
                    throw applicationException.new(applicationException.BAD_REQUEST, 'User not exist');
                }
                return resetPasswordTokenDAO.create(user.id);
            }).then(function (token)
            {
                return sparkPost.passwordReset(email, token);
            });
        }

        function checkToken(token)
        {
            return resetPasswordTokenDAO.checkToken(token);
        }

        function setPassword(token, password)
        {
            return resetPasswordTokenDAO.remove(token).then(function (results)
            {
                if (dateHelper.get() + resetPasswordTokenDAO.offset < results.expire) {
                    throw applicationException.new(applicationException.UNAUTHORIZED, 'Token has been expired');
                }
                return passwordsDAO.createOrUpdate(results.userId, sha1(password));
            });
        }

        function getResume(id)
        {
            var projection = {
                firstName : 1,
                lastName : 1,
                email: 1,
                thumbnail : 1,
                socials : 1,
                firstLogin : 1,
                createDate : 1,
                extendDate : 1,
                categories : 1,
                resume : 1,
                phone : 1,
                city : 1,
                workExperience : 1
            };

            return usersDAO.get({_id: id}, projection, true).then(function (results)
            {
                if (!results) {
                    throw applicationException.new(applicationException.NOT_FOUND, 'User not found!');
                }
                var user = results;
                if (user.hasOwnProperty('socials')) {
                    user.socialOnly = user.socials.socialOnly;
                }
                results = results.resume;
                delete user.resume;
                results.userData = user;

                var promise = Promise.resolve();

                if (!results.userData.categories.length) {
                    promise = SubscriptionsDAO.get({email: results.userData.email}).then(function (subscription)
                    {
                        // because subscription.categories can contain null/undefined as first element
                        if (subscription.categories[0]) {
                            results.userData.categories = subscription.categories;
                        }
                    }).catch(function ()
                    {
                        return Promise.resolve();
                    });
                }

                if (context.user && 'employer' === context.user.role) {
                    promise = promise.then(function () {
                        return usersDAO.markSeen(context.user.email, id);
                    }).catch(function ()
                    {
                        return Promise.resolve();
                    });
                }

                return promise.then(function () {
                    return results;
                });
            }).then(function (resume)
            {
                // this condition below is enough to ensure that system will count views correctly - JS isn't employer and admin too,
                // so each view by employer is correct
                if (security.isEmployer(context)) {
                    usersDAO.markResumeViewedBy(resume.userData.id, context.user.id);
                }
                return resume;
            });
        }

        function updateResume(resume)
        {
            function calculateWorkExperience(work)
            {
                var totalWorkExperience = 0;
                for (var i = 0; i < work.length; i++) {
                    work[i].startDate = new Date(work[i].startDate).valueOf();
                    if(work[i].endDate)
                        work[i].endDate = new Date(work[i].endDate).valueOf();

                    if (work[i].startDate && work[i].endDate) {
                        totalWorkExperience += (work[i].endDate - work[i].startDate) + dateHelper.moment.duration(10,'d').asMilliseconds() + dateHelper.moment.duration(1,'M').asMilliseconds();
                    } else if (work[i].startDate && !work[i].endDate) {
                        totalWorkExperience += Date.now() - work[i].startDate;
                    }
                }
                return totalWorkExperience;
            }

            function update()
            {
                var userId = resume.userId;
                var workExperience = calculateWorkExperience(resume.workExperience);
                delete resume.userId;
                resume.updateDate = dateHelper.get();

                resume.userData.categories = _.map(resume.userData.categories, function (elem)
                {
                    return elem.id;
                });

                if (resume.userData.city && resume.userData.city.hasOwnProperty('id')) {
                    resume.userData.city = resume.userData.city.id;
                }

                if (resume.desiredSalaryRange && resume.desiredSalaryRange.hasOwnProperty('id')) {
                    resume.desiredSalaryRange = resume.desiredSalaryRange.id;
                }



                console.log(resume.workExperience);
                return usersDAO.update({_id: userId}, {$set: {
                        resume: resume,
                        workExperience: workExperience,
                        categories: resume.userData.categories,
                        city: resume.userData.city,
                        phone: resume.userData.phone,
                        firstName: resume.userData.firstName,
                        lastName: resume.userData.lastName
                    }}).then(function (res)
                {
                    solrHelper.pushApplication(userId, true);
                });
            }

            return security.requireAdmin(context).catch(function ()
            {
                return security.requireUser(context).then(function ()
                {
                    if (resume.userId !== context.user.id.toString()) {
                        return Promise.reject(applicationException.new(applicationException.FORBIDDEN));
                    }
                });
            }).then(function ()
            {
                return checkIfTriedToChangeEmail(resume.userData);
            }).then(update);
        }

        function getResumeAsPDF(userId)
        {

            // var sourceUrl = config.frontendBaseUrl + '/user/' + userId + '/profile?pdf';
            // return new Promise(function (resolve, reject)
            // {
            //     require('http').get('http://api.html2pdfrocket.com/pdf?apikey=' + config.pdfApiKey + '&value=' + escape(sourceUrl), function(response)
            //     {
            //
            //         resolve(response);
            //     }).on('error', function (error)
            //     {
            //         console.log(error);
            //         reject(error);
            //     });
            // });

            return new Promise(function (resolve, reject) {
                getResume(userId).then(function (resume) {
                    var lang = 'en';
                    var languageLevels = ['Basic', 'Intermediate', 'Advanced'];
                    var path = 'exporter_data/cv-' + userId + '.pdf';

                    resume.userData.categories = _.map(resume.userData.categories, function (item) {
                        return item.name[lang];
                    }).join(', ');

                    var workExperienceDuration = resume.userData.workExperience;
                    var workYears = dateHelper.moment.duration(workExperienceDuration)._data.years;
                    var workMonths = dateHelper.moment.duration(workExperienceDuration)._data.months;

                    resume.userData.workExperience = workYears + ' years ' + workMonths + ' months';

                    resume.workExperience = _.map(resume.workExperience, function (work) {
                        work.startDate = dateHelper.moment(work.startDate).format('MM.YYYY');
                        work.endDate = work.currentWork ? 'Present' : dateHelper.moment(work.endDate).format('MM.YYYY');
                        return work;
                    });

                    resume.education = _.map(resume.education, function (education) {
                        education.startDate = dateHelper.moment(education.startDate).format('YYYY');
                        education.endDate = dateHelper.moment(education.endDate).format('YYYY');
                        return education;
                    });

                    resume.languages = _.map(resume.languages, function (lang) {
                        lang.level = languageLevels[lang.level];
                        return lang;
                    });

                    resume.updateDate = dateHelper.moment(resume.updateDate).format('DD/MM/YYYY');

                    var compile = jade.compileFile(__dirname + '/../pdfTemplates/cv.tpl.jade', {pretty: true});
                    var htmlOut = compile({
                        resume: resume,
                        logo: config.backendEmailStatic + '/logo.png',
                        lang: lang
                    });

                    var options = {
                        width: '270mm',
                        height: '382mm',
                        format: 'A4',
                        border: '0.5cm',
                        timeout: 120000,
                        zoomFactor: 1
                    };

                    pdf.create(htmlOut, options).toFile('./' + path, function (err, res) {
                        if (err)
                            reject(err);
                        resolve(path);
                    });
                });
            });
        }

        function callForInterview(resumeId, payload)
        {
            return security.requireUser(context).then(function ()
            {
                return usersDAO.update(resumeId, {$addToSet: {offers: {vacancyId: payload.vacancyId}}});
            }).then(function (result)
            {
                return usersDAO.get({_id: resumeId});
            }).then(function (user)
            {
                sparkPost.callForInterview(user.email, payload.vacancyId).catch(business.getEmailErrorsManager(context).logErrors);
            });
        }

        function removeOldProfile()
        {
            var usersIds, usersEmails = [];
            var userExpirationTime;
            return business.getSettingsManager(context).get('userExpirationTime').then(function (settings) {
                userExpirationTime = settings.userExpirationTime;

                return security.requireAdmin(context);
            }).then(function ()
            {
                return usersDAO.search({extendDate: {$lte: dateHelper.get() - userExpirationTime}, role: {$ne: security.ADMIN}}, {_id: 1});
            }).then(function (users)
            {
                usersIds = users.map(function (element)
                {
                    usersEmails.push(element.email);
                    return element.id;
                });

                return usersDAO.update({_id: {$in: usersIds}}, {$set: {hidden: true, remindSent: false}}, {multi: true});
            }).then(function ()
            {
                return VacancyDAO.update({userId: {$in: usersIds}}, {$set: {archived: true}}, {multi: true});
            }).then(function ()
            {
                var promises = usersIds.map(function (id)
                {
                    return passwordsDAO.remove(id);
                });
                return Promise.all(promises);

            }).then(function ()
            {
                var promises = usersIds.map(function (id)
                {
                    return VacancyDAO.removeAllApplications(id);
                });
                return Promise.all(promises);
            }).then(function ()
            {
                var promises = usersIds.map(function (id)
                {
                    return VacancyDAO.removeByUserId(id);
                });
                return Promise.all(promises);
            }).then(function ()
            {
                var promises = usersIds.map(function (id)
                {
                    return usersDAO.removeAllViewsOfUser(id);
                });
                return Promise.all(promises);
            }).then(function ()
            {
                var promises = usersIds.map(function (id)
                {
                    return usersDAO.removeFromSeenByUserId(id);
                });
                return Promise.all(promises);
            }).then(function ()
            {
                return SubscriptionsDAO.remove({'$or': [{_id: {$in: usersIds}}, {email: {$in: usersEmails}}]});
            })
        }

        function createTokenAndSendRemind(user, index, expirationTime)
        {
            var oneDay = (24 * 3600000);
            var timeToExpire = Math.round(((user.extendDate + expirationTime) - dateHelper.get()) / oneDay);
            return usersDAO.createExtendToken(user.id).then(function (token)
            {
                return sparkPost.sendReminderToOldProfile(user, token.id, timeToExpire);
            }).then(function ()
            {
                return usersDAO.createNewOrUpdate({
                    id: user.id,
                    remindSent: true
                });
            });
        }

        function sendReminderProfile()
        {
            var oneDay = (24 * 3600000);
            var userExpirationTime;
            return security.requireAdmin(context).then(function ()
            {
                return business.getSettingsManager(context).get('userExpirationTime');
            }).then(function (settings)
            {
                userExpirationTime = settings.userExpirationTime;
                return usersDAO.search({
                    extendDate: {
                        $gte: (dateHelper.get() - userExpirationTime) + oneDay + 1,
                        $lte: (dateHelper.get() - userExpirationTime) + (10 * oneDay)
                    },
                    verified: true,
                    remindSent: false
                }, {
                    _id: 1,
                    email: 1,
                    firstName: 1,
                    lastName: 1,
                    extendDate: 1
                });

            }).then(function (users)
            {
                var promises = _.map(users, function (user, index)
                {
                    createTokenAndSendRemind(user, index, userExpirationTime);
                });
                return Promise.all(promises);
            });
        }

        function getNotifications() {
            var oneDay = (24 * 3600000);
            var userExpirationTime;

            return new Promise(function (resolve, reject) {
                business.getSettingsManager(context).get('userExpirationTime').then(function (settings) {

                    userExpirationTime = settings.userExpirationTime;
                    var notifications = {};
                    notifications.daysLeft = false;
                    notifications.unverified = !context.user.verified;
                    notifications.hidden_cv = context.user.hiddenOnSearch;
                    notifications.unconfirmed_email = !!context.user.newEmail;


                    var expDate = context.user.extendDate + userExpirationTime;
                    var today = dateHelper.get();
                    var daysLeft = dateHelper.moment(expDate).diff(dateHelper.moment(today), 'days');

                    //Get days to expire if at least 7 days left
                    if (daysLeft <= 7 && daysLeft > 0) {
                        notifications.daysLeft = daysLeft;
                    }

                    resolve(notifications);
                }).catch(function (error) {
                    reject(error);
                });
            });
        }

        function isSeekerProfileActive(user) {
            return user.resume.title && user.resume.title.length && user.categories.length && user.city &&
                    user.firstName && user.firstName.length &&
                    user.lastName && user.lastName.length && user.resume.education.length;
        }

        function isSeekerProfileInModeration(user) {
            return !!user.uploadedResume && user.uploadedResume.messageSent == false;
        }

        function isEmployerProfileActive(user) {
            return user.phone &&
                    user.firstName && user.firstName.length &&
                    user.lastName && user.lastName.length;
        }

        function extendProfile(token)
        {
            return usersDAO.getUserFromExtendToken(token).then(function (user)
            {
                user.hidden = false;
                user.extendDate = dateHelper.get();
                user.remindSent = false;
                return usersDAO.createNewOrUpdate(user);
            });
        }

        function getOffers(filter)
        {
            return security.requireUser(context).then(function ()
            {
                var projectionUser = {
                    offers: 1
                };

                filter.id = context.user.id;
                return usersDAO.usersOffers(filter, projectionUser).then(function (results) {
                    results.results = results.results.map(function (elem)
                    {
                        elem.isNotVisited = !elem.seen;
                        return elem;
                    });

                    return results;
                });
            });
        }

        function removeOffers(offerId)
        {
            return security.requireUser(context).then(function ()
            {
                return usersDAO.removeOffers(context.user.id, offerId);
            });
        }

        function removeUsersOffers(vacancyId)
        {
            return usersDAO.removeUserOffers(vacancyId);
        }

        function carousel()
        {
            return security.requireEmployer(context).then(function () {
                return usersDAO.queryForJobSeekerCarousel().then(function (users)
                {
                    return {
                        results: users
                    };
                });
            }).catch(function () {
                return usersDAO.queryForEmployerCarousel().then(function (users)
                {
                    return {
                        results: users
                    };
                });
            });
        }

        function markOfferSeen(vacancyId)
        {
            return security.requireUser(context).then(function ()
            {
                return context.user.offers.map(function (offer)
                {
                    if (offer.vacancyId.toString() === vacancyId) {
                        offer.seen = true;
                    }
                    return offer;
                });
            }).then(function (offers)
            {
                usersDAO.update({_id: context.user.id}, {offers: offers});
            });
        }

        function markAllOffersSeen()
        {
            return security.requireUser(context).then(function ()
            {
                return context.user.offers.map(function (offer)
                {
                    offer.seen = true;
                    return offer;
                });
            }).then(function (offers)
            {
                usersDAO.update({_id: context.user.id}, {offers: offers});
            });
        }

        function queryResumeViews(id, filter)
        {
            if (!security.isAuthenticated(context)) {
                return Promise.reject(applicationException.new(applicationException.UNAUTHORIZED));
            }
            if (!security.isJobSeeker(context)) {
                return Promise.reject(applicationException.new(applicationException.FORBIDDEN));
            }

            var query = {};
            query.from = filter.from;
            query.size = filter.size;

            var queriedApplicants = [];

            return usersDAO.queryResumeViews(id, query, context.user.id).then(function (result)
            {
                return result;
            });
        }

        function getNewResumeViewsCount()
        {
            return security.requireUser(context).then(function ()
            {
                return usersDAO.countNewResumeViewsByUserId(context.user.id);
            }).then(function (count)
            {
                return {count: count};
            });
        }

        function getNewOffersViewsCount()
        {
            return security.requireUser(context).then(function ()
            {
                return usersDAO.countNewOffersViewsByUserId(context.user.id);
            }).then(function (count)
            {
                return {count: count};
            });
        }

        function markResumeViewsSeen(userId, views)
        {
            var viewers = _.map(views, function (elem) {
                return elem.user._id.toString();
            });

            if (!security.isAuthenticated(context) || !security.isJobSeeker(context)) {
                return Promise.reject(applicationException.new(applicationException.FORBIDDEN));
            }

            return usersDAO.get({_id: userId}).then(function (user)
            {
                if (context.user.id.toString() !== user.id.toString()) {
                    return Promise.reject(applicationException.new(applicationException.FORBIDDEN));
                }

                var userToUpdate = {
                    resumeViews: _.map(user.resumeViews, function (elem)
                    {
                        if (-1 < viewers.indexOf(elem.user.toString())) {
                            elem.seen = true;
                        }
                        return elem;
                    })
                };

                return usersDAO.update(userId, userToUpdate);
            });
        }

        function showCvOnHome(userId, showCvOnHomeValue)
        {
            return usersDAO.update({_id: userId}, {showOnHomePage: showCvOnHomeValue});
        }

        function getAll(filter)
        {
            return usersDAO.getAll(filter);
        }

        function forwardCv(email)
        {
            return security.requireUser(context).then(function ()
            {
                if (!context.user.resume.title) {
                    throw applicationException.new(applicationException.CONFLICT);
                }
                return getResumeAsPDF(context.user.id);
            }).then(function (filepath)
            {
                return new Promise(function (resolve, reject)
                {
                    var buffer = fs.readFileSync(filepath);

                    pdf = new Buffer(buffer).toString('base64');
                    sparkPost.forwardCv(email, context.user, pdf).then(function ()
                    {
                        resolve();
                    }).catch(function (error)
                    {
                        business.getEmailErrorsManager(context).logErrors(error);
                        reject();
                    });
                });
            });
        }

        function cvActivationNotification(user) {
            return security.requireAdmin(context).then(function () {
                return sparkPost.CvActivationNotification(user.email).then(function () {
                    return usersDAO.update({_id: user.id}, {uploadedResume: {messageSent: true}});
                });
            });
        }

        return {
            sendReminderProfile: sendReminderProfile,
            getNotifications: getNotifications,
            removeOldProfile: removeOldProfile,
            setPassword: setPassword,
            checkToken: checkToken,
            activate: activate,
            confirmEmail: confirmEmail,
            authenticate: authenticate,
            authenticateByFacebook: authenticateByFacebook,
            authenticateByLinkedIn: authenticateByLinkedIn,
            impersonate: impersonate,
            ban: ban,
            unban: unban,
            changePermission: changePermission,
            isNew: isNew,
            searchVisited: searchVisited,
            passwordReset: passwordReset,
            getUserByToken: getUserByToken,
            registerEmployer: registerEmployer,
            registerAdmin: registerAdmin,
            getProfile: getProfile,
            updateProfile: updateProfile,
            remove: remove,
            uploadAvatar: uploadAvatar,
            uploadCv: uploadCv,
            changeApplicationsNotifications: changeApplicationsNotifications,
            sendApplyNotifications: sendApplyNotifications,
            resendRegistrationEmail: resendRegistrationEmail,
            register: register,
            checkEmailAvailability: checkEmailAvailability,
            search: search,
            find: find,
            verify: verify,
            findByEmailAndActivate: findByEmailAndActivate,
            getResume: getResume,
            updateResume: updateResume,
            getResumeAsPDF: getResumeAsPDF,
            callForInterview: callForInterview,
            extendProfile: extendProfile,
            getOffers: getOffers,
            removeOffers: removeOffers,
            removeUsersOffers: removeUsersOffers,
            carousel: carousel,
            markOfferSeen: markOfferSeen,
            markAllOffersSeen: markAllOffersSeen,
            queryResumeViews: queryResumeViews,
            markResumeViewsSeen: markResumeViewsSeen,
            getNewResumeViewsCount: getNewResumeViewsCount,
            getNewOffersViewsCount: getNewOffersViewsCount,
            showCvOnHome: showCvOnHome,
            getAll: getAll,
            forwardCv: forwardCv,
            unremove: unremove,
            isSeekerProfileActive: isSeekerProfileActive,
            isSeekerProfileInModeration: isSeekerProfileInModeration,
            isEmployerProfileActive: isEmployerProfileActive,
            cancelCvCheckup: cancelCvCheckup,
            isVisited: isVisited,
            isVisitedList: isVisitedList,
            cvActivationNotification: cvActivationNotification
        };
    }

    module.exports = {
        create: create
    };
})();
