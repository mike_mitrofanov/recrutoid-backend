(function ()
{
    'use strict';

    var cvfilesDAO = require('../DAO/cvfilesDAO');
    var daoFilterHelper = require('../util/daoFilterHelper');
    var applicationException = require('../service/applicationException');
    var Promise = require('bluebird');
    var security = require('./security');
    var _ = require('lodash');

    function create(context)
    {
        function search(filter)
        {
            var query = {};
            daoFilterHelper.copy(query, filter, 'email');

            query.from = filter.from;
            query.size = filter.size;
            query.email = filter.email;

            return cvfilesDAO.query(query);
        }

        function update(translation)
        {
            return cvfilesDAO.update(translation);
        }

        function remove(id)
        {
            var removedUser;
            return security.requireAdmin(context).catch(function ()
            {
                return security.requireUser(context).then(function ()
                {
                    if (id !== context.user.id.toString()) {
                        return Promise.reject(applicationException.new(applicationException.FORBIDDEN));
                    }
                });
            }).then(function ()
            {
                return cvfilesDAO.remove(id, context);
            });
        }

        return {
            update: update,
            search: search,
            remove: remove
        };
    }

    module.exports = {
        create: create
    };
})();
