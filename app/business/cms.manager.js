(function ()
{
    'use strict';
    var security = require('./security');
    var CMSDAO = require('../DAO/cmsDAO');
    var applicationException = require('../service/applicationException');
    var dateHelper = require('../service/dateHelper');
    var cloudinary = require('../service/cloudinary');

    function create(context)
    {
        function createNewOrUpdate(page)
        {
            return security.requireAdmin(context).then(function ()
            {
                if(!page.id && page.newsBox) {
                    return CMSDAO.query({newsBox: true}).then(function (news)
                    {
                        if(3 < news.length) {
                            throw applicationException.new(applicationException.VALIDATION_FAILURE, 'Can not add more than 4 news!');
                        }
                        page.slug = dateHelper.get();
                    });
                }
            }).then(function ()
            {
                return CMSDAO.createNewOrUpdate(page);
            });
        }

        function query(filter)
        {
            return security.requireAdmin(context).then(function ()
            {
                return CMSDAO.query(filter);
            });
        }

        function getPagesList()
        {
            return CMSDAO.getList();
        }

        function getNews()
        {
            return CMSDAO.getNews();
        }

        function remove(pageId)
        {
            return security.requireAdmin(context).then(function ()
            {
                return CMSDAO.remove(pageId);
            });
        }

        function get(slug)
        {
            return CMSDAO.get(slug);
        }

        function uploadImage(request)
        {
            var pageId;
            return security.requireAdmin(context).then(function ()
            {
                return cloudinary.upload(null, request, function (fields)
                {
                    fields.page = JSON.parse(fields.page);
                    pageId = fields.page.id;
                    /*jshint -W106*/
                    return {
                        overwrite: true,
                        public_id: fields.page.slug,
                        resource_type: 'image'
                    };
                });
            }).then(function (data)
            {
                return CMSDAO.updateImage(pageId, data.url);
            });
        }

        return {
            query: query,
            getPagesList: getPagesList,
            getNews: getNews,
            get: get,
            remove: remove,
            createNewOrUpdate: createNewOrUpdate,
            uploadImage: uploadImage
        };
    }

    module.exports = {
        create: create
    };
})();
