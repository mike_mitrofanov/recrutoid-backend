(function ()
{
    'use strict';

    var Promise = require('bluebird');
    var _ = require('lodash');
    var config = require('../config/config');
    var security = require('./security');
    var mongoXlsx = require('mongo-xlsx');
    var fs = require('fs');
    var vacanciesDAO = require('../DAO/vacanciesDAO');
    var daoFilterHelper = require('../util/daoFilterHelper');
    var subscriptionsDAO = require('../DAO/subscriptionsDAO');
    var usersDAO = require('../DAO/usersDAO');
    var jade = require('jade');
    
    
    function create(context, business)
    {
        function vacanciesXls(filter, callback) {
            return security.requireAdmin(context).then(function () {

                return vacanciesDAO.query(filter)
                    .then(function(data) {
                
                        var dataModel = vacanciesDataModel(data);

                        return new Promise(function(resolve, reject) {
                            generateXls(dataModel, function(err, data) {
                                if (err) {
                                    reject(err);
                                } else {
                                    resolve(data);
                                }
                            });
                        });
                    });
            });
        }

        function vacanciesPdf(filter) {
            return security.requireAdmin(context).then(function () {

                return vacanciesDAO.query(filter)
                  .then(function(data) {

                      var dataModel = vacanciesDataModel(data);
                      return generatePdf(dataModel,'vacancies');
                  });
            });
        }

        function subscriptionsXls(filter) {
            var query = {};

            daoFilterHelper.copy(query, filter, 'size');
            daoFilterHelper.copy(query, filter, 'email');
            daoFilterHelper.copy(query, filter, 'city');
            daoFilterHelper.copy(query, filter, 'categories');
            daoFilterHelper.copy(query, filter, 'interval');
            daoFilterHelper.copyBoolean(query, filter, 'verified');
            daoFilterHelper.copyBoolean(query, filter, 'banned');
            daoFilterHelper.copyBoolean(query, filter, 'subscribed');
            daoFilterHelper.copy(query, filter, 'desiredSalaryRange');
            
            return security.requireAdmin(context).then(function ()
            {
                return subscriptionsDAO.query(query)
                    .then(function(data) {

                        var dataModel = subscriptionDataModel(data);

                        return new Promise(function(resolve, reject) {
                            generateXls(dataModel, function(err, data) {
                                if (err) {
                                    reject(err);
                                } else {
                                    resolve(data);
                                }
                            });
                        });
                    });
            });
        }


        function subscriptionsPdf(filter) {
            var query = {};

            daoFilterHelper.copy(query, filter, 'size');
            daoFilterHelper.copy(query, filter, 'email');
            daoFilterHelper.copy(query, filter, 'city');
            daoFilterHelper.copy(query, filter, 'categories');
            daoFilterHelper.copy(query, filter, 'interval');
            daoFilterHelper.copyBoolean(query, filter, 'verified');
            daoFilterHelper.copyBoolean(query, filter, 'banned');
            daoFilterHelper.copyBoolean(query, filter, 'subscribed');
            daoFilterHelper.copy(query, filter, 'desiredSalaryRange');

            return security.requireAdmin(context).then(function ()
            {
                return subscriptionsDAO.query(query)
                  .then(function(data) {

                      var dataModel = subscriptionDataModel(data);
                      return generatePdf(dataModel, 'subscriptions');
                  });
            });
        }

        function getUsersData(filter){
            var query = {};

            daoFilterHelper.copy(query, filter, 'searchQuery');
            daoFilterHelper.copy(query, filter, 'email');
            daoFilterHelper.copy(query, filter, 'title');
            daoFilterHelper.copy(query, filter, 'nameOrCvTitle');
            daoFilterHelper.copy(query, filter, 'workgte');
            daoFilterHelper.copy(query, filter, 'worklte');
            daoFilterHelper.copy(query, filter, 'days');
            daoFilterHelper.copy(query, filter, 'categories');
            daoFilterHelper.copy(query, filter, 'city');
            daoFilterHelper.copy(query, filter, 'role');
            daoFilterHelper.copy(query, filter, 'appNotifications');
            daoFilterHelper.copy(query, filter, 'salaryRange', 'resume.desiredSalaryRange');
            daoFilterHelper.copyBoolean(query, filter, 'dbAccess');
            daoFilterHelper.copyBoolean(query, filter, 'addVacancies');
            daoFilterHelper.copyBoolean(query, filter, 'isSubscriber');
            daoFilterHelper.copyBoolean(query, filter, 'cvActive');

            if ('true' === filter.foreignDegree) {
                query['resume.foreignDegree'] = true;
            }
            if ('true' === filter.expat) {
                query['resume.expat'] = true;
            }
            if (filter.language) {
                query['resume.languages.name'] = filter.language;
            }

            if (filter.proficiency) {
                query['resume.languages.level'] = filter.proficiency;
            }

            var vacancyDAO;
            if (!security.isAdmin(context)) {
                query.hidden = false;
                query.role = 'normal';
                query.verified = true;
                query.banned = false;
                query['resume.title'] = {$ne: null};
            } else {
                daoFilterHelper.copyBoolean(query, filter, 'verified');
                daoFilterHelper.copyBoolean(query, filter, 'banned');
                daoFilterHelper.copyBoolean(query, filter, 'deleted');
                daoFilterHelper.copy(query, filter, 'type');
                vacancyDAO = require('../DAO/vacanciesDAO');
            }


            return security.requireAdmin(context).then(function(){
                return usersDAO.query(query, vacancyDAO);
            });
        }

        function usersXls(filter) {
            return getUsersData(filter).then(function(data) {

                var dataModel = usersDataModel(data, filter);

                return new Promise(function(resolve, reject) {
                    generateXls(dataModel, function(err, data) {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(data);
                        }
                    });
                });
            });
        }

        function usersPdf(filter) {
            return getUsersData(filter).then(function(data) {
                var dataModel = usersDataModel(data, filter);

                return generatePdf(dataModel, 'users', filter);
            });
        }


        function usersDataModel(data, filter) {
            var dataModel = [];

            for (var i = 0; i < data.results.length; i++) {
                var user = {};
                user['Email'] = data.results[i].email;
                user['Name'] = getUserName(data.results[i]);
                user['Active'] = data.results[i].hidden?'No':'Yes';
                user['Verified'] = data.results[i].verified?'Yes':'No';
                user['Banned'] = data.results[i].banned?'Yes':'No';

                switch(filter.role) {
                    case 'normal':
                        user['City'] = data.results[i].city ? data.results[i].city.name.en : 'None';
                        user['Categories'] = data.results[i].categories.length?joinCategories(data.results[i].categories):'All categories';
                        user['Salary range'] = getUserSalaryRange(data.results[i]);
                        user['CV active'] = data.results[i].resume && data.results[i].resume.active?'Yes':'No';
                        user['Subscription'] = data.results[i].isSubscriber?'Yes':'No';
                        break;
                    case 'employer':
                        user['Adding Vacancies'] = data.results[i].permissions && data.results[i].permissions.vacancyAdding?'Yes':'No';
                        user['CV Access'] = data.results[i].permissions && data.results[i].permissions.cvSearching?'Yes':'No';
                        user['Notifications'] = data.results[i].applicationsNotificationsInterval?data.results[i].applicationsNotificationsInterval:'None';
                      
                        break;
                }
                
                dataModel.push(user);
            }

            function getUserName(data){
                var name = '';
                if (data.firstName || data.lastName) {
                    var lastName = data.lastName || '';
                    var firstName = data.firstName || '';

                    name = firstName + ' ' + lastName;
                }

                if (!!data.resume && !!data.resume.title){
                    if (name !== '') {
                        name += ' / ';
                    }
                    name += data.resume.title;
                }

                return name;
            }

            function getUserSalaryRange(data){
                if(!!data.resume){
                    if (!!data.resume.desiredSalaryRange){
                        return data.resume.desiredSalaryRange.name.en;
                    }
                    return 'None';
                }
                return 'None';
            }

            return dataModel;
        }
        
        function subscriptionDataModel(data) {
            var dataModel = [];

            for (var i = 0; i < data.results.length; i++) {
                var subscription = {};
                var th = [ 'Email', 'Categories', 'City', 'Salary Range', 'Timestamp', 'Subscription', 'Verified', 'Banned' ];

                var d = new Date(data.results[i].createDate);
                var dateFormated = d.getDate() + '-' + (d.getMonth() + 1) + '-' + d.getFullYear();
                
                subscription['Email'] = data.results[i].email;
                subscription['Categories'] = data.results[i].categories.length?joinCategories(data.results[i].categories):'All categories';
                subscription['City'] = data.results[i].city ? data.results[i].city.name.en : 'All cities';
                subscription['Salary range'] = data.results[i].desiredSalaryRange?data.results[i].desiredSalaryRange.name.en:'All salary ranges';
                subscription['Timestamp'] = dateFormated;
                subscription['Interval'] = data.results[i].interval;
                subscription['Verified'] = data.results[i].verified ? 'yes' : 'no';
                subscription['Banned'] = data.results[i].banned ? 'yes' : 'no';

                dataModel.push(subscription);
            }

            return dataModel;
        }

        function vacanciesDataModel(data) {
            var dataModel = [];

            for (var i = 0; i < data.results.length; i++) {
                var vacancy = {};

                var d = new Date(data.results[i].createDate);
                var dateFormated = d.getDate() + '-' + (d.getMonth() + 1) + '-' + d.getFullYear();

                vacancy['ID'] = data.results[i].id;
                vacancy['Email'] = data.results[i].email;
                vacancy['Categories'] = joinCategories(data.results[i].categories);
                vacancy['Company name'] = data.results[i].companyName;
                vacancy['Salary range'] = data.results[i].salaryRangeId.name.en;
                vacancy['City'] = data.results[i].cityId.name.en;
                vacancy['Vacancy title'] = data.results[i].title;
                vacancy['Timestamp'] = dateFormated;
                vacancy['Matches'] = data.results[i].matchingSubscriptions;
                vacancy['Distributions'] = data.results[i].sentEmails;

                dataModel.push(vacancy);
            }

            return dataModel;
        }


        function generateXls(dataModel, callback) {
            var model = mongoXlsx.buildDynamicModel(dataModel);

            var options = {
                path: 'exporter_data'
            };

            return mongoXlsx.mongoData2Xlsx(dataModel, model, options, callback);
        }

        function generatePdf(data, type, filter){
            var pdf = require('html-pdf');
            var templatePath = '/../pdfTemplates/'+type+'.tpl.jade';

            if(filter && filter.role){
                templatePath = '/../pdfTemplates/'+type+'-'+filter.role+'.tpl.jade';
            }

            var compiler = jade.compileFile(__dirname + templatePath,
              {pretty: true, compileDebug: true});

            var html = compiler({
                data: data
            });

            var path = 'exporter_data/'+type+'.pdf';

            var options = {
                format: 'A4',
                orientation: "landscape",
                border: '1.5cm',
                "directory": "./tmp11",
                "timeout": 120000
            };

            return new Promise(function(resolve, reject) {
                pdf.create(html, {directory: __dirname}, function(err, res) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(path);
                    }
                });
            });
        }

        function joinCategories(array) {
            var tempArray = [];
            array.forEach(function (item) {
                tempArray.push(item.name.en);
            });
            
            return tempArray.join(', ');
        }

        return {
            vacanciesXls: vacanciesXls,
            vacanciesPdf: vacanciesPdf,
            subscriptionsXls: subscriptionsXls,
            subscriptionsPdf: subscriptionsPdf,
            usersXls: usersXls,
            usersPdf: usersPdf
        };
    }

    module.exports = {
        create: create
    };
})();
