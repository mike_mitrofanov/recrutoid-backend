(function ()
{
    'use strict';

    var bannersDAO = require('../DAO/bannersDAO');
    var security = require('./security');
    var cloudinary = require('../service/cloudinary.js');

    function create(context)
    {
        function createNewOrUpdate(banner)
        {
            if(!banner.id) {
                banner.url = 'empty';
            }
            return bannersDAO.createNewOrUpdate(banner);
        }

        function upload(request)
        {
            var banner;
            var sizes = [
                [500, 200], [200, 500]
            ];
            return security.requireAdmin(context).then(function ()
            {
                return cloudinary.upload(null, request, function (fields)
                {
                    banner = fields;
                    var transformation = {
                        crop: 'scale',
                        width: sizes[banner.location][0],
                        height: sizes[banner.location][1]
                    };
                    /*jshint -W106*/
                    return {
                        overwrite: true,
                        public_id: 'banner-' + banner.id,
                        resource_type: 'image',
                        transformation: transformation
                    };
                });
            }).then(function (data)
            {
                banner = {
                    id: banner.id,
                    url: data.url
                };
                return bannersDAO.createNewOrUpdate(banner);
            }).then(function (updatedBanner)
            {
                return updatedBanner;
            });
        }

        function query(filter)
        {
            return security.requireAdmin(context).then(function ()
            {
                return bannersDAO.query(filter);
            });
        }

        function get(id)
        {
            return bannersDAO.get({_id: id});
        }

        function remove(id)
        {
            return bannersDAO.remove(id);
        }

        function activate(bannerId, query)
        {
            return bannersDAO.updateAll(query, {
                active: false
            }).then(function ()
            {
                return bannersDAO.createNewOrUpdate({
                    id: bannerId,
                    active: true
                });
            });
        }

        function deactivate(bannerId)
        {
            return bannersDAO.createNewOrUpdate({
                id: bannerId,
                active: false
            });
        }

        function getPublished()
        {
            return bannersDAO.query({
                active: true
            });
        }

        return {
            createNewOrUpdate: createNewOrUpdate,
            upload: upload,
            query: query,
            get: get,
            remove: remove,
            activate: activate,
            deactivate: deactivate,
            getPublished: getPublished
        };
    }

    module.exports = {
        create: create
    };
})();
