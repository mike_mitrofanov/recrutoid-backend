(function ()
{
    'use strict';

    var translationsDAO = require('../DAO/translationsDAO');
    var vacanciesDAO = require('../DAO/vacanciesDAO');
    var subscriptionsDAO = require('../DAO/subscriptionsDAO');
    var usersDAO = require('../DAO/usersDAO');
    var daoFilterHelper = require('../util/daoFilterHelper');
    var applicationException = require('../service/applicationException');
    var Promise = require('bluebird');
    var security = require('./security');
    var _ = require('lodash');

    function create(context)
    {
        function search(filter)
        {
            var query = {};
            daoFilterHelper.copy(query, filter, 'action');

            query.from = filter.from;
            query.size = filter.size;

            return translationsDAO.query(query);
        }

        function update(translation)
        {
            return translationsDAO.update(translation);
        }

        function remove(id)
        {
            return security.requireAdmin(context).then(function ()
            {
                return translationsDAO.remove(id);
            });
        }

        return {
            update: update,
            search: search,
            remove: remove
        };
    }

    module.exports = {
        create: create
    };
})();
