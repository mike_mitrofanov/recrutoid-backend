(function ()
{
    'use strict';

    var blacklistedDomainsDAO = require('../DAO/blacklistedDomainsDAO');
    var applicationException = require('../service/applicationException');
    var Promise = require('bluebird');
    var security = require('./security');

    function create(context)
    {
        function getAllDomains(filter)
        {
            return security.requireAdmin(context).then(function ()
            {
                return blacklistedDomainsDAO.query(filter);
            });
        }

        function createNewOrUpdate(domain)
        {
            return security.requireAdmin(context).then(function ()
            {
                if (!domain.id) {
                    return blacklistedDomainsDAO.get({name: domain.name}).then(function (result)
                    {
                        if (result) {
                            return Promise.reject(applicationException.new(applicationException.CONFLICT, 'This domain already exist in blacklist'));
                        }

                        return blacklistedDomainsDAO.createNewOrUpdate(domain);
                    });
                }

                return blacklistedDomainsDAO.createNewOrUpdate(domain);
            });
        }

        function remove(id)
        {
            return security.requireAdmin(context).then(function ()
            {
                return blacklistedDomainsDAO.remove(id);
            });
        }

        function rejectIfBlacklisted(email)
        {
            if (!email || -1 === email.indexOf('@')) {
                return Promise.reject(applicationException.new(applicationException.VALIDATION_FAILURE));
            }
            var domain = email.split('@')[1];
            return blacklistedDomainsDAO.query({name: domain}).then(function (domains)
            {
                if (0 < domains.total) {
                    return Promise.reject(applicationException.new(applicationException.DOMAIN_BLACKLISTED));
                }
            });
        }

        return {
            getAllDomains: getAllDomains,
            createNewOrUpdate: createNewOrUpdate,
            remove: remove,
            rejectIfBlacklisted: rejectIfBlacklisted
        };
    }

    module.exports = {
        create: create
    };
})();
