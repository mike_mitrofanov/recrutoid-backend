(function ()
{
    'use strict';

    var categoriesDAO = require('../DAO/categoriesDAO');
    var vacanciesDAO = require('../DAO/vacanciesDAO');
    var subscriptionsDAO = require('../DAO/subscriptionsDAO');
    var usersDAO = require('../DAO/usersDAO');
    var applicationException = require('../service/applicationException');
    var Promise = require('bluebird');
    var security = require('./security');
    var _ = require('lodash');

    function create(context)
    {
        function getAllCategories()
        {
            return categoriesDAO.query();
        }

        function createNewOrUpdate(category)
        {
            return security.requireAdmin(context).then(function ()
            {
                if (!category.id) {
                    return categoriesDAO.get({name: category.name}).then(function (result)
                    {
                        if (result) {
                            return Promise.reject(applicationException.CONFLICT);
                        }

                        return categoriesDAO.createNewOrUpdate(category);
                    });
                }
                return categoriesDAO.createNewOrUpdate(category);
            });
        }

        function replaceCategories(categories, oldId, newId)
        {
            if(categories[0].hasOwnProperty('_id')) {
                categories = _.map(categories, '_id');
            }

            if(-1 === categories.indexOf(newId)) {
                return _.map(categories, function (cat)
                {
                    cat = cat.toString();
                    return cat.toString() === oldId.toString() ? newId : cat;
                });
            }
            else {
                categories.splice(categories.indexOf(oldId), 1);
                return categories;
            }
        }

        function updateCategoryEverywhere(id, toReplace)
        {
            var promises;
            return vacanciesDAO.query({categories: id}).then(function (vacancies)
            {
                promises = [];
                _.map(vacancies.results, function (elem)
                {
                    promises.push(vacanciesDAO.update({_id: elem.id}, {categories: replaceCategories(elem.categories, id, toReplace)}));
                });

                return Promise.all(promises).then(function ()
                {
                    return subscriptionsDAO.search({categories: id});
                });
            }).then(function (subscriptions)
            {
                promises = [];
                _.map(subscriptions, function (elem)
                {
                    elem.categories = replaceCategories(elem.categories, id, toReplace);
                    promises.push(subscriptionsDAO.update({_id: elem.id}, elem));
                });

                return Promise.all(promises).then(function ()
                {
                    return usersDAO.search({categories: id});
                });
            }).then(function (users)
            {
                promises = [];
                _.map(users, function (elem)
                {
                    promises.push(usersDAO.update({_id: elem.id}, {categories: replaceCategories(elem.categories, id, toReplace)}));
                });

                return Promise.all(promises);
            });
        }

        function remove(id, toReplace)
        {
            return security.requireAdmin(context).then(function ()
            {
                return updateCategoryEverywhere(id, toReplace);
            }).then(function ()
            {
                return categoriesDAO.remove(id);
            });
        }

        function getAllCategoriesWithCount()
        {
            return categoriesDAO.query().then(function (results)
            {
                return vacanciesDAO.queryCountVacancyInCategory(results).then(function (data)
                {
                    return usersDAO.search({role: 'normal', banned: false, verified: true}, {_id: 1}).then(function (arrayUsers)
                    {
                        data.countUsers = arrayUsers.length || 0;
                        return data;
                    });
                });
            });
        }

        return {
            getAllCategories: getAllCategories,
            createNewOrUpdate: createNewOrUpdate,
            getAllCategoriesWithCount: getAllCategoriesWithCount,
            remove: remove
        };
    }

    module.exports = {
        create: create
    };
})();
