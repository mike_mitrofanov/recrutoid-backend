(function ()
{
    'use strict';

    var Promise = require('bluebird');

    var solrHelper = require('../service/solrHelper');
    var citiesDAO = require('../DAO/citiesDAO');
    var phrasesDAO = require('../DAO/phrasesDAO');


    function create(context, business)
    {

        var filterLimit = 10,
                bigNumber = 1000,
                companyName = null,
                city = null
                ;


        function sortCallback(kv1, kv2) {
            var diff = kv2.doclist.numFound - kv1.doclist.numFound;

            if (diff === 0) {
                return kv2.createDate - kv1.createDate;
            } else {
                return diff;
            }
        }

        function filterCompanyDataCallback(response, fieldName) {

            var groups = [];
            
            var sorted = response.grouped.vacancyCompanyNameExact.groups.sort(sortCallback);

            for (var i in sorted) {
                if (i > filterLimit) {
                    break;
                }
                var item = response.grouped.vacancyCompanyNameExact.groups[i];
                var group = {name: item.doclist.docs[0].vacancyCompanyNameExact, count: item.doclist.numFound};
                groups.push(group);
            }

            return {data: groups, countOfAll: response.grouped.vacancyCompanyNameExact.matches};
        }

        function filterLocationDataCallback(response, fieldName) {

            var groupedInfo = response.grouped.cityId;
            var groups = groupedInfo.groups;
            
            var ids = [];
            var counts = [];

            var sorted = groups.sort(sortCallback);

            for (var i in sorted) {
                if (i > filterLimit) {
                    break;
                }

                ids.push(groups[i].groupValue);
                counts.push(groups[i].doclist.numFound);
            }

            return {countOfAll: groupedInfo.matches, ids: ids, counts: counts};
        }


        function find(query) {

            var finalResults = {};
            
            companyName = query.companyName;
                
            

            return  new Promise(function (resolve, reject) {

                phrasesDAO.getTranslationsForTitle(query.vacancyTitle).then(function (phrases) {

                    
                    var prepareLocationData = new Promise(function (resolve, reject) {

                        if (!(query.vacancyTitle || query.category || query.city || query.salaryRange)) {
                            resolve();
                            return;
                        }
                        var cq = JSON.parse(JSON.stringify(query));

                        cq['from'] = 0;
                        cq['size'] = bigNumber;
                        cq['city'] = null;

                        solrHelper.queryVacancy(cq, {
                            fieldName: ['cityId',  'createDate'],
                            groupBy: 'cityId',
                            phrases: phrases,
                            resultProcessCallback: filterLocationDataCallback
                        }).then(function (solrData) {

                            citiesDAO.findByIds(solrData.ids).then(function (data) {

                                var finalData = [];

                                for (var i in data) {
                                    var item = {cityId: solrData.ids[i], name: data[i].name,
                                        count: solrData.counts[i]};
                                    finalData.push(item);
                                }

                                finalResults['locationFilterData'] = {countOfAll: solrData.countOfAll,
                                    data: finalData};

                                resolve();
                            });

                        });
                    });

                    var prepareCompanyData = new Promise(function (resolve, reject) {
                        if (!(query.vacancyTitle || query.category || query.city || query.salaryRange)) {
                            resolve();
                            return;
                        }
                        var cq = JSON.parse(JSON.stringify(query));

                        cq['from'] = 0;
                        cq['size'] = bigNumber;
                        cq['companyName'] = null;

                        solrHelper.queryVacancy(cq, {
                            fieldName: ['vacancyCompanyNameExact', 'createDate'],
                            groupBy: 'vacancyCompanyNameExact',
                            phrases: phrases,
                            resultProcessCallback: filterCompanyDataCallback
                        }).then(function (solrData) {

                            finalResults['companyFilterData'] = solrData;

                            resolve();
                        });
                    });




                    var findIds = new Promise(function (resolve, reject) {

                        solrHelper.queryVacancy(query, {fieldName: 'dataJson',
                            resultProcessCallback: solrHelper.common.filterJsonCallback,
                            phrases: phrases}).then(function (solrData)
                        {

                            finalResults['results'] = solrData.data;
                            finalResults['total'] = solrData.count;
                            resolve();

                        }).catch(function (error)
                        {
                            reject(error);
                        });

                    });


                    Promise.all([findIds, prepareCompanyData, prepareLocationData])
                            .then(function () {
                                resolve(finalResults);
                            });
                });
            });



        }

        return {
            find: find
        };
    }

    module.exports = {
        create: create
    };

})();
