(function ()
{
    'use strict';

    var Promise = require('bluebird');
    var _ = require('lodash');
    var sm = require('sitemap');
    var config = require('../config/config');
    var security = require('./security');

    function create(context, business)
    {
        function generateUrls()
        {
            var urls = [
                {url: config.frontendBaseUrl, changefreq: 'monthly'},
                {url: config.frontendBaseUrl + '/vacancies', changefreq: 'monthly'}
            ];

            return business.getVacanciesManager(context).search({
                approved: true,
                publishMode: 'website'
            }).then(function (vacancies)
            {
                _.forEach(vacancies.results, function (elem)
                {
                    urls.push({
                        url: config.frontendBaseUrl + '/vacancy/' + elem.id,
                        changefreq: 'daily'
                    });
                });

                return business.getCitiesManager(context).getAllCities();
            }).then(function (cities)
            {
                _.forEach(cities, function (elem)
                {
                    urls.push({
                        url: config.frontendBaseUrl + '/vacancies/city/' + elem.name.en,
                        changefreq: 'daily'
                    });
                    urls.push({
                        url: config.frontendBaseUrl + '/vacancies/city/' + elem.name.ru,
                        changefreq: 'daily'
                    });
                });

                return business.getUsersManager(context).carousel();
            }).then(function (profiles)
            {
                _.forEach(profiles.results, function (elem)
                {
                    urls.push({
                        url: config.frontendBaseUrl + '/user/' + elem.id + '/profile',
                        changefreq: 'daily'
                    });
                });
                return business.getCmsManager({user: {role: security.ADMIN}}).query({});
            }).then(function (cmsPages)
            {
                _.forEach(cmsPages.results, function (elem)
                {
                    urls.push({
                        url: config.frontendBaseUrl + '/page/' + elem.slug,
                        changefreq: 'monthly'
                    });
                });

                return urls;
            });
        }

        function createSiteMap()
        {
            var siteMapData = {
                hostname: config.frontendBaseUrl,
                cacheTime: 600000
            };

            return generateUrls().then(function (urls)
            {
                siteMapData.urls = urls;
                var generator = sm.createSitemap(siteMapData);
                return Promise.promisify(generator.toXML, generator)();
            });
        }

        return {
            createSiteMap: createSiteMap
        };
    }

    module.exports = {
        create: create
    };
})();
