(function ()
{
    'use strict';

    var settingsDAO = require('../DAO/settingsDAO');
    var usersDAO = require('../DAO/usersDAO');
    var vacanciesDAO = require('../DAO/vacanciesDAO');
    var security = require('./security');
    var _ = require('lodash');

    function create(context, business)
    {
        function query()
        {
            return security.requireAdmin(context).then(function ()
            {
                return settingsDAO.get({
                    userExpirationTime: 1,
                    paymentModeEnabled: 1,
                    backendUri: 1
                });
            });
        }

        function get()
        {
            var projection = {};

            if(arguments.length) {
                _.map(arguments, function(elem)
                {
                    projection[elem] = 1;
                });
            }

            var requiredRole;

            //If there is only userExpirationTime, then grant access for user
            if(arguments.length == 1 && 'userExpirationTime' in projection){
                requiredRole = security.requireUser(context);
            }else{
                requiredRole = security.requireAdmin(context);
            }

            return requiredRole.then(function ()
            {
                return settingsDAO.get(projection);
            });
        }

        function set(settings)
        {
            return security.requireAdmin(context).then(function ()
            {
                return settingsDAO.set(settings);
            });
        }

        function getPublicSettings()
        {
            return settingsDAO.get();
        }

        function getStatistics()
        {
            var users;
            var companies;

            var filter = {
                firstName: {$exists: true, $ne: ''},
                thumbnail: {$exists: true, $ne: ''},
                hidden: false,
                banned: false,
                verified: true,
                role: 'normal'
            };
            return usersDAO.query(filter).then(function (data)
            {
                users = data.total;

                filter.role = 'employer';
                return usersDAO.query(filter);
            }).then(function (data)
            {
                companies = data.total;

                filter = {
                    verified : true,
                    archived : false,
                    banned : false,
                    approved : true,
                    publishViaEmail : false,
                    publishOnWebsite : true
                };
                return vacanciesDAO.query(filter);
            }).then(function (data)
            {
                return {
                    users: users,
                    companies: companies,
                    vacancies: data.total
                };
            });
        }

        return {
            query: query,
            get: get,
            set: set,
            getPublicSettings: getPublicSettings,
            getStatistics: getStatistics
        };
    }

    module.exports = {
        create: create
    };
})();
