(function ()
{
    'use strict';

    var _ = require('lodash');
    var applicationException = require('../service/applicationException');
    var Promise = require('bluebird');
    var settingsDAO = require('../DAO/settingsDAO');
    var UsersPermissionsDAO = require('../DAO/usersPermissionsDAO');

    function isAuthenticated(context)
    {
        return context && null != context.user;
    }

    function isSystem(context)
    {
        return new Promise(function (resolve, reject)
        {
            if (!context || !context.user || 'system' !== context.user.nickName) {
                reject(applicationException.new(applicationException.UNAUTHORIZED, 'Invalid user!'));
            } else {
                resolve();
            }
        });
    }

    function requireUser(context)
    {
        return new Promise(function (resolve, reject)
        {
            if (isAuthenticated(context)) {
                resolve();
            } else {
                reject(applicationException.new(applicationException.FORBIDDEN));
            }
        });
    }

    function checkAuthor(userId, context)
    {
        return new Promise(function (resolve, reject)
        {
            if (!context || !context.user || userId !== context.user.id.toString()) {
                reject(applicationException.new(applicationException.FORBIDDEN));
            } else {
                resolve();
            }
        });
    }

    function isAdmin(context)
    {
        return context && context.user && context.user.role && 'admin' === context.user.role;
    }

    function isEmployer(context)
    {
        return context && context.user && context.user.role && 'employer' === context.user.role;
    }

    function isJobSeeker(context)
    {
        return context && context.user && context.user.role && 'normal' === context.user.role;
    }

    function requireAdmin(context)
    {
        return new Promise(function (resolve, reject)
        {
            if (!isAuthenticated(context)) {
                reject(applicationException.new(applicationException.UNAUTHORIZED, 'You don\'t have access to this page or resource'));
            } else if (!isAdmin(context)) {
                reject(applicationException.new(applicationException.FORBIDDEN, 'You don\'t have access to this page or resource'));
            } else {
                resolve();
            }
        });
    }

    function requireEmployer(context)
    {
        return new Promise(function (resolve, reject)
        {
            if (!isAuthenticated(context)) {
                reject(applicationException.new(applicationException.UNAUTHORIZED, 'You don\'t have access to this page or resource'));
            } else if (!isEmployer(context)) {
                reject(applicationException.new(applicationException.FORBIDDEN, 'You don\'t have access to this page or resource'));
            } else {
                resolve();
            }
        });
    }

    function checkRoles(context)
    {
        var user = context && context.user;
        return new Promise(function (resolve, reject)
        {
            if (!user || !user.role) {
                reject();
            } else if (_.intersection(user.role, arguments).length) {
                resolve();
            } else {
                reject();
            }
        });
    }

    function checkUser(email, nickName, password)
    {
        var regexEmail = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.exec(email);
        return new Promise(function (resolve, reject)
        {
            if (password && nickName && email && regexEmail) {
                resolve();
            } else {
                reject(applicationException.new(applicationException.VALIDATION_FAILURE));
            }
        });
    }

    function checkPermission(userId, permissionName)
    {
        return settingsDAO.get().then(function (settings)
        {
            if(settings.paymentModeEnabled) {
                return UsersPermissionsDAO.get(userId).then(function (permissions)
                {
                    if(!permissions[permissionName]) {
                        return Promise.reject(applicationException.new(applicationException.UNAUTHORIZED));
                    }
                });
            }
        });
    }

    function requireVacancyPermission(context)
    {
        return requireEmployer(context).then(function ()
        {
            return checkPermission(context.user.id, 'vacancyAdding');
        });
    }

    function requireCvSearchingPermission(context)
    {
        return requireEmployer(context).then(function ()
        {
            return checkPermission(context.user.id, 'cvSearching');
        });
    }

    module.exports = {
        ADMIN: 'admin',
        USER: 'normal',
        EMPLOYER: 'employer',
        isAdmin: isAdmin,
        isEmployer: isEmployer,
        isJobSeeker: isJobSeeker,
        isSystem: isSystem,
        isAuthenticated: isAuthenticated,
        checkAuthor: checkAuthor,
        checkRoles: checkRoles,
        checkUser: checkUser,
        requireAdmin: requireAdmin,
        requireEmployer: requireEmployer,
        requireUser: requireUser,
        requireVacancyPermission: requireVacancyPermission,
        requireCvSearchingPermission: requireCvSearchingPermission
    };
})();
