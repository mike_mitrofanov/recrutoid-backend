(function ()
{
    'use strict';
    var security = require('./security');
    var LanguagesDAO = require('../DAO/languagesDAO');
    var applicationException = require('../service/applicationException');
    var Promise = require('bluebird');


    function create(context)
    {
        function createNewOrUpdate(language)
        {
            return security.requireAdmin(context).then(function ()
            {
                return LanguagesDAO.createNewOrUpdate(language);
            });
        }

        function query()
        {
            return LanguagesDAO.query();
        }

        function remove(languageId)
        {
            return security.requireAdmin(context).then(function ()
            {
                return LanguagesDAO.remove(languageId);
            });
        }

        return {
            query: query,
            remove: remove,
            createNewOrUpdate: createNewOrUpdate
        };
    }

    module.exports = {
        create: create
    };
})();
