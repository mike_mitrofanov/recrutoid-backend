(function ()
{
    'use strict';

    var subscriptionsManager = require('./subscriptions.manager');
    var usersManager = require('./users.manager');
    var citiesManager = require('./cities.manager');
    var salaryRange = require('./salaryRanges.manager');
    var categoriesManager = require('./categories.manager');
    var vacanciesManager = require('./vacancies.manager');
    var commonManager = require('./common.manager');
    var languagesManger = require('./languages.manager');
    var emailManager = require('./emails.manager');
    var distributorsManager = require('./distributors.manager');
    var blacklistedDomainsManager = require('./blacklistedDomains.manager');
    var cmsManager = require('./cms.manager');
    var bannersManager = require('./banners.manager');
    var settingsManager = require('./settings.manager');
    var seoManager = require('./seo.manager');
    var bugreportsManager = require('./bugreports.manager');
    var translationsManager = require('./translations.manager');
    var phrasesManager = require('./phrases.manager');
    var cvfilesManager = require('./cvfiles.manager');
    var intervalManager = require('./interval.manager');
    var exporterManager = require('./exporter.manager');
    var emailErrorsManager = require('./emailErrors.manager');
    var security = require('./security');
    var vacanciesSearchManager = require('./vacancies.search.manager');
    var usersSearchManager = require('./users.search.manager');
    var autocompleteManager = require('./autocomplete.manager');
    
    function getContext(request)
    {
        return {user: request && request.user};
    }

    function getter(manager)
    {
        return function (request)
        {
            return manager.create(getContext(request), module.exports);
        };
    }

    var managers = {
        getSubscriptionsManager: getter(subscriptionsManager),
        getUsersManager: getter(usersManager),
        getCitiesManager: getter(citiesManager),
        getSalaryRangesManager: getter(salaryRange),
        getCategoriesManager: getter(categoriesManager),
        getVacanciesManager: getter(vacanciesManager),
        getCommonManager: getter(commonManager),
        getLanguagesManager: getter(languagesManger),
        getEmailManager: getter(emailManager),
        getDistributorsManager: getter(distributorsManager),
        getBlacklistedDomainsManager: getter(blacklistedDomainsManager),
        getCmsManager: getter(cmsManager),
        getBannersManager: getter(bannersManager),
        getSettingsManager: getter(settingsManager),
        getSeoManager: getter(seoManager),
        getBugreportsManager: getter(bugreportsManager),
        getTranslationsManager: getter(translationsManager),
        getPhrasesManager: getter(phrasesManager),
        getCvfilesManager: getter(cvfilesManager),
        getIntervalManager: getter(intervalManager),
        getExporterManager: getter(exporterManager),
        getEmailErrorsManager: getter(emailErrorsManager),
        getVacanciesSearchManager: getter(vacanciesSearchManager),
        getUsersSearchManager: getter(usersSearchManager),
        getAutocompleteManager: getter(autocompleteManager)
    };
    module.exports = managers;

    managers.getIntervalManager({user: {role: security.ADMIN}}).startInterval();
})();
