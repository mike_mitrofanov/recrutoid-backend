(function ()
{
    'use strict';
    var _ = require('lodash');
    var applicationException = require('../service/applicationException');
    var Promise = require('bluebird');
    var daoFilterHelper = require('../util/daoFilterHelper');
    var dateHelper = require('../service/dateHelper');
    var vacanciesDAO = require('../DAO/vacanciesDAO');
    var phrasesDAO = require('../DAO/phrasesDAO');
    var citiesDAO = require('../DAO/citiesDAO');
    var usersDAO = require('../DAO/usersDAO');
    var emailTokensDAO = require('../DAO/emailTokensDAO');
    var sparkPost = require('../service/sparkPost');
    var CategoryDAO = require('../DAO/categoriesDAO');
    var security = require('./security.js');
    var solrHelper = require('../service/solrHelper');

    function create(context, business)
    {
        function createTokenAndSendEmail(vacancy, password)
        {
            //TODO add language to request
            CategoryDAO.findCategoriesByArrayIdCategories(vacancy.categories).then(function (results)
            {
                vacancy.categories = results;
                emailTokensDAO.create(vacancy.id, 'vacancy', vacancy.secondId).then(function (token)
                {
                    sparkPost.createTokenAndSendEmail(token.token, vacancy.email, 'vacancy', vacancy, password).catch(business.getEmailErrorsManager(context).logErrors);
                });
            });
        }

        function sendNewVacancyNotification(vacancy) {
            usersDAO.model.find({role: security.ADMIN}).then(function (results) {
                var emails = _.pluck(results, 'email');
                sparkPost.sendNewVacancyNotification(vacancy, emails)
                        .catch(business.getEmailErrorsManager(context).logErrors);
            });
        }

        function doCreate(vacancy, publishOptions) {

            var promise = Promise.resolve();
            var id = [];

            if (publishOptions.email) {
                promise = promise.then(function ()
                {
                    vacancy.approved = false;
                    vacancy.publishOnWebsite = false;
                    vacancy.publishViaEmail = true;
                    return vacanciesDAO.createNewOrUpdate(vacancy).then(function (vac)
                    {
                        var populateCategories = {
                            path: 'categories',
                            model: 'categories'
                        };
                        var populateCity = {
                            path: 'cityId',
                            model: 'cities'
                        };
                        var populateSalaryRange = {
                            path: 'salaryRangeId',
                            model: 'salary_ranges'
                        };

                        vacanciesDAO.model.populate(vac, [populateCity, populateSalaryRange, populateCategories], function (err, vac) {
                            sendNewVacancyNotification(vac);
                        });
                        id.push(vac.id);
                    });
                });
            }

            if (publishOptions.website) {
                promise = promise.then(function ()
                {
                    vacancy.approved = true;
                    vacancy.publishOnWebsite = true;
                    vacancy.publishViaEmail = false;
                    return vacanciesDAO.createNewOrUpdate(vacancy).then(function (vac)
                    {
                        id.push(vac.id);
                    });
                });
            }

            return promise.then(function ()
            {
                vacancy.publishOnWebsite = publishOptions.website;
                vacancy.publishViaEmail = publishOptions.email;
                vacancy.id = id[0];
                vacancy.secondId = id[1];

                return vacancy;
            });
        }

        function createNewVacancy(vacancy)
        {
            vacancy.approved = false;
            vacancy.archived = false;
            vacancy.verified = false;
            if (security.isAuthenticated(context)) {
                vacancy.userId = context.user.id;
                vacancy.email = context.user.email;

            }

            if (vacancy.publishOnWebsite || vacancy.publishViaEmail) {
                var publishOptions = {
                    email: vacancy.publishViaEmail,
                    website: vacancy.publishOnWebsite
                };
                if (!security.isAuthenticated(context)) {
                    var password = vacancy.password;
                    delete vacancy.password;
                    return business.getUsersManager(context).registerEmployer({
                        email: vacancy.email,
                        password: password,
                        firstName: vacancy.companyName
                    }).then(function (user)
                    {
                        vacancy.userId = user.id;
                        vacancy.email = user.email;
                        return doCreate(vacancy, publishOptions).then(function (vac)
                        {
                            createTokenAndSendEmail(vac, password);
                            return vac;
                        });
                    });
                } else {
                    return security.requireVacancyPermission(context).then(function ()
                    {
                        vacancy.verified = true;
                        return doCreate(vacancy, publishOptions);
                    });
                }
            } else {
                return Promise.reject(applicationException.new(applicationException.CONFLICT));
            }
        }

        function createNewOrUpdate(vacancy)
        {


            var res;
            if (!vacancy.id) {
                return createNewVacancy(vacancy).then(function (vac) {
                    if (vac.secondId) {

                        solrHelper.pushVacancy(vac.secondId + "", true);
                    }
                    solrHelper.pushVacancy(vac.id + "", true);

                });
            } else {
                delete vacancy.verified;
                return vacanciesDAO.createNewOrUpdate(vacancy).then(function (vac) {
                    solrHelper.pushVacancy(vac.id + "", true);

                });
            }





        }

        function dispatchVacancies(manually)
        {
            if (manually !== false) {
                manually = true;
            }

            if (!security.isAdmin(context)) {
                return Promise.reject(applicationException.new(applicationException.FORBIDDEN));
            }


            function setOffsetAndContext(vacancies, language)
            {
                language = language || 'en';
                for (var i = 0; i < vacancies.length; i++) {
                    vacancies[i].offset = dateHelper.moment(vacancies[i].createDate).format('D.M.YYYY');
                    vacancies[i].context = require('../config/config').frontendBaseUrl + '/vacancy/' + vacancies[i].id;
                    vacancies[i].cityId = {name: vacancies[i].cityId ? vacancies[i].cityId.name[language] : ''};
                }
            }

            var query = ['daily'];
            var dayOfWeek = parseInt(dateHelper.getMoment().format('E'), 10);

            if (manually || dayOfWeek === 6) {
                query.push('weekly');
            }
            var count = 0;
            return business.getSubscriptionsManager(context).getSubscriptionsForDispatch(query).then(function (subscriptions)
            {
                var promises = _.map(subscriptions.results, function (subscription)
                {
                    var vacanciesManagerInNewContext = create({user: {email: subscription.email}});
                    return vacanciesManagerInNewContext.getVacanciesToDispatch(subscription).then(function (vacancies)
                    {
                        if (_.isEmpty(vacancies)) {
                            return null;
                        }
                        setOffsetAndContext(vacancies);

                        if (null == subscription.categories[0]) {
                            subscription.categories = [{name: {en: 'All categories'}}];
                        }

                        return sparkPost.dispatchVacancies(subscription, vacancies, subscription.categories).then(function () {
                            _.forEach(vacancies, function (vacancy)
                            {
                                count += 1;
                                vacanciesManagerInNewContext.markSeen(vacancy.id);
                                vacanciesDAO.incrementEmailSent(vacancy.id);
                            });
                        }, business.getEmailErrorsManager(context).logErrors);
                    });

                });

                return Promise.all(promises).then(function ()
                {
                    return {
                        sent: count
                    };
                });
            });
        }

        function get(id)
        {
            return vacanciesDAO.get(id).then(function (result)
            {
                if (true !== result.approved) {
                    if (!security.isAuthenticated(context)) {
                        throw applicationException.new(applicationException.UNAUTHORIZED);
                    }
                    var isNotOwner = (null == context.user.id || null == result.userId || context.user.id.toString() !== result.userId._id.toString());
                    if (!security.isAdmin(context) && isNotOwner) {
                        throw applicationException.new(applicationException.FORBIDDEN);
                    }
                }
                return result;
            }).catch(function (error)
            {
                if (error.error && 404 === error.error.code) {
                    return emailTokensDAO.get(id).then(function (token)
                    {
                        if ('vacancy' !== token.type) {
                            throw applicationException.new(applicationException.NOT_FOUND);
                        }

                        return vacanciesDAO.get(token.originId).then(function (vacancy)
                        {
                            vacancy.token = token.id;
                            return vacancy;
                        });
                    });
                } else if (applicationException.is(error, applicationException.UNAUTHORIZED) ||
                        applicationException.is(error, applicationException.FORBIDDEN)) {
                    throw error;
                } else {
                    throw applicationException.new(applicationException.NOT_FOUND);
                }
            });
        }

        function getVacanciesToDispatch(subscription)
        {
            var startDate = dateHelper.getMoment().startOf('day').subtract(14, 'days').valueOf();
            var filter = {publishedDate: {$gte: startDate}, publishMode: subscription.interval == 'daily'?'email':'website', size: -1};

            if (null != subscription.categories[0]) {
                filter.categories = _.map(subscription.categories, function (item)
                {
                    return item.id || item;
                });
            }
            if (null != subscription.desiredSalaryRange) {
                subscription.desiredSalaryRange.ranges.push(subscription.desiredSalaryRange.id || subscription.desiredSalaryRange._id);
                filter.dispatchSalaryRanges = {$in: subscription.desiredSalaryRange.ranges};
            }

            filter.dispatch = true;

            return citiesDAO.model.find({oilGasCamp: true}).then(function(cities) {
                var gasOilCamps = _.pluck(cities, '_id');

                if (null != subscription.city) {
                    gasOilCamps.push(subscription.city.id || subscription.city._id);
                    filter.dispatchCities = {$in: gasOilCamps};
                }
                return search(filter).then(function (result)
                {
                    var ids = _.pluck(result.results, 'id');
                    return isNew(ids).then(function (hash)
                    {
                        return _.filter(result.results, function (item)
                        {
                            return !!hash[item.id];
                        });
                    });
                });
            });
        }

        function markSeen(id)
        {
            if (security.isAuthenticated(context)) {
                return vacanciesDAO.markSeen(context.user.email, id);
            } else {
                return Promise.resolve();
            }
        }

        function getSendingViaEmails(query)
        {
            query.approved = false;
            query.publishViaEmail = true;
            return vacanciesDAO.query(query);
        }

        function getPublishedOnWebsite(query)
        {
            query.approved = true;
            query.publishOnWebsite = true;
            return vacanciesDAO.query(query);
        }

        function getArchived(query)
        {
            query.archived = true;
            return vacanciesDAO.query(query);
        }

        function getUnapproved(query)
        {
            query.approved = false;
            query.verified = true;
            return vacanciesDAO.query(query);
        }

        function search(filter)
        {
            var query = {};
            var promise;
            daoFilterHelper.copy(query, filter, 'publishedDate');
            daoFilterHelper.copyBoolean(query, filter, 'approved');
            daoFilterHelper.copyBoolean(query, filter, 'verified');
            daoFilterHelper.copyBoolean(query, filter, 'archived');
            daoFilterHelper.copyBoolean(query, filter, 'dispatch');
            daoFilterHelper.copy(query, filter, 'title');
            daoFilterHelper.copy(query, filter, 'email');
            daoFilterHelper.copy(query, filter, 'companyName');
            daoFilterHelper.copy(query, filter, 'categories');
            daoFilterHelper.copy(query, filter, 'emailOrTitle');
            daoFilterHelper.copy(query, filter, 'city', 'cityId');
            daoFilterHelper.copy(query, filter, 'dispatchCities', 'dispatchCities');
            daoFilterHelper.copy(query, filter, 'dispatchSalaryRanges', 'dispatchSalaryRanges');
            daoFilterHelper.copy(query, filter, 'salaryRange', 'salaryRangeId');
            daoFilterHelper.copyBoolean(query, filter, 'registeredUser');
            var onlyMy = filter.myAll && security.isEmployer(context);
            var callForInterview = filter.my && security.isEmployer(context);

            if (((filter.myAll || filter.my) && !security.isEmployer(context))
                    || ('email' === filter.publishMode && (!security.isAdmin(context) && !filter.dispatch)) && !filter.myAll
                    || (filter.adminPanel && !security.isAdmin(context))) {
                return Promise.reject(applicationException.new(applicationException.UNAUTHORIZED));
            }

            if (onlyMy || callForInterview) {
                daoFilterHelper.copy(query, context.user, 'id', 'userId');
            }
            if ('website' === filter.publishMode) {
                query.publishOnWebsite = true;
            } else if ('email' === filter.publishMode) {
                query.publishViaEmail = true;
            }


            query.from = filter.from;
            query.size = filter.size;
            if (!filter.adminPanel) {
                if (!onlyMy || callForInterview) {
                    query.approved = true;
                    query.archived = false;
                    query.banned = false;
                }
                //return all records for interview invitation
//                if (callForInterview) {
//                    promise = usersDAO.findUserOffersIds(filter.userId).then(function (data)
//                    {
//                        console.log(data);
//                        query._id = data;
//                        return vacanciesDAO.query(query);
//                    });
                if (filter.mainSearching) {
                    promise = vacanciesDAO.find(query);
                } else {
                    promise = vacanciesDAO.query(query);
                }
                delete query.registeredUser;
            } else {
                promise = vacanciesDAO.query(query);
            }

            return promise;
        }

        function find(query)
        {
            var filter = {};
            var finalResults;
            var companyFilterData;
            var locationFilterData;

            daoFilterHelper.copy(filter, query, 'vacancyTitle');
            daoFilterHelper.copy(filter, query, 'category', 'categories');
            daoFilterHelper.copy(filter, query, 'city', 'cityId');
            daoFilterHelper.copy(filter, query, 'days');
            daoFilterHelper.copy(filter, query, 'salaryRange', 'salaryRangeId');

            var from = parseInt(query.from, 10);
            var size = parseInt(query.size, 10);


            return phrasesDAO.getPhrasesForVacancy(filter).then(function (results) {
                var filter = results;

                return vacanciesDAO.find(filter).then(function (results)
                {
                    finalResults = results;
                }).then(function ()
                {
                    if (filter.vacancyTitle || filter.categories || filter.cityId || filter.salaryRangeId) {
                        var companies = _.groupBy(finalResults, function (item) {
                            return item.companyName;
                        });

                        var companyData = _.map(companies, function (item, key) {
                            return {
                                name: key,
                                count: item.length
                            };
                        });

                        companyFilterData = {
                            countOfAll: companyData.length,
                            data: companyData.slice(0, 10)
                        };
                    }
                }).then(function ()
                {
                    if (filter.vacancyTitle || filter.categories) {
                        if (filter.vacancyTitle || filter.categories) {
                            var cities = _.groupBy(finalResults, function (item) {
                                return item.cityId._id;
                            });

                            var citiesData = _.map(cities, function (item, key) {
                                return {
                                    cityId: key,
                                    name: item[0].cityId.name,
                                    count: item.length
                                };
                            });

                            locationFilterData = {
                                countOfAll: citiesData.length,
                                data: citiesData.slice(0, 10)
                            };
                        }
                    }
                }).then(function ()
                {
                    if (query.companyName) {
                        finalResults = _.filter(finalResults, function (item) {
                            return item.companyName == query.companyName;
                        });
                    }

                    return {
                        results: finalResults.slice(from, from + size),
                        total: finalResults.length,
                        companyFilterData: companyFilterData || void 0,
                        locationFilterData: locationFilterData || void 0
                    };
                });
            });
        }

        function getNotVerified(query)
        {
            query.verified = false;
            return vacanciesDAO.query(query);
        }

        function getSpecific(type, query)
        {
            switch (type) {
                case 'email':
                    return getSendingViaEmails(query);
                case 'website':
                    return getPublishedOnWebsite(query);
                case 'archived':
                    return getArchived(query);
                case 'unapproved':
                    return getUnapproved(query);
                case 'notverified':
                    return getNotVerified(query);
                default:
                    return Promise.reject(applicationException.new(applicationException.NOT_FOUND));
            }
        }

        function remove(id)
        {
            return usersDAO.removeUserOffers(id).then(function ()
            {
                var res = vacanciesDAO.remove(id);
                res.then(function () {
                    solrHelper.dropVacancy(id);
                });
                return res;
            });
        }

        function removeArray(ids)
        {
            if (!Array.isArray(ids)) {
                ids = [ids];
            }
            return Promise.reduce(ids, function (sum, next)
            {
                return remove(next);
            }, 0);
        }

        function getUserIdByVacancyToken(token)
        {
            return checkVacancyTokenCorrectness(token).then(function (token)
            {
                return vacanciesDAO.get(token.originId);
            }).then(function (vacancy)
            {
                return vacancy.userId;
            });
        }

        function checkVacancyTokenCorrectness(token)
        {
            return emailTokensDAO.get(token).then(function (token)
            {
                if (token.type !== 'vacancy') {
                    return Promise.reject(applicationException.new(applicationException.FORBIDDEN));
                }

                return token;
            });
        }

        function verifyVacancyUsingToken(id, data)
        {
            var promises = [];
            return checkVacancyTokenCorrectness(id).then(function (token)
            {
                promises.push(vacanciesDAO.get(token.originId));
                if (token.secondOriginId) {
                    promises.push(vacanciesDAO.get(token.secondOriginId));
                }

                return Promise.all(promises);
            }).then(function (vacancies)
            {
                var promise;
                if (vacancies[0].publishOnWebsite) {
                    promise = business.getUsersManager(context).findByEmailAndActivate(vacancies[0].email).then(function ()
                    {
                        return vacanciesDAO.update(vacancies[0].id, {
                            verified: true
                        });
                    });
                } else {
                    promise = vacanciesDAO.update(vacancies[0].id, {
                        verified: true
                    });
                }

                if (vacancies[1]) {
                    if (vacancies[1].publishOnWebsite) {
                        promise = business.getUsersManager(context).findByEmailAndActivate(vacancies[1].email).then(function ()
                        {
                            return vacanciesDAO.update(vacancies[1].id, {
                                verified: true
                            });
                        });
                    } else {
                        promise = vacanciesDAO.update(vacancies[1].id, {
                            verified: true
                        });
                    }
                }

                return promise;
            });
        }

        function approveVacancy(id)
        {
            return security.requireAdmin(context).then(function ()
            {
                return vacanciesDAO.update(id, {approved: true, publishedDate: dateHelper.get()}, {new : true}).then(function (results)
                {
                    return sparkPost.approveVacancy(results).catch(business.getEmailErrorsManager(context).logErrors);

                });
            });
        }

        function verifyVacancy(id)
        {
            return security.requireAdmin(context).then(function ()
            {
                return vacanciesDAO.update(id, {verified: true});
            });

        }

        function verifyVacancyAndUser(id) {
            return security.requireAdmin(context).then(function ()
            {
                return vacanciesDAO.model.findOne({'_id': id}).then(function (vacancy) {
                    return business.getUsersManager(context).verify(vacancy.userId);
                });

            });
        }

        function ban(id)
        {
            return security.requireAdmin(context).then(function ()
            {
                return vacanciesDAO.update(id, {banned: true});
            });

        }

        function unban(id)
        {
            return security.requireAdmin(context).then(function ()
            {
                return vacanciesDAO.update(id, {banned: false});
            });

        }

        function performAction(id, action, data)
        {

            var action;
            if ('verify' === action) {
                action = verifyVacancy(id);
            } else if ('verifyAll' === action) {
                return verifyVacancyAndUser(id);
            } else if ('verifyByToken' === action) {
                return verifyVacancyUsingToken(id, data);
            } else if ('ban' === action) {
                action = ban(id);
            } else if ('unban' === action) {
                action = unban(id);
            } else if ('approve' === action) {
                action = approveVacancy(id);
            } else {
                return Promise.reject(applicationException.NOT_FOUND);
            }

            if (action) {
                action.then(function () {
                    solrHelper.pushVacancy(id, true);
                });
            }

            return action;
        }

        function findAnonymousUsersWithVacancies(query)
        {
            query.userId = null;
            return vacanciesDAO.query(query);
        }

        function isNew(vacancyIds, shouldMarkAsSeen)
        {
            return security.requireUser(context).then(function ()
            {
                return vacanciesDAO.isNew(context.user.email, vacancyIds).then(function (result)
                {
                    if (shouldMarkAsSeen) {
                        var notSeen = [];
                        _.map(result, function (elem, prop)
                        {
                            if (elem) {
                                notSeen.push(prop);
                            }
                        });
                        vacanciesDAO.markManySeen(context.user.email, notSeen);
                    }

                    return result;
                });
            }).catch(function ()
            {
                var result = {};
                _.forEach(vacancyIds, function (value)
                {
                    result[value] = true;
                });
                return Promise.resolve(result);
            });
        }

        function isNotVisitedList(vacancyIds, shouldMarkAsSeen)
        {
            return security.requireUser(context).then(function ()
            {
                return vacanciesDAO.isNotVisitedList(context.user.email, vacancyIds).then(function (result)
                {
                    if (shouldMarkAsSeen) {
                        var notSeen = [];
                        _.map(result, function (elem, prop)
                        {
                            if (elem) {
                                notSeen.push(prop);
                            }
                        });
                        vacanciesDAO.markManySeen(context.user.email, notSeen);
                    }

                    return result;
                });
            }).catch(function ()
            {
                var result = {};
                _.forEach(vacancyIds, function (value)
                {
                    result[value] = true;
                });
                return Promise.resolve(result);
            });
        }

        function isNotVisited(vacancyIds)
        {

            return security.requireUser(context).then(function ()
            {
                return vacanciesDAO.isNotVisited(vacancyIds).then(function (result)
                {
                    if (shouldMarkAsSeen) {
                        var notSeen = [];
                        _.map(result, function (elem, prop)
                        {
                            if (elem) {
                                notSeen.push(prop);
                            }
                        });
                        vacanciesDAO.markManySeen(context.user.email, notSeen);
                    }

                    return result;
                });
            }).catch(function ()
            {
                var result = {};
                _.forEach(vacancyIds, function (value)
                {
                    result[value] = true;
                });
                return Promise.resolve(result);
            });
        }

        function applyTo(vacancyId)
        {
            return security.requireUser(context).then(function ()
            {
                return vacanciesDAO.applyTo(vacancyId, context.user.id).then(function () {
                     solrHelper.pushVacancy(vacancyId + "", true);
                });
            }).then(function ()
            {
                return vacanciesDAO.get(vacancyId);
            });
        }

        function queryApplications(id, filter)
        {
            if (!security.isAuthenticated(context)) {
                return Promise.reject(applicationException.new(applicationException.UNAUTHORIZED));
            }
            if (!security.isEmployer(context)) {
                return Promise.reject(applicationException.new(applicationException.FORBIDDEN));
            }

            var query = {};
            query.from = filter.from;
            query.size = filter.size;
            daoFilterHelper.copy(query, filter, 'city');
            daoFilterHelper.copy(query, filter, 'languages');

            var queriedApplicants = [];

            return vacanciesDAO.queryApplications(id, query, context.user.id).then(function (results)
            {
                if (!results.isOwner) {
                    return Promise.reject(applicationException.new(applicationException.FORBIDDEN));
                }

                results.results = results.results.map(function (elem)
                {
                    if (!elem.seen) {
                        queriedApplicants.push(elem.user.id.toString());
                    }
                    delete elem.user.resumeViews;
                    delete elem.user.offers;
                    elem.user.applicationVisited = elem.seen;
                    elem.user.applicationNew = elem.new;
                    return elem.user;
                });

                return results;
            }).then(function (results)
            {
                markApplicationsSeen(id, queriedApplicants, true);
                return results;
            });
        }

        function removeApplication(vacancyId, applications)
        {
            if (!security.isAuthenticated(context) || !security.isEmployer(context)) {
                return Promise.reject(applicationException.new(applicationException.FORBIDDEN));
            }

            return vacanciesDAO.get({_id: vacancyId}).then(function (vacancy)
            {
                if (context.user.id.toString() !== vacancy.userId._id.toString()) {
                    return Promise.reject(applicationException.new(applicationException.FORBIDDEN));
                }

                return vacanciesDAO.removeApplications(vacancyId, applications);
            });
        }

        function markApplicationsSeen(vacancyId, applicants, justNewStatus)
        {
            if (!security.isAuthenticated(context) || !security.isEmployer(context)) {
                return Promise.reject(applicationException.new(applicationException.FORBIDDEN));
            }

            return vacanciesDAO.get({_id: vacancyId}).then(function (vacancy)
            {
                if (context.user.id.toString() !== vacancy.userId._id.toString()) {
                    return Promise.reject(applicationException.new(applicationException.FORBIDDEN));
                }

                var vacancyToUpdate = {
                    applicants: _.map(vacancy.applicants, function (elem)
                    {
                        if (-1 < applicants.indexOf(elem.user.toString())) {
                            elem.new = false;
                            if (!justNewStatus) {
                                elem.seen = true;
                            }
                        }
                        return elem;
                    })
                };

                return vacanciesDAO.update(vacancyId, vacancyToUpdate);
            });
        }

        function getNumberOfNewApplications()
        {
            return security.requireEmployer(context).then(function ()
            {
                return vacanciesDAO.countNewApplicationsByUserId(context.user.id);
            }).then(function (count)
            {
                return {count: count};
            });
        }

        function removeOldVacancy()
        {
            return vacanciesDAO.removeOldVacancy();
        }

        function removeOldDistributions()
        {
            return vacanciesDAO.removeOldDistributions();
        }

        function createTokenAndSendRemind(vacancy)
        {
            return vacanciesDAO.createExtendToken(vacancy.id).then(function (token)
            {
                return sparkPost.sendReminderToOldVacancy(vacancy, token.id);
            });
        }

        function sendReminderVacancy()
        {
            var oneDay = (24 * 3600000);
            return vacanciesDAO.searchBySystem({
                extendDate: {
                    $gte: (dateHelper.get() - vacanciesDAO.offset) + oneDay + 1,
                    $lte: (dateHelper.get() - vacanciesDAO.offset) + (2 * oneDay)
                },
                approved: true,
                archived: false
            }, {
                email: 1, title: 1, companyName: 1, _id: 1
            }).then(function (vacancies)
            {
                var promises = _.map(vacancies, createTokenAndSendRemind);
                return Promise.all(promises);
            });
        }

        function extendPublication(token)
        {
            return vacanciesDAO.getVacancyFromExtendToken(token).then(function (vacancy)
            {
                vacancy.archived = false;
                vacancy.extendDate = dateHelper.get();
                return vacanciesDAO.createNewOrUpdate(vacancy);
            });
        }

        return {
            removeOldVacancy: removeOldVacancy,
            removeOldDistributions: removeOldDistributions,
            sendReminderVacancy: sendReminderVacancy,
            createNewOrUpdate: createNewOrUpdate,
            dispatchVacancies: dispatchVacancies,
            get: get,
            getVacanciesToDispatch: getVacanciesToDispatch,
            isNew: isNew,
            isNotVisited: isNotVisited,
            isNotVisitedList: isNotVisitedList,
            markSeen: markSeen,
            remove: remove,
            removeArray: removeArray,
            getSpecific: getSpecific,
            search: search,
            find: find,
            applyTo: applyTo,
            queryApplications: queryApplications,
            removeApplication: removeApplication,
            getNumberOfNewApplications: getNumberOfNewApplications,
            getUserIdByVacancyToken: getUserIdByVacancyToken,
            checkVacancyTokenCorrectness: checkVacancyTokenCorrectness,
            performAction: performAction,
            findAnonymousUsersWithVacancies: findAnonymousUsersWithVacancies,
            extendPublication: extendPublication,
            markApplicationsSeen: markApplicationsSeen,
            createTokenAndSendRemind: createTokenAndSendRemind
        };
    }

    module.exports = {
        create: create
    };

})();
