(function ()
{
    'use strict';

    var Promise = require('bluebird');

  
    var solrHelper = require('../service/solrHelper');


    function create(context, business)
    {
        function vacancy(query) {
           return solrHelper.suggest.vacancy(query.q);
        }
        
         function application(query) {
           return solrHelper.suggest.application(query.q, 'applications', 'resumeTitleExact', ['resumeTitleNgram']);
        }
        
        function company(query) {
            return solrHelper.suggest.company(query.q);
        }

        
        return {
            vacancy: vacancy,
            application: application,
            company: company
        };

    }

    module.exports = {
        create: create
    };
})();


