(function ()
{
    'use strict';

    var _ = require('lodash');
    var mongodb = require('mongodb');
    var applicationException = require('../../service/applicationException');
    var mongoose = require('mongoose-bird')(require('mongoose'));
    var mongoConverter = require('../../DAO/mongoConverter');
    var Promise = require('bluebird');
    var getDate = require('../../service/dateHelper').get;
    var deepPopulate = require('mongoose-deep-populate')(mongoose);
    var dateHelper = require('../../service/dateHelper');
    var vacanciesDAO = require('../../DAO/vacanciesDAO');

    var maxlength = [300, 'The value of path `{PATH}` (`{VALUE}`) exceeds the maximum allowed length ({MAXLENGTH}).'];
    var jobFeedsSchema = new mongoose.Schema({
        text: {type: String, maxlength: maxlength, required: true},
        vacancy: {type: mongoose.Schema.Types.ObjectId, ref: 'vacancies'},
        job_seekers: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'users'
        }],
        published: { type: Date},
        seen_count: {type: Number},
        updated: { type: Date, default: Date.now },
        company: {type: mongoose.Schema.Types.ObjectId, ref: 'users'}
    });
    var JobFeedsModel = mongoose.model('job_feeds', jobFeedsSchema);


    function getAll(filter)
    {
        return JobFeedsModel.find(filter, { job_seekers: { $slice: 5 } }).populate('company').populate('job_seekers').then(function (result)
        {
            console.log(mongoConverter.fromMongo(result))
            return mongoConverter.fromMongo(result);
        });
    }


    module.exports = {
        getAll: getAll,

        model: JobFeedsModel
    };
})();
