(function ()
{
    'use strict';

    // var sha1 = require('sha1');
    // var sizeOf = require('image-size');
    // var applicationException = require('../service/applicationException');
    // var _ = require('lodash');
    // var Promise = require('bluebird');
    // var security = require('./security.js');
    // var resetPasswordTokenDAO = require('../DAO/resetPasswordTokenDAO');
    // var activateAccountTokenDAO = require('../DAO/actiavateAccountToken');
    // var EmailTokensDAO = require('../DAO/emailTokensDAO');
    // var VacancyDAO = require('../DAO/vacanciesDAO');
    // var SubscriptionsDAO = require('../DAO/subscriptionsDAO');
    // var usersDAO = require('../DAO/usersDAO');
    // var passwordsDAO = require('../DAO/passwordsDAO');
    // var tokensDAO = require('../DAO/tokensDAO');
    // var UsersPermissionsDAO = require('../DAO/usersPermissionsDAO');
    // var daoFilterHelper = require('../util/daoFilterHelper.js');
    // var cloudinary = require('../service/cloudinary.js');
    // var sparkPost = require('../service/sparkPost');
    // var dateHelper = require('../service/dateHelper');
    // var request = require('request-promise');
    // var config = require('../config/config');
    // var jade = require('jade');
    // var pdf = require('html-pdf');
    var fs = require('fs');
    var jobFeedsDao = require('./job_feedsDAO');
    // var solrHelper = require('../service/solrHelper');
    // var cvfilesDAO = require('../DAO/cvfilesDAO');

    function create(context, business)
    {
        function getAll(filter)
        {
            // return {'from': 'job_feeds_manager'};
            return jobFeedsDao.getAll(filter);
        }

        return {
            getAll: getAll
        };
    }

    module.exports = {
        create: create
    };
})();
