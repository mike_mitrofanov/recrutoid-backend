(function ()
{
    'use strict';

    var applicationException = require('../../service/applicationException');
    var container = require('./container');
    var config = require('../../config/config.js');
    var fs = require('fs');

    module.exports = function (router)
    {
        router.route('/api/job-feeds').get(function (request, response)
        {
            // console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
            // var result = container.getJobFeedsManager(request).getAll(request.query);
            // response.send(result);
            container.getJobFeedsManager(request).getAll(request.query).then(function (result)
            {
                response.send(result);
            })
            // .catch(function (error)
            // {
            //     applicationException.errorHandler(error, response);
            // });
        });
    };
})();
