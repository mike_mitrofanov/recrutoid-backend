(function ()
{
    'use strict';

    var jobFeedsManager = require('./job_feeds.manager');

    function getContext(request)
    {
        return {user: request && request.user};
    }

    function getter(manager)
    {
        return function (request)
        {
            return manager.create(getContext(request), module.exports);
        };
    }

    var managers = {
        getJobFeedsManager: getter(jobFeedsManager)
    };
    module.exports = managers;

    // managers.getIntervalManager({user: {role: security.ADMIN}}).startInterval();
})();
