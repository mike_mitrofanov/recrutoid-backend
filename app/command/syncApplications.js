(function ()
{
    'use strict';

    var solrHelper = require('../service/solrHelper');
    var solrSync = require('../service/solrHandlers/sync');
    

    function run() {
        console.log('Syncing applications...');
        solrSync.syncApplications().then(function (index) {
            solrHelper.commit().then(function () {
                console.log('Done');
                process.exit(0);
            });
        });

    }
    solrSync.limitHttp();
    solrSync.connectDB();
    run();

})();