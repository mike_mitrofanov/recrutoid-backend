(function ()
{
    'use strict';

    var solrHelper = require('../service/solrHelper');

    function run()
    {
        console.log('Purging solr...');

        solrHelper.purge().then(function () {
            solrHelper.commit().then(function () {
                console.log('Done');
                process.exit(0);
            });
        });
    }
    
    run();

})();



