(function ()
{
    'use strict';
    var mongoose = require('mongoose-bird')(require('mongoose'));
    var mongoConverter = require('./mongoConverter');
    var applicationException = require('../service/applicationException');

    var settingsSchema = new mongoose.Schema({
        userExpirationTime: {type: Number, required: true, default: 2592000000, select: false},
        paymentModeEnabled: {type: Boolean, required: true, default: false},
        backendUri: {type: String, required: true, default: ' '}
    }, {
        collection: 'settings'
    });
    var SettingsModel = mongoose.model('settings', settingsSchema);

    function get(projection)
    {
        projection = projection || {};

        return SettingsModel.findOneAsync(null, projection).then(function (data)
        {
            if (!data) {
                throw applicationException.new(applicationException.PRECONDITION_FAILED, 'Something went wrong');
            }
            return mongoConverter.fromMongo(data);
        });
    }

    function set(data)
    {
        return SettingsModel.findOneAndUpdateAsync(null, data);
    }

    module.exports = {
        get: get,
        set: set,

        model: SettingsModel
    };
})();
