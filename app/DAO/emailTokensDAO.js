(function ()
{
    'use strict';
    var applicationException = require('../service/applicationException');
    var mongoose = require('mongoose-bird')(require('mongoose'));
    var mongoConverter = require('../DAO/mongoConverter'),
            getDate = require('../service/dateHelper').get;

    var typeEnum = ['subscription', 'vacancy', 'user', 'emailChange'];

    var emailTokensSchema = new mongoose.Schema({
        originId: {type: mongoose.Schema.Types.ObjectId, required: true},
        secondOriginId: {type: mongoose.Schema.Types.ObjectId, required: false},
        date: {type: Number, required: true},
        type: {type: String, enum: typeEnum, required: true},
        additionalData: {type: String}
    }, {
        collection: 'email_tokens'
    });
    var EmailTokensModel = mongoose.model('email_tokens', emailTokensSchema);

    //var expirationTime = 86400; // 86400 seconds = 1 day

    function create(originId, type, secondOrigin, additionalData)
    {
        var newToken = {
            originId: originId,
            date: getDate(),
            type: type
        };
        if(secondOrigin) {
            newToken.secondOriginId = secondOrigin;
        }
        if(additionalData) {
            newToken.additionalData = additionalData;
        }
        return new EmailTokensModel(newToken).saveAsync().then(function (token)
        {
            return {token: mongoConverter.fromMongo(token[0]).id};
        });
    }

    function get(id)
    {
        return EmailTokensModel.findOneAsync({_id: id}).then(function (token)
        {
            if (!token) {
                throw applicationException.new(applicationException.NOT_FOUND);
            }
            return mongoConverter.fromMongo(token);
        });
    }

    function remove(id)
    {
        return EmailTokensModel.removeAsync({_id: id});
    }

    function getByOriginId(originId)
    {
        return EmailTokensModel.findOneAsync({originId: originId}).then(function (results)
        {
            if (!results) {
                throw applicationException.new(applicationException.NOT_FOUND);
            }
            return mongoConverter.fromMongo(results);
        });
    }

    function removeByOriginId(id)
    {
        return EmailTokensModel.removeAsync({originId: id});
    }

    module.exports = {
        getByOriginId: getByOriginId,
        removeByOriginId: removeByOriginId,
        create: create,
        get: get,
        remove: remove,
        model: EmailTokensModel
    };
})();
