(function ()
{
    'use strict';
    var mongoose = require('mongoose-bird')(require('mongoose'));
    var mongoConverter = require('./mongoConverter');
    var sha1 = require('sha1');
    var getDate = require('../service/dateHelper').get;
    var applicationException = require('../service/applicationException');
    var resetPassword = new mongoose.Schema({
        token: {type: String, required: true},
        userId: {required: true, type: String, unique: true},
        expire: {required: true, type: Number}
    }, {
        collection: 'reset_password_token'
    });


    var Model = mongoose.model('resetPassword', resetPassword);

    var offset = 24 * 3600000;//24h
    function create(userId)
    {
        return Model.findOneAsync({userId: userId}).then(function (results)
        {
            if (!results) {
                return new Model({userId: userId, expire: getDate() + offset, token: sha1('' + getDate())}).saveAsync().then(function (newToken)
                {
                    return mongoConverter.fromMongo(newToken[0]).token;
                });
            } else if (results && getDate() >= results.expire) {
                //when user try reset password after expire token i extend old token
                return Model.findByIdAndUpdateAsync(results.id, {$set: {expire: getDate() + offset}}).then(function (updateToken)
                {
                    return mongoConverter.fromMongo(updateToken).token;
                });
            } else {
                throw applicationException.new(applicationException.PRECONDITION_FAILED, 'Email was sent, please check e-mail or try later.');
            }
        });
    }

    function remove(token)
    {
        return Model.findOneAndRemoveAsync({token: token}).then(function (results)
        {
            if (!results) {
                throw applicationException.new(applicationException.CONFLICT);
            }
            return mongoConverter.fromMongo(results);
        });
    }

    function checkToken(token)
    {
        return Model.findOneAsync({token: token}).then(function (results)
        {
            if (!results) {
                throw applicationException.new(applicationException.UNAUTHORIZED, 'Token doesn\'t exist');
            }
            results = mongoConverter.fromMongo(results);
            if (getDate() + offset < results.expire) {
                return Model.removeAsync({token: token}).then(function ()
                {
                    throw applicationException.new(applicationException.UNAUTHORIZED, 'Token has been expired');
                });
            }
        });
    }

    module.exports = {
        checkToken: checkToken,
        remove: remove,
        create: create,
        offset:offset,
        model: Model
    };
})();
