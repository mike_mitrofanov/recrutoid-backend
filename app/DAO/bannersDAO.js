(function ()
{
    'use strict';
    var mongoose = require('mongoose-bird')(require('mongoose'));
    var mongoConverter = require('../DAO/mongoConverter');
    var locationEnum = [0, 1];
    var bannersSchema = new mongoose.Schema({
        location: {type: Number, enum: locationEnum, required: true},
        url: {type: String, required: true},
        description: {type: String, required: true},
        active: {type: Boolean, default: false, required: true}
    }, {
        collection: 'banners'
    });
    var BannersModel = mongoose.model('banners', bannersSchema);

    function query(filter)
    {
        var from = Math.max(filter.from, 0) || 0;
        var size = Math.max(filter.size, 0) || 20;
        delete filter.from;
        delete filter.size;
        var findQuery = BannersModel.find(filter).skip(from);
        if (null != size) {
            findQuery.limit(size);
        }
        return findQuery.execAsync().then(function (data)
        {
            return mongoConverter.fromMongo(data);
        }).then(function (results)
        {
            return BannersModel.countAsync(filter).then(function (count)
            {
                return {results: results, total: count};
            });
        });
    }

    function createNewOrUpdate(banner)
    {
        if (!banner.id) {
            return new BannersModel(banner).saveAsync().then(function (result)
            {
                return mongoConverter.fromMongo(result[0]);
            });
        } else {
            return BannersModel.findByIdAndUpdateAsync(banner.id, banner, {new: true}).then(function (result)
            {
                return mongoConverter.fromMongo(result);
            });
        }
    }

    function remove(id)
    {
        return BannersModel.removeAsync({_id: id});
    }

    function get(query)
    {
        return BannersModel.findOneAsync(query);
    }

    function updateAll(query, banner)
    {
        return BannersModel.updateAsync(query, banner, {multi: true});
    }

    module.exports = {
        createNewOrUpdate: createNewOrUpdate,
        remove: remove,
        query: query,
        get: get,
        updateAll: updateAll,

        model: BannersModel
    };
})();
