(function ()
{
    'use strict';
    var mongoose = require('mongoose-bird')(require('mongoose'));
    var mongoConverter = require('./mongoConverter');
    var sha1 = require('sha1');
    var getDate = require('../service/dateHelper').get;
    var applicationException = require('../service/applicationException');
    var activateAccount = new mongoose.Schema({
        token: {type: String, required: true},
        userId: {required: true, type: String, unique: true},
        expire: {required: true, type: Number}
    }, {
        collection: 'activate_account_token'
    });


    var Model = mongoose.model('activate_account_token', activateAccount);

    var offset = 24 * 3600000;//24h
    function create(userId)
    {
        return Model.findOneAsync({userId: userId}).then(function (results)
        {
            if (!results) {
                return new Model({userId: userId, expire: getDate() + offset, token: sha1('' + getDate())}).saveAsync().then(function (newToken)
                {
                    return mongoConverter.fromMongo(newToken[0]).token;
                });
            } else if (results && getDate() >= results.expire) {
                //when user try reset password after expire token i extend old token
                return Model.findByIdAndUpdateAsync(results.id, {$set: {expire: getDate() + offset}}).then(function (updateToken)
                {
                    return mongoConverter.fromMongo(updateToken).token;
                });
            } else {
                throw applicationException.new(applicationException.PRECONDITION_FAILED, 'Email was sent, please check e-mail or try later.');
            }
        });
    }

    function remove(token)
    {
        return Model.findOneAndRemoveAsync({token: token}).then(function (results)
        {
            if (!results) {
                throw applicationException.new(applicationException.NOT_FOUND);
            }
            return mongoConverter.fromMongo(results);
        });
    }

    function checkToken(token)
    {
        return Model.findOneAsync({token: token}).then(function (results)
        {
            if (!results) {
                throw applicationException.new(applicationException.UNAUTHORIZED, 'Token doesn\'t exist');
            }
            results = mongoConverter.fromMongo(results);
            if (results.expire < getDate()) {
                return Model.removeAsync({token: token}).then(function ()
                {

                    throw applicationException.new(applicationException.UNAUTHORIZED, 'Token has been expired');
                });
            }
        });
    }

    function get(query)
    {
        return Model.findOneAsync(query).then(function (token)
        {
            if (!token) {
                throw applicationException.new(applicationException.NOT_FOUND, 'Account is activated');
            }
            return mongoConverter.fromMongo(token);
        });
    }

    module.exports = {
        get: get,
        checkToken: checkToken,
        remove: remove,
        create: create,
        model: Model,
        offset: offset
    };
})();
