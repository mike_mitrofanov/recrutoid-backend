(function ()
{
    'use strict';
    var mongoConverter = require('./mongoConverter');
    var applicationException = require('../service/applicationException');
    var mongoose = require('mongoose-bird')(require('mongoose'));
    var blacklistedDomains = new mongoose.Schema({
        name: {type: String, required: true, unique: true}
    }, {
        collection: 'blacklisted_domains'
    });
    var Model = mongoose.model('blacklisted_domains', blacklistedDomains);


    function createNewOrUpdate(entity)
    {
        if (!entity.id) {
            return new Model(entity).saveAsync().then(function (result)
            {
                return mongoConverter.fromMongo(result[0]);
            }).catch(function (error)
            {
                if (11000 === error.code) {
                    throw applicationException.new(applicationException.CONFLICT, 'This blacklisted domain already exists in database');
                } else {
                    throw error;
                }
            });
        } else {
            return Model.findByIdAndUpdateAsync(entity.id, entity).then(function ()
            {
                return true;
            });
        }
    }

    function query(filter)
    {
        var from = Math.max(filter.from, 0) || 0;
        var size = Math.max(filter.size, 0) || 20;
        delete filter.from;
        delete filter.size;
        var findQuery = Model.find(filter).skip(from);
        if (null != size) {
            findQuery.limit(size);
        }
        return findQuery.execAsync().then(function (data)
        {
            return mongoConverter.fromMongo(data);
        }).then(function (results)
                {
                    return Model.countAsync(filter).then(function (count)
                    {
                        return {results: results, total: count};
                    });
                });
    }

    function get(query)
    {
        return Model.findOneAsync(query);
    }

    function remove(languageId)
    {
        return Model.findByIdAndRemoveAsync(languageId);
    }

    module.exports = {
        get: get,
        query: query,
        remove: remove,
        createNewOrUpdate: createNewOrUpdate,
        model: Model
    };
})();
