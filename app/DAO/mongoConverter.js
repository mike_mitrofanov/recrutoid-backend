(function ()
{
    'use strict';

    var mongoose = require('mongoose');

    // doNotConvertId is to convert only top level _id and do not touch deeper ids to prevent logic broke.
    function fromMongo(data, doNotConvertId, enableIdConvertingOnLowerLevels)
    {
        if (null == data) {
            return null;
        }
        if ('object' !== typeof data) {
            return data;
        }
        if (data instanceof Array) {
            return data.map(function (ele)
            {
                return fromMongo(ele, doNotConvertId, enableIdConvertingOnLowerLevels);
            });
        } else {
            if(data.hasOwnProperty('_doc')) {
                data = data._doc;
            }
            if(!doNotConvertId) {
                if (data._id) {
                    data.id = data._id;
                }
                delete data._id;
            }
            delete data.__v;

            for (var prop in data) {
                if (data.hasOwnProperty(prop) && !(data[prop] instanceof mongoose.Types.ObjectId)) {
                    data[prop] = fromMongo(data[prop], enableIdConvertingOnLowerLevels ? doNotConvertId : true, enableIdConvertingOnLowerLevels);
                }
            }

            return data;
        }
    }

    module.exports = {
        fromMongo: fromMongo
    };
})();
