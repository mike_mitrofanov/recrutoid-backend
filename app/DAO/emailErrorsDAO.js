(function ()
{
    'use strict';
    var mongoose = require('mongoose-bird')(require('mongoose'));
    var mongoConverter = require('../DAO/mongoConverter');
    var dateHelper = require('../service/dateHelper');
    var getDate = dateHelper.get;
    var _ = require('lodash');
    var emailErrorsSchema = new mongoose.Schema({
        name: {type: String},
        message: {type: String},
        description: {type: String},
        code: {type: String},
        stack: {type: String},
        createDate: {type: Number, default: getDate}
    }, {
        collection: 'email_errors'
    });
    var emailErrorsModel = mongoose.model('email_errors', emailErrorsSchema);

    function query(filter, projection)
    {
        var from = Math.max(filter.from, 0) || 0;
        var size = Math.max(filter.size, 0) || 20;
        if (0 > filter.size) {
            size = null;
        }
        if (!_.isEmpty(filter.email)) {
            if (!filter.hasOwnProperty('$or')) {
                filter.$or = [];
            }
            filter.$or.push({email: {$regex: filter.email, $options: 'i'}});
        }

        projection = projection || {};
        delete filter.from;
        delete filter.size;
        delete filter.email;

        var sort = {createDate: -1};

        var findQuery = emailErrorsModel.find(filter, projection).skip(from).sort(sort);
        if (null != size) {
            findQuery.limit(size);
        }
        return findQuery.execAsync().then(function (data)
        {
            return mongoConverter.fromMongo(data);
        }).then(function (results)
        {
            return emailErrorsModel.countAsync(filter).then(function (count)
            {
                return {results: results, total: count};
            });
        });
    }

    function createNew(emailError)
    {
        return new emailErrorsModel(emailError).saveAsync().then(function (result)
        {
            return mongoConverter.fromMongo(result[0]);
        });
    }

    function remove(id)
    {
        return emailErrorsModel.removeAsync({_id: id});
    }

    function get(query)
    {
        return emailErrorsModel.findOneAsync(query);
    }

    module.exports = {
        createNew: createNew,
        remove: remove,
        query: query,
        get: get,

        model: emailErrorsModel
    };
})();
