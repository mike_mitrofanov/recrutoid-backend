(function ()
{
    'use strict';
    var mongoose = require('mongoose-bird')(require('mongoose'));
    var mongoConverter = require('./mongoConverter');
    var passwordssSchema = new mongoose.Schema({
        _id: {type: mongoose.Schema.Types.ObjectId, ref: 'users', required: true, unique: true},
        password: {type: String, required: true}
    }, {
        collection: 'users_passwords'
    });
    var PasswordsModel = mongoose.model('users_passwords', passwordssSchema);

    function checkUserPassword(userId, passHash)
    {
        return PasswordsModel.findOneAsync({_id: userId, password: passHash});
    }

    function createOrUpdate(userId, password)
    {
        return PasswordsModel.findOneAsync({_id: userId}).then(function (pass)
        {
            if (!pass) {
                return new PasswordsModel({_id: userId, password: password}).saveAsync().then(function (passFromDB)
                {
                    return mongoConverter.fromMongo(passFromDB[0]);
                });
            } else {
                return PasswordsModel.findOneAndUpdateAsync({_id: userId}, {password: password}, {'new': true});
            }
        });
    }

    function get(userId)
    {
        return PasswordsModel.findOneAsync({_id: userId}).then(function (pass)
        {
            return mongoConverter.fromMongo(pass);
        });
    }
    
    function remove(userId)
    {
        return PasswordsModel.removeAsync({_id: userId});
    }

    module.exports = {
        checkUserPassword: checkUserPassword,
        createOrUpdate: createOrUpdate,
        get: get,
        remove: remove,

        model: PasswordsModel
    };
})();
