(function ()
{
    'use strict';
    var applicationException = require('./../service/applicationException');
    var mongoose = require('mongoose-bird')(require('mongoose'));
    var mongoConverter = require('../DAO/mongoConverter');
    var mongodb = require('mongodb');
    var dateHelper = require('../service/dateHelper');
    var getDate = dateHelper.get;
    var intervalEnum = ['daily', 'weekly'];
    var subscriptionsSchema = new mongoose.Schema({
        email: {type: String, required: true},
        categories: [
            {type: mongoose.Schema.Types.ObjectId, ref: 'categories'}
        ],
        city: {type: mongoose.Schema.Types.ObjectId, ref: 'cities'},
        interval: {type: String, enum: intervalEnum, required: true},
        verified: {type: Boolean, required: true},
        subscribed: {type: Boolean},
        banned: {type: Boolean, required: true, default: false},
        token: {type: String},
        desiredSalaryRange: {type: mongoose.Schema.Types.ObjectId, ref: 'salary_ranges'},
        createDate: {type: Number, required: true, default: getDate}
    }, {
        collection: 'subscriptions'
    });
    var SubModel = mongoose.model('subscriptions', subscriptionsSchema);


    function query(filter)
    {
        var from = Math.max(filter.from, 0) || 0;
        var size = Math.max(filter.size, 0);
        if (0 > filter.size || filter.size == 'all') {
            size = null;
        }

        delete filter.size;
        delete filter.from;
        if (filter.email) {
            filter.email = {$regex: filter.email, $options: 'i'};
        } else {
            delete filter.email;
        }
        var options = {sort: '-_id', skip: from};
        if (0 < size) {
            options.limit = size;
        }
        var populateCity = {
            path: 'city',
            model: 'cities'
        };
        var populateCategories = {
            path: 'categories',
            model: 'categories'
        };
        var populateDesiredSalaryRange = {
            path: 'desiredSalaryRange',
            model: 'salary_ranges'
        };

        if (filter.categories == 'all') {
            filter.categories = {$in: [null]};
        }

        if (filter.city == 'all') {
            filter.city = null;
        }

        if (filter.desiredSalaryRange == 'all') {
            filter.desiredSalaryRange = null;
        }
        return SubModel.find(filter, {token: 0}, options).populate(populateDesiredSalaryRange).populate(populateCategories).populate(populateCity).execAsync().then(function (subscriptions)
        {
            subscriptions = subscriptions.map(function (subscription)
            {
                subscription._doc.categories = subscription.categories.map(function (category)
                {
                    if (category) {
                        return {id: category._id, name: category.name};
                    }
                });
                return subscription;
            });
            return SubModel.countAsync(filter).then(function (count)
            {
                return {results: mongoConverter.fromMongo(subscriptions), total: count};
            });
        });
    }

    function getAll(filter)
    {
        return SubModel.findAsync(filter).then(function (results)
        {
            return mongoConverter.fromMongo(results);
        });
    }

    function createNewOrUpdate(subscription, returnOld)
    {
        if (!subscription.id) {
            return new SubModel(subscription).saveAsync().then(function (result)
            {
                if (subscription.verified) {
                    return remove({_id: {$ne: result[0].id}, email: result[0].email}).then(function ()
                    {
                        return mongoConverter.fromMongo(result[0]);
                    });
                } else {
                    return mongoConverter.fromMongo(result[0]);
                }
            }).catch(function (error)
            {
                throw error;
            });
        } else {
            if (!subscription.city) {
                subscription.city = null;
            }
            var options = returnOld ? {} : {new: true};
            return SubModel.findByIdAndUpdateAsync(subscription.id, subscription, options).then(function (result)
            {
                return mongoConverter.fromMongo(result);
            });
        }
    }

    function get(queryOrId, projection)
    {
        projection = projection || {};
        var filter = mongodb.ObjectID.isValid(queryOrId) ? {_id: queryOrId} : queryOrId;
        var populateCity = {
            path: 'city',
            model: 'cities'
        };
        var populateCategories = {
            path: 'categories',
            model: 'categories'
        };
        var populateDesiredSalaryRange = {
            path: 'desiredSalaryRange',
            model: 'salary_ranges'
        };
        return SubModel.findOne(filter, projection).populate(populateCategories).populate(populateCity).populate(populateDesiredSalaryRange).execAsync().then(function (result)
        {
            if (!result) {
                throw  applicationException.new(applicationException.NOT_FOUND);
            }
            return mongoConverter.fromMongo(result, false, true);
        });
    }

    function queryToSendEmail(query)
    {
//        TODO this should check for unverified subscriptions
        return SubModel.find({interval: {$in: query}}, {token: 0}).execAsync().then(function (results)
        {
            return mongoConverter.fromMongo(results);
        });
    }

    function update(query, body, returnOld)
    {
        var populateCity = {
            path: 'city',
            model: 'cities',
        };
        var populateCategories = {
            path: 'categories',
            model: 'categories',
        };
        var populateDesiredSalaryRange = {
            path: 'desiredSalaryRange',
            model: 'salary_ranges'
        };
        var options = {
            new: !returnOld
        };
        return SubModel.findOneAndUpdate(query, body, options)
                .populate(populateCategories).populate(populateCity).populate(populateDesiredSalaryRange).execAsync().then(function (result)
                {
                    if (result) {
                        if (query._id) {
                            SubModel.remove({_id: {$ne: result._id}, email: result.email}).exec();
                        } else if (result.token) {
                            SubModel.remove({token: {$ne: result.token}, email: result.email}).exec();
                        }
                    }
                    return mongoConverter.fromMongo(result, false, true);
                });
    }

    function remove(query)
    {
        return SubModel.removeAsync(query);
    }

    function search(filter, project)
    {
        return SubModel.find(filter, project);
    }

    module.exports = {
        search: search,
        get: get,
        query: query,
        getAll: getAll,
        remove: remove,
        update: update,
        queryToSendEmail: queryToSendEmail,
        createNewOrUpdate: createNewOrUpdate,

        model: SubModel
    };
})();
