(function ()
{
    'use strict';
    var applicationException = require('../service/applicationException');
    var mongoose = require('mongoose-bird')(require('mongoose'));
    var mongoConverter = require('../DAO/mongoConverter'),
            getDate = require('../service/dateHelper').get;

    var tokensSchema = new mongoose.Schema({
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'users', required: true},
        date: {type: Number, required: true}
    }, {
        collection: 'tokens'
    });
    var TokensModel = mongoose.model('tokens', tokensSchema);

    //var expirationTime = 86400; // 86400 seconds = 1 day

    function create(userId)
    {
        var newToken = {
            userId: userId,
            date: getDate()
        };
        return new TokensModel(newToken).saveAsync().then(function (token)
        {
            return {token: mongoConverter.fromMongo(token[0]).id};
        });
    }

    function getByToken(token)
    {
        return TokensModel.findOneAsync({_id: token}).then(function (token)
        {
            if (!token) {
                throw applicationException.new(applicationException.NOT_FOUND);
            }
            return mongoConverter.fromMongo(token);
        });
    }
    
    function removeAllByUserId(userId)
    {
        return TokensModel.removeAsync({userId: userId});
    }

    module.exports = {
        create: create,
        getByToken: getByToken,
        removeAllByUserId: removeAllByUserId,

        model: TokensModel
    };
})();
