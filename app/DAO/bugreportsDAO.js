(function ()
{
    'use strict';
    var mongoose = require('mongoose-bird')(require('mongoose'));
    var mongoConverter = require('../DAO/mongoConverter');
    var dateHelper = require('../service/dateHelper');
    var getDate = dateHelper.get;
    var _ = require('lodash');
    var bugreportsSchema = new mongoose.Schema({
        email: {type: String, required: true},
        message: {type: String, required: true},
        createDate: {type: Number, default: getDate}
    }, {
        collection: 'bugreports'
    });
    var BugreportsModel = mongoose.model('bugreports', bugreportsSchema);

    function query(filter, projection)
    {
        var from = Math.max(filter.from, 0) || 0;
        var size = Math.max(filter.size, 0) || 20;
        if (0 > filter.size) {
            size = null;
        }
        if (!_.isEmpty(filter.email)) {
            if (!filter.hasOwnProperty('$or')) {
                filter.$or = [];
            }
            filter.$or.push({email: {$regex: filter.email, $options: 'i'}});
        }

        projection = projection || {};
        delete filter.from;
        delete filter.size;
        delete filter.email;

        var sort = {createDate: -1};

        var findQuery = BugreportsModel.find(filter, projection).skip(from).sort(sort);
        if (null != size) {
            findQuery.limit(size);
        }
        return findQuery.execAsync().then(function (data)
        {
            return mongoConverter.fromMongo(data);
        }).then(function (results)
        {
            return BugreportsModel.countAsync(filter).then(function (count)
            {
                return {results: results, total: count};
            });
        });
    }

    function createNew(bugreport)
    {
        return new BugreportsModel(bugreport).saveAsync().then(function (result)
        {
            return mongoConverter.fromMongo(result[0]);
        });
    }

    function remove(id)
    {
        return BugreportsModel.removeAsync({_id: id});
    }

    function get(query)
    {
        return BugreportsModel.findOneAsync(query);
    }

    module.exports = {
        createNew: createNew,
        remove: remove,
        query: query,
        get: get,

        model: BugreportsModel
    };
})();
