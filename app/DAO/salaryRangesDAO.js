(function ()
{
    'use strict';
    var mongoose = require('mongoose-bird')(require('mongoose'));
    var mongoConverter = require('../DAO/mongoConverter');
    var salaryRangesSchema = new mongoose.Schema({
        order: {type: Number, required: true, default: 0},
        name: {
            type:Object,
            en: {
                type: String,
                required: true
            },
            ru: {
                type: String,
                required: true
            }
        },
        ranges: [
            {type: mongoose.Schema.Types.ObjectId, ref: 'salary_ranges', required: false}
        ]
    }, {
        collection: 'salary_ranges'
    });
    var SalaryRangesModel = mongoose.model('salary_ranges', salaryRangesSchema);

    function query()
    {
        return SalaryRangesModel.findAsync(null, null, {sort:'order'}).then(function (data)
        {
            return mongoConverter.fromMongo(data);

        });
    }

    function createNewOrUpdate(salaryRange)
    {
        if (!salaryRange.id) {
            return new SalaryRangesModel(salaryRange).saveAsync().then(function (result)
            {
                return mongoConverter.fromMongo(result[0]);
            });
        } else {
            return SalaryRangesModel.findByIdAndUpdateAsync(salaryRange.id, salaryRange, {new: true}).then(function (result)
            {
                return mongoConverter.fromMongo(result);
            });
        }
    }

    function remove(id)
    {
        return SalaryRangesModel.removeAsync({_id: id});
    }

    function get(query)
    {
        return SalaryRangesModel.findOneAsync(query);
    }

    function populateSalaryRanges(salaryRanges, path)
    {
        return SalaryRangesModel.populateAsync(salaryRanges, {path: path});
    }

    module.exports = {
        createNewOrUpdate: createNewOrUpdate,
        remove: remove,
        query: query,
        get: get,
        populateSalaryRanges: populateSalaryRanges,

        model: SalaryRangesModel
    };
})();
