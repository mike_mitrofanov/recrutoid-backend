(function ()
{
    'use strict';

    var _ = require('lodash');
    var mongodb = require('mongodb');
    var applicationException = require('../service/applicationException');
    var mongoose = require('mongoose-bird')(require('mongoose'));
    var mongoConverter = require('../DAO/mongoConverter');
    var Promise = require('bluebird');
    var getDate = require('../service/dateHelper').get;
    var offset = 2592000000; //30 days
    var deepPopulate = require('mongoose-deep-populate')(mongoose);
    var dateHelper = require('../service/dateHelper');
    var UsersPermissionsDAO = require('../DAO/usersPermissionsDAO');
    var SubscriptionsDAO = require('../DAO/subscriptionsDAO');
    var vacanciesDAO = require('../DAO/vacanciesDAO');

    var userTypesEnum = [
        'normal', 'employer', 'admin'
    ];

    var applicationsNotificationsIntervalEnum = [
        'daily', 'weekly', 'never'
    ];

    var currencyEnum = ['KZT', 'USD', 'EUR'];
    var languageLevelEnum = ['0', '1', '2'];
    var degreeEnum = ['Bachelors', 'Masters', 'PhD'];

    var workExperienceSchema = new mongoose.Schema({
        title: {type: String},
        company: {type: String},
        startDate: {type: Number},
        endDate: {type: Number},
        city: {type: String},
        description: {type: String},
        currentWork: {type: Boolean, default: false}
    });

    var educationSchema = new mongoose.Schema({
        fieldOfStudy: {type: String},
        school: {type: String},
        startDate: {type: Number},
        endDate: {type: Number},
        city: {type: String},
        degree: {type: String, enum: degreeEnum}
    });

    var languagesSchema = new mongoose.Schema({
        name: {type: String},
        code: {type: String},
        level: {type: String, enum: languageLevelEnum}
    });

    var usersSchema = new mongoose.Schema({
        email: {type: String, required: true, unique: true},
        newEmail: {type: String, required: false, unique: false},
        verified: {type: Boolean, default: false},
        banned: {type: Boolean, default: false},
        hidden: {type: Boolean, default: false},
        hiddenOnSearch: {type: Boolean, default: false},
        firstName: {type: String},
        lastName: {type: String},
        phone: {type: String},
        thumbnail: {type: String},
        resume: {
            //type: Object,
            title: {type: String},
            foreignDegree: {type: Boolean, default: false},
            expat: {type: Boolean, default: false},
            updateDate: {type: Number},
            workExperience: [workExperienceSchema],
            education: [educationSchema],
            additionalInfo: {type: String},
            languages: [languagesSchema],
            desiredSalaryRange: {type: mongoose.Schema.Types.ObjectId, ref: 'salary_ranges'}
        },
        uploadedResume: {
            messageSent: {type: Boolean, required: false},
            data: {type: mongoose.Schema.Types.ObjectId, ref: 'cvfiles'}
        },
        cancelCvExpireTime: {type: Number, required: false},
        city: {type: mongoose.Schema.Types.ObjectId, ref: 'cities'},
        categories: [
            {type: mongoose.Schema.Types.ObjectId, ref: 'categories'}
        ],
        role: {type: String, enum: userTypesEnum, default: 'normal', required: true},
        offers: [{
                addDate: {type: Number, required: true, default: getDate},
                vacancyId: {type: mongoose.Schema.Types.ObjectId, ref: 'vacancies'},
                seen: {type: Boolean, default: false, required: true}
            }],
        extendDate: {type: Number, default: getDate},
        remindSent: {type: Boolean, default: false, select: false},
        socials: {
            type: Object,
            select: false,
            facebook: {type: String},
            linkedin: {type: String},
            socialOnly: {type: Boolean, default: false}
        },
        searchVisited: {type: Boolean, default: false},
        workExperience: {type: Number},
        createDate: {type: Number, default: getDate},
        showOnHomePage: {type: Boolean, default: false},
        applicationsNotificationsInterval: {type: String, enum: applicationsNotificationsIntervalEnum},
        resumeViews: [
            {
                user: {type: mongoose.Schema.Types.ObjectId, ref: 'users'},
                date: {type: Number},
                seen: {type: Boolean, default: false, required: true}
            }
        ],
        firstLogin: {type: Boolean, default: true}
    }, {
        collection: 'users'
    });
    usersSchema.index({firstName: 'text', lastName: 'text'}, {'default_language': 'english'});
    usersSchema.plugin(deepPopulate);
    var UsersModel = mongoose.model('users', usersSchema);
    var deepPopulateUserModelAsync = Promise.promisify(UsersModel.deepPopulate, UsersModel);
    var seenUsersSchema = new mongoose.Schema({
        email: {type: String, required: true},
        user: {type: mongoose.Schema.Types.ObjectId, ref: 'users', required: true}
    }, {
        collection: 'seen_users'
    });
    var SeenUsersModel = mongoose.model('seen_users', seenUsersSchema);

    var VisitedUsersSchema = new mongoose.Schema({
        email: {type: String, required: true},
        visited_user: {type: mongoose.Schema.Types.ObjectId, ref: 'users', required: true},
        user: {type: mongoose.Schema.Types.ObjectId, ref: 'users', required: true},
    }, {
        collection: 'visited_users'
    });
    var VisitedUsersModel = mongoose.model('visited_users', VisitedUsersSchema);

    var extendProfileTokensSchema = new mongoose.Schema({
        userId: {type: mongoose.Schema.Types.ObjectId, ref: 'users', required: true}
    }, {
        collection: 'extend_profile_tokens'
    });
    var ExtendProfileTokensModel = mongoose.model('extend_profile_tokens', extendProfileTokensSchema);

    function createNewOrUpdate(data, social)
    {
        if (!data.id) {
            if (!social) {
                data.verified = false;
            }
            return new UsersModel(data).saveAsync().then(function (results)
            {
                return mongoConverter.fromMongo(results[0]);
            });
        } else {
            return UsersModel.findByIdAndUpdateAsync(data.id, data, {new : true});
        }
    }

    function getIdByEmailOrCreateNew(email)
    {
        return UsersModel.findOneAsync({email: email}).then(function (data)
        {
            if (!data) {
                return new UsersModel({email: email, registered: false}).saveAsync().then(function (user)
                {
                    return mongoConverter.fromMongo(user[0]).id;
                });
            } else {
                return mongoConverter.fromMongo(data).id;
            }
        });
    }

    function markSeen(email, user)
    {
        var entry = {email: email, user: user};
        return SeenUsersModel.findOneAsync({email: email, user: user}).then(function (result)
        {
            if (result) {
                return Promise.resolve();
            }
            return SeenUsersModel.createAsync(entry);
        });
    }

    function markManySeen(email, ids)
    {
        return _.map(ids, function (id)
        {
            return SeenUsersModel.createAsync({
                email: email,
                user: id
            });
        });
    }

    function markAllAsNotNew(userId)
    {
        UsersModel.findById(userId).then(function (usr) {
            console.log(usr.email);

            UsersModel.find({role: 'normal'}).execAsync().then(function (all) {
                for (var i in all) {
                    var id = all[i]._id;
                    if (id == userId) {
                        continue;
                    }
                    SeenUsersModel.createAsync({email: usr.email, user: id});
                }

                console.log('Users done', i);

            });


        });

    }

    function removeFromSeenByUserId(userId)
    {
        return SeenUsersModel.removeAsync({user: userId});
    }

    function get(query, projection, shouldPopulateCategories)
    {
        projection = projection || {};

        var populateCity = {
            path: 'city',
            model: 'cities'
        };

        var populateCategories = {
            path: 'categories',
            model: 'categories'
        };

        var populateDesiredSalaryRange = {
            path: 'resume.desiredSalaryRange',
            model: 'salary_ranges'
        };

        var promise = UsersModel.findOne(query, projection).populate(populateDesiredSalaryRange).populate(populateCity);
        if (shouldPopulateCategories) {
            promise.populate(populateCategories);
        }
        return promise.execAsync().then(function (user)
        {
            if (null == user) {
                return user;
            }

            if (user.city) {
                user._doc.city = {name: user.city.name, id: user.city.id};
            }

            if (shouldPopulateCategories) {
                user._doc.categories = _.map(user.categories, function (elem)
                {
                    return {name: elem.name, id: elem.id};
                });
            }


            return mongoConverter.fromMongo(user);
        });
    }

    function queryCompanyName(users, VacancyDAO)
    {
        var arrayEmails = _.map(users, function (user)
        {
            return user.email;
        });
        return new Promise(function (resolve, reject)
        {
            VacancyDAO.model.findAsync({email: {$in: arrayEmails}}, {companyName: 1, email: 1}).then(function (results)
            {
                _.forEach(users, function (user)
                {
                    for (var i = 0; i < results.length; i++) {
                        if (user.email === results[i].email) {
                            user.companyName = results[i].companyName;
                            user.shortDescription = results[i].shortDescription;
                            results.splice(i, 1);
                            break;
                        }
                    }
                });
                resolve(users);
            }).catch(reject);
        });
    }



    function activateAccount(userId)
    {

        return UsersModel.findByIdAndUpdateAsync(userId, {$set: {verified: true}}, {'new': true});
    }

    function isNew(email, userIds)
    {
        var mongooseUserIds = userIds.filter(function (item)
        {
            return mongodb.ObjectID.isValid(item && item.toString && item.toString());
        });
        return SeenUsersModel.find({email: email, user: {$in: mongooseUserIds}}).execAsync().then(function (data)
        {
            var map = {};
            _.forEach(data, function (item)
            {
                map[item.user] = true;
            });
            var result = {};
            _.forEach(userIds, function (vacancy)
            {
                result[vacancy] = !map[vacancy];
            });
            return result;
        });
    }

    function query(filter, VacancyDAO)
    {
        var from = Math.max(filter.from, 0) || 0;
        var size = Math.max(filter.size, 0);

        filter.role = filter.role ? filter.role : 'normal';

        if (-1 !== filter.size) {
            size = Math.max(filter.size, 0) || 20;
        }
        if (0 > filter.size || filter.size == 'all') {
            size = null;
        }

        var additionalFilter = {};

        if (null != filter.dbAccess) {
            additionalFilter.dbAccess = filter.dbAccess;
            delete filter.dbAccess;
        }

        if (null != filter.addVacancies) {
            additionalFilter.addVacancies = filter.addVacancies;
            delete filter.addVacancies;
        }

        if (null != filter.isSubscriber) {
            additionalFilter.isSubscriber = filter.isSubscriber;
            delete filter.isSubscriber;
        }

        if (null != filter.cvActive) {
            additionalFilter.cvActive = filter.cvActive;
            delete filter.cvActive;
        }

        if (null != filter.cvUploaded) {

            if (filter.cvUploaded) {
                filter.uploadedResume = {$exists: true, $not: {$size: 0}, $ne: null};
            } else {
                filter.uploadedResume = null;
                additionalFilter.cvActive = true;
            }

            delete filter.cvUploaded;
        }

        if (!_.isEmpty(filter.email)) {
            if (!filter.hasOwnProperty('$or')) {
                filter.$or = [];
            }
            filter.$or.push({email: {$regex: filter.email, $options: 'i'}});
        }

        if (!_.isEmpty(filter.searchQuery)) {
            if (-1 === filter.searchQuery.indexOf('@')) {
                filter.$text = {
                    $search: filter.searchQuery
                };
            } else {
                filter.email = {$regex: filter.searchQuery, $options: 'i'};
            }
        }
        if (!_.isEmpty(filter.title)) {
            if (!filter.hasOwnProperty('$or')) {
                filter.$or = [];
            }
            filter.$or.push({firstName: {$regex: filter.title, $options: 'i'}});
            filter.$or.push({lastName: {$regex: filter.title, $options: 'i'}});
        }
        if (null != filter.workgte && null != filter.worklte) {
            filter.workExperience = {$gte: filter.workgte, $lte: filter.worklte};
        }

        if (null != filter.workgte && !filter.worklte) {
            filter.workExperience = {$gte: filter.workgte};
        }

        if (null != filter.language) {
            filter['resume.languages.name'] = filter.language;
        }

        if (null != filter.days) {
            filter.createDate = {$gte: getDate() - 259200000};
            // last three days
        }

        if (filter.categories) {
            if (!(filter.categories instanceof Array)) {
                filter.categories = [filter.categories];
            }
            filter.categories = {$in: filter.categories};
        }

        if (filter.education) {
            if (!(filter.education instanceof Array)) {
                filter.education = [filter.education];
            }
            filter['resume.education.degree'] = {$in: filter.education};
        }

        if (!_.isEmpty(filter.appNotifications)) {
            filter.applicationsNotificationsInterval = {$regex: filter.appNotifications, $options: 'i'};
        }

        if (!_.isEmpty(filter.nameOrCvTitle)) {
            if (!filter.hasOwnProperty('$or')) {
                filter.$or = [];
            }
            filter.$or.push({'resume.title': {$regex: filter.nameOrCvTitle, $options: 'i'}});
            filter.$or.push({firstName: {$regex: filter.nameOrCvTitle, $options: 'i'}});
            filter.$or.push({lastName: {$regex: filter.nameOrCvTitle, $options: 'i'}});
        }

        delete filter.from;
        delete filter.size;
        delete filter.query;
        delete filter.worklte;
        delete filter.workgte;
        delete filter.language;
        delete filter.days;
        delete filter.searchQuery;
        delete filter.email;
        delete filter.title;
        delete filter.nameOrCvTitle;
        delete filter.appNotifications;
        delete filter.education;


        var findQuery = UsersModel.find(filter, null, {sort: {_id: -1}}).skip(from);
        if (void 0 !== size) {
            findQuery = findQuery.limit(size);
        }

        var populateCity = {
            path: 'city',
            model: 'cities'
        };
        var populateCategories = {
            path: 'categories',
            model: 'categories'
        };

        var populateDesiredSalaryRange = {
            path: 'resume.desiredSalaryRange',
            model: 'salary_ranges'
        };

        var populateCvFiles = {
            path: 'uploadedResume.data',
            model: 'cvfiles'
        };

        return findQuery.populate(populateDesiredSalaryRange).populate(populateCategories).populate(populateCity).populate(populateCvFiles).execAsync().then(function (data)
        {
            return mongoConverter.fromMongo(data);
        }).then(function (results)
        {
            var finalResults;
            return getAdditionalUserData(results).then(function (results) {
                var data;
                var promises;
                if (VacancyDAO) {
                    promises = queryCompanyName(results, VacancyDAO);
                } else {
                    promises = Promise.resolve();
                }
                return promises.then(function ()
                {
                    data = _.filter(results, function (user)
                    {
                        var dbAccess = null != additionalFilter.dbAccess ? user.permissions.cvSearching == additionalFilter.dbAccess : true;
                        var addVacancies = null != additionalFilter.addVacancies ? user.permissions.vacancyAdding == additionalFilter.addVacancies : true;

                        var isSubscriber = null != additionalFilter.isSubscriber ? user.isSubscriber == additionalFilter.isSubscriber : true;
                        var cvActive = null != additionalFilter.cvActive ? !!user.resume.active == additionalFilter.cvActive : true;

                        return dbAccess && addVacancies && isSubscriber && cvActive;
                    });

                    return UsersModel.countAsync(filter);
                }).then(function (count) {
                    finalResults = {results: data, total: count};
                    return UsersModel.countAsync({showOnHomePage: true, role: filter.role});
                }).then(function (showOnHpCount) {
                    finalResults.hp_count = showOnHpCount;
                    return finalResults;
                });
            });
        });
    }

    function getAdditionalUserData(results) {
        var promises;
        promises = _.map(results, function (user)
        {
            if ('employer' === user.role) {
                return UsersPermissionsDAO.get(user.id).then(function (permissions)
                {
                    user.permissions = permissions;
                    return user;
                });
            }

            if ('normal' === user.role) {
                return SubscriptionsDAO.model.find({email: user.email}).then(function (data)
                {
                    user.isSubscriber = data.length > 0;
                    user.resume.active = user.resume.title && user.resume.title.length && user.categories.length && user.city &&
                            user.firstName && user.firstName.length &&
                            user.lastName && user.lastName.length && user.resume.education.length;
                    return user;
                });
            }
            return user;
        });
        return Promise.all(promises)
    }

    function find(filter)
    {
        var languageProficiencies = ['Basic', 'Intermediate', 'Advanced'];

        var queries = [];
        var defaultQuery = {
            hidden: false,
            hiddenOnSearch: false,
            role: 'normal',
            verified: true,
            banned: false,
            'resume.title': {$ne: null}
        };
        if (filter.city) {
            defaultQuery.city = filter.city;
        }
        if (filter.workExperienceGTE || filter.workExperienceLTE) {
            defaultQuery.workExperience = {};
            if (filter.workExperienceGTE) {
                defaultQuery.workExperience.$gt = filter.workExperienceGTE;
            }
            if (filter.workExperienceLTE) {
                defaultQuery.workExperience.$lt = filter.workExperienceLTE;
            }
        }
        // TODO: language
        // if (filter.languages) {
        //     defaultQuery.$or = [];
        //     _.forEach(filter.languages, function (level, name)
        //     {
        //         defaultQuery.$or.push({
        //             'resume.languages': {
        //                 name: name,
        //                 level: level
        //             }
        //         });
        //     });
        // }

        if (filter.days) {
            defaultQuery.createDate = {$gt: dateHelper.getMoment().subtract(3, 'days').set({
                    hour: 0,
                    minute: 0,
                    second: 0,
                    millisecond: 0
                }).valueOf()};
        }
        if (filter.foreignDegree) {
            defaultQuery['resume.foreignDegree'] = true;
        }

        if (filter.expat) {
            defaultQuery['resume.expat'] = true;
        }

        var from = parseInt(filter.from, 10);
        var size = parseInt(filter.size, 10);
        delete filter.from;
        delete filter.size;

        if (filter.resumeTitle) {

            // matching by resume title and category
            if (filter.categories) {
                queries.push(_.assign({}, defaultQuery, {
                    'resume.title': filter.resumeTitle,
                    'categories.0': filter.categories
                }));
                queries.push(_.assign({}, defaultQuery, {
                    'resume.title': filter.resumeTitle,
                    'categories.1': filter.categories
                }));
                queries.push(_.assign({}, defaultQuery, {
                    'resume.title': {$regex: filter.resumeTitle, $options: 'i'},
                    'categories.0': filter.categories
                }));
                queries.push(_.assign({}, defaultQuery, {
                    'resume.title': {$regex: filter.resumeTitle, $options: 'i'},
                    'categories.1': filter.categories
                }));
            }

            // matching by resume title only
            queries.push(_.assign({}, defaultQuery, {
                'resume.title': filter.resumeTitle
            }));
            queries.push(_.assign({}, defaultQuery, {
                'resume.title': {$regex: filter.resumeTitle, $options: 'i'}
            }));

            // matching by work title and category
            if (filter.categories) {
                queries.push(_.assign({}, defaultQuery, {
                    'resume.workExperience.title': {$regex: filter.resumeTitle, $options: 'i'},
                    'categories.0': filter.categories
                }));
                queries.push(_.assign({}, defaultQuery, {
                    'resume.workExperience.description': {$regex: filter.resumeTitle, $options: 'gm'},
                    'categories.0': filter.categories
                }));
                queries.push(_.assign({}, defaultQuery, {
                    'resume.workExperience.title': {$regex: filter.resumeTitle, $options: 'i'},
                    'categories.1': filter.categories
                }));
                queries.push(_.assign({}, defaultQuery, {
                    'resume.workExperience.description': {$regex: filter.resumeTitle, $options: 'gm'},
                    'categories.1': filter.categories
                }));
            }

            // matching by work title only
            queries.push(_.assign({}, defaultQuery, {
                'resume.workExperience.title': {$regex: filter.resumeTitle, $options: 'i'}
            }));
            queries.push(_.assign({}, defaultQuery, {
                'resume.workExperience.description': {$regex: filter.resumeTitle, $options: 'gm'}
            }));

            // matching by additionalInfo and category
            if (filter.categories) {
                queries.push(_.assign({}, defaultQuery, {
                    'resume.additionalInfo': {$regex: filter.resumeTitle, $options: 'gm'},
                    categories: filter.categories
                }));
            }

            // matching by additionalInfo only
            queries.push(_.assign({}, defaultQuery, {
                'resume.additionalInfo': {$regex: filter.resumeTitle, $options: 'gm'}
            }));
        }

        if (filter.categories) {
            queries.push(_.assign({}, defaultQuery, {
                'categories.0': filter.categories
            }));
            queries.push(_.assign({}, defaultQuery, {
                'categories.1': filter.categories
            }));
        }

        if (filter.education) {
            filter.education = Array.isArray(filter.education) ? filter.education : [filter.education];

            queries.push(_.assign({}, defaultQuery, {
                'resume.education.degree': {$in: filter.education}
            })
                    );
        }

        if (!queries.length) {
            queries.push(_.assign({}, defaultQuery));
        }

        var populateCity = {
            path: 'city',
            model: 'cities'
        };
        var populateCategories = {
            path: 'categories',
            model: 'categories'
        };
        var populateDesiredSalaryRange = {
            path: 'resume.desiredSalaryRange',
            model: 'salary_ranges'
        };

        return Promise.reduce(queries, function (results, query)
        {
            query._id = {$nin: _.map(results.results, 'id')};
            return UsersModel.find(query)
                    .sort({createDate: -1})
                    .skip(from)
                    .limit(size)
                    .populate(populateDesiredSalaryRange)
                    .populate(populateCity)
                    .populate(populateCategories)
                    .execAsync()
                    .then(function (data)
                    {
                        return UsersModel.countAsync(query).then(function (count)
                        {
                            return {
                                results: results.results.concat(mongoConverter.fromMongo(data)),
                                total: results.total + count
                            };
                        });
                    });
        }, {
            results: [],
            total: 0
        });
    }

    function remove(query, context)
    {
        if (context.user.role !== 'admin') {
            return removeByUser(query);
        }

        query = mongodb.ObjectID.isValid(query) ? {_id: query} : query;
        return UsersModel.findOneAndRemoveAsync(query);
    }

    function removeByUser(query)
    {
        query = mongodb.ObjectID.isValid(query) ? {_id: query} : query;
        return UsersModel.update(query, {hidden: true}).execAsync();
    }

    function queryUsersByArrayId(query)
    {
        return UsersModel.findAsync({_id: query}).then(function (results)
        {
            return {results: mongoConverter.fromMongo(results)};
        });
    }

    function findUserOffersIds(userId)
    {

        return UsersModel.findOneAsync({_id: userId}, {'offers.vacancyId': 1}).then(function (results)
        {
            return results.offers.map(function (element)
            {
                return element.vacancyId.toString();
            });
        });
    }

    function update(query, body, options)
    {
        query = mongodb.ObjectID.isValid(query.toString()) ? {_id: query} : query;
        options = options || {multi: false};
        return UsersModel.update(query, body, options).execAsync();
    }

    function searchVisited(user)
    {
        return new Promise(function (resolve, reject) {
             
            if (!user.searchVisited) {
                
                if (user.role == 'normal') {
                    vacanciesDAO.model.find({}).select({_id: 1}).then(function (result) {
                        vacanciesDAO.markManySeen(user.email, _.pluck(result, '_id'));
                    });
                } else {
                    UsersModel.find({}).select({_id: 1}).then(function (result) {
                        markManySeen(user.email, _.pluck(result, '_id'));
                    });
                }

                return UsersModel.findByIdAndUpdateAsync(user.id, {searchVisited: true}, {new : true});
            }


            resolve();
        });
    }

    function register(user)
    {
        user.verified = false;
        //i think remove role is better from create, default value set role to normal
        delete user.role;
        return new UsersModel(user).saveAsync().then(function (user)
        {
            return mongoConverter.fromMongo(user[0]);
        }).catch(function (error)
        {
            if (11000 === error.code) {
                throw applicationException.new(applicationException.PRECONDITION_FAILED, 'This email is taken');
            } else {
                throw error;
            }
        });
    }

    function search(filter, projection)
    {
        return UsersModel.findAsync(filter, projection).then(function (results)
        {
            return mongoConverter.fromMongo(results);
        });
    }

    function createExtendToken(userId)
    {
        return new ExtendProfileTokensModel({
            userId: userId
        }).saveAsync().then(function (token)
        {
            return mongoConverter.fromMongo(token[0]);
        });
    }

    function getUserFromExtendToken(token)
    {
        return ExtendProfileTokensModel.findOneAsync({_id: token}).then(function (token)
        {
            if (!token) {
                throw applicationException.new(applicationException.NOT_FOUND);
            }
            return UsersModel.findOneAsync({_id: token.userId}, {extendDate: 1});
        }).then(function (user)
        {
            if (!user) {
                throw applicationException.new(applicationException.NOT_FOUND);
            }
            return mongoConverter.fromMongo(user);
        });
    }

    function removeExtendToken(token)
    {
        return ExtendProfileTokensModel.removeAsync({_id: token});
    }


    function usersOffers(query, projection)
    {
        var from = Math.max(query.from, 0) || 0;
        var size = Math.max(query.size, 0) || 20;
        if (0 > query.size) {
            size = null;
        }

        delete query.from;
        delete query.size;
        delete query.query;


        projection = projection || {};
        var tmp;

        function checkIfShowEmailOnWebsiteAndRemapThumbnail(data)
        {
            for (var i = 0; i < data.length; i++) {
                if (!!data[i].offers.vacancyId) {
                    if (!data[i].offers.vacancyId._doc.showEmailOnWebsite) {
                        delete data[i].offers.vacancyId._doc.email;
                    }
                    delete data[i].offers.vacancyId._doc.showEmailOnWebsite;
                    delete data[i].offers.vacancyId._doc.userId._doc.resume;
                }
            }
        }

        function remapingData(data)
        {
            var tmp;
            for (var i = 0; i < data.length; i++) {
                if (!!data[i].offers.vacancyId) {
                    data[i].offers.vacancyId._doc.id = data[i].offers.vacancyId._doc._id;
                    delete data[i].offers.vacancyId._doc._id;
                    tmp = data[i].offers.vacancyId._doc;
                    tmp.addDate = data[i].offers.addDate;
                    tmp.offerId = data[i].offers._id;
                    tmp.seen = data[i].offers.seen;
                    delete data[i].offers.vacancyId;
                    data[i] = tmp;
                }
            }
        }

        return UsersModel.aggregateAsync([
            {$match: {_id: query.id, banned: false}},
            {$project: projection},
            {$unwind: '$offers'},
            {$sort: {'offers.addDate': -1}},
            {$skip: from},
            {$limit: size}
        ]).then(function (results)
        {
            tmp = results;
            return UsersModel.findOneAsync({_id: query.id});
        }).then(function (total)
        {
            return deepPopulateUserModelAsync(tmp,
                    ['offers.vacancyId', 'offers.vacancyId.cityId', 'offers.vacancyId.salaryRangeId', 'offers.vacancyId.categories', 'offers.vacancyId.userId'],
                    {
                        populate: {
                            'offers.vacancyId': {
                                select: 'cityId salaryRangeId email showEmailOnWebsite companyName title salary categories userId'
                            },
                            'offers.vacancyId.cityId': {
                                select: 'name'
                            },
                            'offers.vacancyId.salaryRangeId': {
                                select: 'name'
                            },
                            'offers.vacancyId.categories': {
                                select: 'name'
                            },
                            'offers.vacancyId.userId': {
                                select: 'thumbnail phone lastName'
                            }
                        }
                    }
            ).then(function (data)
            {
                checkIfShowEmailOnWebsiteAndRemapThumbnail(data);
                remapingData(data);
                return {results: data, total: total.offers.length};
            });
        });
    }

    function removeOffers(userId, offerId)
    {
        if (!Array.isArray(offerId)) {
            offerId = [offerId];
        }
        return UsersModel.findByIdAndUpdate(userId, {$pull: {offers: {_id: {$in: offerId}}}});
    }

    function removeUserOffers(vacancyId)
    {
        if (!Array.isArray(vacancyId)) {
            vacancyId = [vacancyId];
        }
        return UsersModel.updateAsync({'offers.vacancyId': {$in: vacancyId}}, {$pull: {offers: {vacancyId: {$in: vacancyId}}}}, {multi: true});
    }

    function markResumeViewedBy(resumeId, userId)
    {
        return UsersModel.findOne({_id: resumeId}).then(function(user){
            user.resumeViews = _.filter(user.resumeViews, function(view, index){
                return view.user.toString() !== userId.toString();
            });

            user.resumeViews.push({user: userId, date: dateHelper.get()});
            return UsersModel.updateAsync({_id: resumeId}, {resumeViews: user.resumeViews});
        });
    }

    function queryResumeViews(userId, filter) {
        var from = Math.max(filter.from, 0) || 0;
        var size = Math.max(filter.size, 0) || 5;

        var projection = {
            resumeViews: 1
        };

        console.log(filter);
        return UsersModel.findById(userId, projection).populate('resumeViews.user').execAsync().then(function (result)
        {
            var data = mongoConverter.fromMongo(result).resumeViews;

            return {
                results: data.slice(from, from + size),
                total: data.length
            };
        });
    }

    function removeAllViewsOfUser(userId)
    {
        return UsersModel.updateAsync({'resumeViews.user': userId}, {$pull: {resumeViews: {user: userId}}});
    }

    function countNewResumeViewsByUserId(userId)
    {
        var query = {
            _id: userId,
            resumeViews: {
                $elemMatch: {seen: false}
            }
        };

        var aggregation = [
            {
                $match: query
            },
            {
                $project: {_id: 0, seen: '$resumeViews.seen'}
            },
            {
                $unwind: '$seen'
            },
            {
                $match: {seen: false}
            },
            {
                $group: {
                    _id: {count: '$seen'},
                    count: {$sum: 1}
                }
            }
        ];

        return UsersModel.aggregateAsync(aggregation).then(function (result)
        {
            if (result && result.length) {
                return result[0].count;
            }

            return 0;
        });
    }

    function countNewOffersViewsByUserId(userId) {
        var query = {
            _id: userId,
            offers: {
                $elemMatch: {seen: false}
            }
        };

        var aggregation = [
            {
                $match: query
            },
            {
                $project: {_id: 0, seen: '$offers.seen'}
            },
            {
                $unwind: '$seen'
            },
            {
                $match: {seen: false}
            },
            {
                $group: {
                    _id: {count: '$seen'},
                    count: {$sum: 1}
                }
            }
        ];

        return UsersModel.aggregateAsync(aggregation).then(function (result)
        {
            if (result && result.length) {
                return result[0].count;
            }

            return 0;
        });
    }

    function getAll(filter)
    {
        return UsersModel.findAsync(filter).then(function (result)
        {
            return mongoConverter.fromMongo(result);
        });
    }

    function queryForJobSeekerCarousel()
    {
        var filter = {
            firstName: {$exists: true, $ne: ''},
            lastName: {$exists: true, $ne: ''},
            thumbnail: {$exists: true, $ne: ''},
            showOnHomePage: true,
            hidden: false,
            banned: false,
            verified: true,
            role: 'normal'
        };

        var populateCity = {
            path: 'city',
            model: 'cities'
        };
        var populateCategories = {
            path: 'categories',
            model: 'categories'
        };
        var populateDesiredSalaryRange = {
            path: 'resume.desiredSalaryRange',
            model: 'salary_ranges'
        };

        return UsersModel.find(filter, null, {sort: {createDate: -1}}).limit(15).populate(populateCity)
                .populate(populateCategories).populate(populateDesiredSalaryRange).execAsync().then(function (data)
        {
            return mongoConverter.fromMongo(data);
        }).then(function (data) {
            delete filter.showOnHomePage;
            return UsersModel.countAsync(filter).then(function (count) {
                //data.count = count;
                return {data: data, count: count}
            });
        });
    }

    function queryForEmployerCarousel()
    {
        var role_users = ['employer'];
        var filter = {
            firstName: {$exists: true, $ne: ''},
            thumbnail: {$exists: true, $ne: ''},
            showOnHomePage: true,
            hidden: false,
            banned: false,
            verified: true,
            role: {$in: role_users}
        };

        var populateCity = {
            path: 'city',
            model: 'cities'
        };
        var populateCategories = {
            path: 'categories',
            model: 'categories'
        };
        var populateDesiredSalaryRange = {
            path: 'resume.desiredSalaryRange',
            model: 'salary_ranges'
        };

        return UsersModel.find(filter, null, {sort: {createDate: -1}}).limit(15).populate(populateCity)
                .populate(populateCategories).populate(populateDesiredSalaryRange).execAsync().then(function (data)
        {
            return mongoConverter.fromMongo(data);
        }).then(function (data) {
            delete filter.showOnHomePage;
            return UsersModel.countAsync(filter).then(function (count) {
                return {data: data, count: count}
            });
        });
    }


    function isVisited(params)
    {
        return VisitedUsersModel.find({visited_user: params.visitedUserId, user: params.userId}).then(function (result)
        {

            if (result.length <= 0) {
                return new VisitedUsersModel({user: params.userId, email: params.userEmail, visited_user: params.visitedUserId}).saveAsync().then(function (data)
                {
                    var result = {};
                    _.forEach(data, function (item)
                    {
                        if (item.visited_user) {
                            result[item.visited_user] = true;
                        }
                    });

                    return result;
                });
            } else {
                var result = {};
                result[params.visitedUserId] = true;
            }
        });
    }

    function isVisitedList(email, userIds)
    {
        var mongooseUserIds = userIds.filter(function (item)
        {
            return mongodb.ObjectID.isValid(item && item.toString && item.toString());
        });

        return VisitedUsersModel.find({email: email, visited_user: {$in: mongooseUserIds}}).execAsync().then(function (data)
        {
            var result = {};
            _.forEach(userIds, function (userId)
            {
                result[userId] = false;
            });
            _.forEach(data, function (item)
            {
                if (item.visited_user) {
                    result[item.visited_user] = true;
                }
            });

            return result;
        });
    }


    module.exports = {
        search: search,
        createNewOrUpdate: createNewOrUpdate,
        getIdByEmailOrCreateNew: getIdByEmailOrCreateNew,
        queryUsersByArrayId: queryUsersByArrayId,
        query: query,
        find: find,
        isNew: isNew,
        searchVisited: searchVisited,
        activateAccount: activateAccount,
        remove: remove,
        get: get,
        update: update,
        register: register,
        createExtendToken: createExtendToken,
        getUserFromExtendToken: getUserFromExtendToken,
        removeExtendToken: removeExtendToken,
        usersOffers: usersOffers,
        removeOffers: removeOffers,
        findUserOffersIds: findUserOffersIds,
        removeUserOffers: removeUserOffers,
        markSeen: markSeen,
        markManySeen: markManySeen,
        removeFromSeenByUserId: removeFromSeenByUserId,
        markResumeViewedBy: markResumeViewedBy,
        queryResumeViews: queryResumeViews,
        removeAllViewsOfUser: removeAllViewsOfUser,
        countNewResumeViewsByUserId: countNewResumeViewsByUserId,
        countNewOffersViewsByUserId: countNewOffersViewsByUserId,
        getAll: getAll,
        queryForJobSeekerCarousel: queryForJobSeekerCarousel,
        queryForEmployerCarousel: queryForEmployerCarousel,
        offset: offset,
        model: UsersModel,
        isVisited: isVisited,
        isVisitedList: isVisitedList,
        markAllAsNotNew: markAllAsNotNew
    };
})();
