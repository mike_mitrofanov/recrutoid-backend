(function ()
{
    'use strict';
    var mongoose = require('mongoose-bird')(require('mongoose'));
    var mongoConverter = require('../DAO/mongoConverter');
    var languagesDAO = require('../DAO/languagesDAO');
    var dateHelper = require('../service/dateHelper');
    var getDate = dateHelper.get;
    var _ = require('lodash');
    var phrasesSchema = new mongoose.Schema({
        phrase: {type: Object, required: true},
        createDate: {type: Number, default: getDate}
    }, {
        collection: 'phrases'
    });
    var PhrasesModel = mongoose.model('phrases', phrasesSchema);

    function query(filter, projection)
    {
        var from = Math.max(filter.from, 0) || 0;
        var size = Math.max(filter.size, 0) || null;
        if (0 > filter.size) {
            size = null;
        }
        if (!_.isEmpty(filter.action)) {
            if (!filter.hasOwnProperty('$or')) {
                filter.$or = [];
            }
            filter.$or.push(
                    {'phrase.en': {$regex: filter.action, $options: 'i'}},
                    {'phrase.ru': {$regex: filter.action, $options: 'i'}}

            );
        }

        projection = projection || {};
        delete filter.from;
        delete filter.size;
        delete filter.action;

        var sort = {action: 1};

        var findQuery = PhrasesModel.find(filter, projection).skip(from).sort(sort);
        if (null != size) {
            findQuery.limit(size);
        }
        return findQuery.execAsync().then(function (data)
        {
            return mongoConverter.fromMongo(data);
        }).then(function (results)
        {
            return PhrasesModel.countAsync(filter).then(function (count)
            {
                return {results: results, total: count};
            });
        });
    }

    function getTranslationsForTitle(vacancyTitle) {
        return new Promise(function (resolve, reject) {

            if (_.isEmpty(vacancyTitle)) {
                resolve();
                return;
            }

            var words = vacancyTitle.split(" ");
            var promises = [];
            var languageCount = 0;

            languagesDAO.model.find({}).then(function (languages) {
                words.forEach(function (word, index) {
                    var phrasesFilter = {$or: []};

                    languages.forEach(function (lang) {
                        phrasesFilter.$or.push(JSON.parse('{\"' + 'phrase.' + lang.code + '\":{ \"$regex\": \"^' + word + '\", \"$options\": \"ig\" }}'));
                    });

                    promises.push(PhrasesModel.find(phrasesFilter).distinct('phrase'));

                });

                Promise.all(promises).then(function (res) {
                    var words = {};

                    for (var i in res) {

                        for (var k in res[i][0]) {
                            
                            if (!words[k]) {
                                words[k] = "";
                            }

                            words[k] += " " + res[i][0][k];
                            words[k] = words[k].trim();
                        }
                    }

                    resolve(words);
                    
                }, function (error) {
                    reject(error);
                });

            });

        });
    }

    function getPhrasesForVacancy(filter) {
        return new Promise(function (resolve, reject) {
            if (!_.isEmpty(filter.vacancyTitle)) {
                var words = filter.vacancyTitle.split(" ");
                var promises = [];

                languagesDAO.model.find({}).then(function (languages) {
                    words.forEach(function (word, index) {

                        var phrasesFilter = {$or: []};

                        languages.forEach(function (lang) {
                            phrasesFilter.$or.push(JSON.parse('{\"' + 'phrase.' + lang.code + '\":{ \"$regex\": \"^' + word + '\", \"$options\": \"ig\" }}'));
                        });

                        promises.push(PhrasesModel.find(phrasesFilter).distinct('phrase'));
                    });

                    Promise.all(promises).then(function (results) {

                        var vacancyFilter = {};
                        var currentPhrase;
                        var finalPhrases = [];

                        results.forEach(function (oneResult) {
                            if (oneResult.length) {
                                oneResult.forEach(function (item, index) {
                                    _.map(item, function (phrase, lang) {
                                        if (!(lang in vacancyFilter)) {
                                            vacancyFilter[lang] = [];
                                        }

                                        currentPhrase = vacancyFilter[lang][index];

                                        vacancyFilter[lang][index] = currentPhrase ? currentPhrase + " " + phrase : phrase;
                                    });
                                });


                                _.map(vacancyFilter, function (array, key) {
                                    finalPhrases = finalPhrases.concat(array);
                                });
                            }
                        });

                        finalPhrases.reverse();

                        if (finalPhrases.length <= 8) {

                            filter.vacancyTitlePhrases = finalPhrases;
                        }

                        resolve(filter);

                    }, function (error) {
                        reject(error);
                    });
                });

            } else {
                resolve(filter);
            }

        });
    }

    function update(phrase)
    {
        return PhrasesModel.findByIdAndUpdateAsync(phrase.id, phrase, {new : true}).then(function (result)
        {
            return mongoConverter.fromMongo(result);
        });
    }

    function add(phrase)
    {
        if (!phrase.id) {
            return new PhrasesModel(phrase).saveAsync().then(function (result)
            {
                return;
                return mongoConverter.fromMongo(result[0]);
            }).catch(function (error)
            {
                if (11000 === error.code) {
                    throw applicationException.new(applicationException.CONFLICT, 'Page with provided slug already exist.');
                } else {
                    throw error;
                }
            });
        }
    }

    function remove(id)
    {
        return PhrasesModel.removeAsync({_id: id});
    }

    function get(query)
    {
        return PhrasesModel.findOneAsync(query);
    }

    module.exports = {
        update: update,
        remove: remove,
        query: query,
        get: get,
        add: add,
        getPhrasesForVacancy: getPhrasesForVacancy,
        getTranslationsForTitle: getTranslationsForTitle,
        model: PhrasesModel
    };
})();
