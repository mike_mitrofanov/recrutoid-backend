(function ()
{
    'use strict';
    var mongoConverter = require('./mongoConverter');
    var applicationException = require('../service/applicationException');
    var mongoose = require('mongoose-bird')(require('mongoose'));
    var getDate = require('../service/dateHelper').get;
    var cms = new mongoose.Schema({
        title: {
            type:Object,
            en: {
                type: String,
                required: true
            },
            ru: {
                type: String,
                required: true
            }
        },
        keywords: {
            type:Object,
            en: {
                type: String
            },
            ru: {
                type: String
            }
        },
        metadescription: {
            type:Object,
            en: {
                type: String
            },
            ru: {
                type: String
            }
        },
        content: {
            type:Object,
            en: {
                type: String
            },
            ru: {
                type: String
            }
        },
        slug: {type: String, required: true, unique: true},
        createDate: {type: Number, required: true, default: getDate},
        static: {type: Boolean, required: true, default: false},

        // extended with the purpose of storing news boxes (boxes on bottom of homepage).
        // I feel that concept of those boxes will change and they will be regular cms pages too.
        newsBox: {type: Boolean, default: false},
        image: {type: String},
        shortDescription: {
            type:Object,
            en: {
                type: String
            },
            ru: {
                type: String
            }
        }
    }, {
        collection: 'cms'
    });
    cms.index({slug: 'text', 'title.en': 'text', 'title.ru': 'text'});
    var Model = mongoose.model('cms', cms);


    function createNewOrUpdate(page)
    {
        if (!page.id) {
            return new Model(page).saveAsync().then(function (result)
            {
                return mongoConverter.fromMongo(result[0]);
            }).catch(function (error)
            {
                if (11000 === error.code) {
                    throw applicationException.new(applicationException.CONFLICT, 'Page with provided slug already exist.');
                } else {
                    throw error;
                }
            });
        } else {
            return Model.findByIdAndUpdateAsync(page.id, page).then(function (result)
            {
                return mongoConverter.fromMongo(result);
            });
        }
    }

    function query(filter)
    {
        if(filter.hasOwnProperty('query') && filter.query.length) {
            filter.$text = {
                $search: filter.query
            };
        }

        delete filter.query;

        filter.newsBox = 'true' === filter.newsBoxes;
        delete filter.newsBoxes;

        // because of returning only custom pages
        filter.static = false;

        var from = Math.max(filter.from, 0) || 0;
        var size = Math.max(filter.size, 0) || 20;
        if (0 > filter.size) {
            size = null;
        }

        delete filter.from;
        delete filter.size;
        var sort = {createDate: -1};
        var findQuery = Model.find(filter).skip(from).sort(sort);
        if (null != size) {
            findQuery.limit(size);
        }
        return findQuery.execAsync().then(function (data)
        {
            return mongoConverter.fromMongo(data);
        }).then(function (results)
        {
            return Model.countAsync(filter).then(function (count)
            {
                return {results: results, total: count};
            });
        });
    }

    function getList()
    {
        var projection = {
            title: 1,
            slug: 1
        };
        return Model.findAsync({newsBox: false}, projection).then(function (data)
        {
            return mongoConverter.fromMongo(data);
        });
    }

    function getNews()
    {
        var projection = {
            title: 1,
            image: 1,
            shortDescription: 1
        };
        return Model.findAsync({newsBox: true}, projection).then(function (data)
        {
            return mongoConverter.fromMongo(data);
        });
    }

    function remove(pageId)
    {
        return Model.findByIdAndRemoveAsync(pageId);
    }

    function get(slug)
    {
        return Model.findOne({slug: slug}).execAsync().then(function (result)
        {
            if (!result) {
                throw  applicationException.new(applicationException.NOT_FOUND);
            }

            return mongoConverter.fromMongo(result);
        });
    }

    function updateImage(id, imageUrl)
    {
        return Model.updateAsync({_id: id}, {$set: {image: imageUrl}}, {new: true});
    }

    module.exports = {
        query: query,
        getList: getList,
        getNews: getNews,
        remove: remove,
        get: get,
        createNewOrUpdate: createNewOrUpdate,
        updateImage: updateImage,

        model: Model
    };
})();
