'use strict';

function copyBoolean(query, filter, propertyName)
{
    /*jshint eqeqeq:false*/
    if (null != filter[propertyName]) {
        query[propertyName] = 'false' != filter[propertyName];
    }
}

function copy(query, filter, filterPropertyName, queryPropertyName)
{
    queryPropertyName = queryPropertyName || filterPropertyName;
    if (null != filter[filterPropertyName]) {
        query[queryPropertyName] = filter[filterPropertyName];
    }
}
module.exports = {
    copy: copy,
    copyBoolean: copyBoolean
};
