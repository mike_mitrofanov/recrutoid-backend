(function ()
{
    'use strict';

    var jobsFeedModel = require('../app/modules/job_feeds/job_feedsDAO').model;

    var Promise = require('bluebird');
    var mongoose = require('mongoose-bird')(require('mongoose'));
    var connectPromise = Promise.promisify(mongoose.connect, mongoose);

    function seedByServer()
    {
        return jobsFeedModel.removeAsync({}).then(function ()
        {
            return jobsFeedModel.createAsync(require('./jobFeeds.json'));
        }).then(function ()
        {
            console.log('JobFeeds loaded');
        }).catch(function (error)
        {
            console.error('error');
            console.error(error);
        });
    }

    function seedStandAlone()
    {
        return connectPromise(require('./../app/config/config').url).then(seedByServer);
    }

    seedStandAlone().then(function ()
    {
        process.exit(0);
    }).catch(function ()
    {
        process.exit(1);
    });

})();
