(function ()
{
    'use strict';

    var _ = require('lodash');
    var mongoConverter = require('./../app/DAO/mongoConverter');
    var CategoriesModel = require('../app/DAO/categoriesDAO').model;
    var CitiesModel = require('../app/DAO/citiesDAO').model;
    var SalaryRangesModel = require('../app/DAO/salaryRangesDAO').model;
    var LanguageModel = require('../app/DAO/languagesDAO').model;
    var PasswordModel = require('../app/DAO/passwordsDAO').model;
    var SubscriptionModel = require('../app/DAO/subscriptionsDAO').model;
    var PhrasesModel = require('../app/DAO/phrasesDAO').model;
    var UsersModel = require('../app/DAO/usersDAO').model;
    var UsersPermissionsModel = require('./../app/DAO/usersPermissionsDAO').model;
    var VacanciesModel = require('../app/DAO/vacanciesDAO').model;
    var EmailTokensModel = require('../app/DAO/emailTokensDAO').model;
    var dateHelper = require('./../app/service/dateHelper');
    var sha1 = require('sha1');
    var faker = require('faker');
    var Promise = require('bluebird');
    var moment = require('moment-timezone');
    var howManyUsers = 1000;
    var howManyAnonymous = 50;
    var mongoose = require('mongoose-bird')(require('mongoose'));
    var connectPromise = Promise.promisify(mongoose.connect, mongoose);
    var fs = require('fs');

    function queryIdCategories()
    {
        return CategoriesModel.findAsync().then(function (results)
        {
            return mongoConverter.fromMongo(results).map(function (category)
            {
                return category.id;
            });
        });
    }

    function queryIdCities()
    {
        return CitiesModel.findAsync().then(function (results)
        {
            return mongoConverter.fromMongo(results).map(function (city)
            {
                return city.id;
            });
        });
    }

    function queryIdSalaryRanges()
    {
        return SalaryRangesModel.findAsync().then(function (results)
        {
            return mongoConverter.fromMongo(results).map(function (salaryRange)
            {
                return salaryRange.id;
            });
        });
    }

    function getLanguages()
    {
        return LanguageModel.findAsync().then(function (results)
        {
            return mongoConverter.fromMongo(results).map(function (language)
            {
                return language.id;
            });
        });
    }


    function generatedUsers(languages)
    {
        var promises = {
            categories: queryIdCategories(),
            cities: queryIdCities(),
            salaryRanges: queryIdSalaryRanges()
        };
        return Promise.props(promises).then(function (result)
        {
            var showOnHomePageLimit = 14;
            var showOnHomePageLimitForEmployer = 14;
            var categories = result.categories;
            var cities = result.cities;
            var salaryRanges = result.salaryRanges;
            var users = [];
            var verified, banned, subscribed, language, role, random, user, hasUploadedCv;
            var langArray = ['en', 'ru'];
            for (var i = 0; i < howManyUsers; i++) {
                random = getRandomInt(0, categories.length - 5);
                verified = i % 7 !== 0;
                subscribed = i % 6 !== 0;
                banned = i % 10 === 0;
                language = i % 5 !== 0 ? languages : [languages[i % 2]];
                hasUploadedCv = Math.random() >= 0.5;
                role = i % 3 !== 0 ? 'normal' : 'employer';
                if (role == 'employer') {
                    user = {
                        email: '' + i + faker.internet.email(null, null, 'company.com'),
                        firstName: faker.company.companyName(),
                        lastName: faker.lorem.sentence(),
                        verified: verified,
                        subscribed: subscribed,
                        resume: {},
                        banned: banned,
                        languages: language,
                        phone: getRandomPhone(),
                        thumbnail: 'http://lorempixel.com/88/88/abstract?' + i,
                        city: cities[getRandomInt(0, 10)],
                        categories: categories[getRandomInt(0, 10)],
                        role: role
                    };

                    if (showOnHomePageLimitForEmployer > 0) {
                        user.showOnHomePage = true;
                        user.banned = false;
                        user.verified = true;
                        user.hidden = false;
                        showOnHomePageLimitForEmployer--;
                    } else {
                        user.showOnHomePage = false;
                    }
                } else {
                    user = {
                        email: '' + i + faker.internet.email(null, null, 'example.com'),
                        firstName: faker.name.firstName(),
                        lastName: faker.name.lastName(),
                        verified: verified,
                        subscribed: subscribed,
                        resume: {},
                        banned: banned,
                        languages: language,
                        phone: getRandomPhone(),
                        thumbnail: 'http://lorempixel.com/88/88/people?' + i,
                        city: cities[getRandomInt(0, 10)],
                        categories: categories[getRandomInt(0, 10)],
                        role: role
                    };

                    if (showOnHomePageLimit > 0) {
                        user.showOnHomePage = true;
                        user.banned = false;
                        user.verified = true;
                        user.hidden = false;
                        showOnHomePageLimit--;
                    } else {
                        user.showOnHomePage = false;
                    }


                    if (!hasUploadedCv) {
                        var degrees = [
                            "PhD",
                            "Bachelors",
                            "Masters"
                        ];

                        var langs = [];

                        var enLang = {
                            name: "English",
                            code: "en",
                            level: getRandomInt(0, 2)
                        };

                        var ruLang = {
                            name: "Russian",
                            code: "ru",
                            level: getRandomInt(0, 2)
                        };


                        if (getRandomInt(0, 100) % 2) {
                            langs.push(enLang);
                        }

                        if (getRandomInt(0, 100) % 2) {
                            langs.push(ruLang);
                        }

                        if (langs.length === 0) {
                            langs = [
                                enLang,
                                ruLang
                            ];
                        }

                        var cvTitleLang = 'en';

                        if (getRandomInt(0, 100) % 2 == 0) {
                            cvTitleLang = 'ru';
                        }

                        user.resume = {
                            additionalInfo: faker.lorem.paragraph(),
                            title: getJobTitle(cvTitleLang),
                            desiredSalaryRange: salaryRanges[getRandomInt(0, salaryRanges.length - 1)],
                            languages: langs,
                            education: [
                                {
                                    endDate: 1454194800000.0,
                                    startDate: 1170198000000.0,
                                    degree: degrees[getRandomInt(0, degrees.length - 1)],
                                    city: faker.address.city(),
                                    fieldOfStudy: "Field of study",
                                    school: "Some school"
                                },
                                {
                                    endDate: 1454194800000.0,
                                    startDate: 1201734000000.0,
                                    degree: degrees[getRandomInt(0, degrees.length - 1)],
                                    city: faker.address.city(),
                                    fieldOfStudy: "Intelectual Property",
                                    school: "Another school"
                                }
                            ],
                            updateDate: dateHelper.get(),
                            expat: i % 4 === 0,
                            foreignDegree: i % 3 === 0
                        };


                        var fakeStart = faker.date.between('2006-01-01', '2016-01-01');
                        var fakeEnd = faker.date.between(fakeStart, '2017-01-01');
                        var fakeCurrent = faker.date.between(fakeStart, new Date());
                        var work = [
                            {
                                title: faker.name.jobTitle(),
                                company: faker.company.companyName(),
                                city: faker.address.city(),
                                description: faker.lorem.paragraph(),
                                startDate: fakeCurrent.getTime(),
                                currentWork: true
                            },
                            {
                                description: faker.lorem.paragraph(),
                                title: faker.name.jobTitle(),
                                company: faker.company.companyName(),
                                city: faker.address.city(),
                                startDate: fakeStart.getTime(),
                                endDate: fakeEnd.getTime(),
                                currentWork: false
                            }
                        ];

                        user.resume.workExperience = work;
                        user.workExperience = calculateWorkExperience(work);
                    } else {
                        user.uploadedResume = "http://res.cloudinary.com/hkuil73zi/image/upload/v1481043253/cv-5846e47dd6a2f4cdeca66cc6.pdf";
                    }
                }

                users.push(user);

            }

            function calculateWorkExperience(work)
            {
                var totalWorkExperience = 0;
                for (var i = 0; i < work.length; i++) {
                    if (work[i].startDate && work[i].endDate) {
                        totalWorkExperience += (work[i].endDate - work[i].startDate) + dateHelper.moment.duration(10,'d').asMilliseconds() + dateHelper.moment.duration(1,'M').asMilliseconds();
                    } else if (work[i].startDate && !work[i].endDate) {
                        totalWorkExperience += Date.now() - work[i].startDate;
                    }
                }
                return totalWorkExperience;
            }

            var seededUsers;
            return UsersModel.createAsync(users).then(function (results)
            {
                return mongoConverter.fromMongo(results);
            }).then(function (results)
            {
                seededUsers = results;
                var permissions = [];
                results.map(function (user)
                {
                    if ('employer' === user.role) {
                        permissions.push({
                            userId: user.id,
                            distributing: false,
                            publishing: false,
                            cvSearching: false
                        });
                    }
                });
                return UsersPermissionsModel.createAsync(permissions);
            }).then(function ()
            {
                return seededUsers;
            });
        });
    }

    function generatePassword(users)
    {
        var passwords = [];
        for (var i = 0; i < users.length - 1; i++) {
            passwords.push({_id: users[i].id, password: sha1(faker.lorem.sentence())});
        }
        return PasswordModel.createAsync(passwords).then(function ()
        {
            return users.map(function (user)
            {
                return {
                    userId: user.id,
                    email: user.email,
                    role: user.role,
                    firstName:
                            user.firstName,
                    hasResume: user.resume && user.resume.workExperience.length
                };
            });
        });
    }

    function getRandomInt(min, max)
    {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function generatedVacancies(arrayUsers)
    {
        console.log('generatedVacancies');
        var promises = [];
        promises.push(queryIdCategories());
        promises.push(queryIdCities());
        promises.push(queryIdSalaryRanges());

        function generateVacancyToEmployer(categories, vacancies, cities, salaryRanges)
        {
            var groupedUsers = _.groupBy(arrayUsers, function (item) {
                return item.role;
            });

            var usersWithCv = _.filter(groupedUsers['normal'], function (user) {
                return user.hasResume;
            });

            var approved, banned, publishedDate, companyName, email, title, lang;
            var random;
            for (var i = 0; i < arrayUsers.length; i++) {
                var user = arrayUsers[i];
                if (user.role == 'employer') {
                    for (var j = 0; j < getRandomInt(5, 10); j++) {
                        random = getRandomInt(0, categories.length - 10);
                        approved = j % 4 === 0;
                        banned = j % 10 === 0;
                        lang = j % 3 == 0 ? 'en' : 'ru';
                        publishedDate = dateHelper.get() - getRandomInt(0, 5000);
                        companyName = user.firstName;
                        email = user.email;
                        title = getJobTitle(lang);

                        //Published (published on webiste)
                        var publishedVacancy = {
                            companyName: companyName,
                            email: email,
                            showEmailOnWebsite: true,
                            title: title,
                            cityId: cities[getRandomInt(0, 10)],
                            salaryRangeId: salaryRanges[getRandomInt(0, salaryRanges.length - 1)],
                            details: faker.lorem.paragraph() + " " + faker.lorem.paragraph(),
                            userId: user.userId,
                            applicants: [],
                            extendDate: publishedDate,
                            createDate: publishedDate,
                            sentEmails: 0,
                            verified: true,
                            archived: false,
                            publishedDate: publishedDate,
                            banned: false,
                            approved: true,
                            publishViaEmail: false,
                            publishOnWebsite: true,
                            categories: categories[getRandomInt(0, 10)]
                        };
                        //Distributed (daily sending mode, distributed via email)
                        //This is always copy of vacancy, not new one
                        var distributedVacancy = Object.assign({}, publishedVacancy);
                        distributedVacancy.approved = false;
                        distributedVacancy.publishViaEmail = true;
                        distributedVacancy.publishOnWebsite = false;

                        //First 20 vacancies for test employer
                        if (i < 20) {
                            publishedVacancy.applicants = [];
                            for (var x = 0; x < 20; x++) {
                                publishedVacancy.applicants.push({
                                    user: usersWithCv[x].userId
                                });
                            }

                            publishedVacancy.userId = "51eae100c2e6b6c222ec3443";
                            distributedVacancy.userId = "51eae100c2e6b6c222ec3443";
                            distributedVacancy.approved = i % 3 == 0;
                        }


                        vacancies.push(publishedVacancy);
                        vacancies.push(distributedVacancy);
                    }
                }
            }
        }

        return Promise.all(promises).then(function (array)
        {
            var categories = array[0];
            var cities = array[1];
            var salaryRanges = array[2];
            var vacancies = [];

            generateVacancyToEmployer(categories, vacancies, cities, salaryRanges);
            console.log('count vacancies ', vacancies.length);
            return VacanciesModel.createAsync(vacancies).then(function ()
            {
                return {cities: cities, categories: categories, users: arrayUsers, salaryRanges: salaryRanges};
            }).catch(function (error)
            {
                console.error(error);
            });
        });
    }

    function generateSubscribers(data, model)
    {
        console.log('generateSubscribers');
        function generateSubscriptionsToUsers(subscriptions)
        {
            var random, interval, date, banned, createDate, subscribed, nullCategories, nullCity, nullSalaryRange;
            for (var i = 0; i < data.users.length; i++) {
                if (data.users[i].role == 'normal') {
                    interval = i % 5 !== 0 ? 'daily' : 'weekly';
                    banned = i % 4 !== 0;
                    subscribed = i % 7 !== 0;
                    nullCategories = i % 5 !== 0;
                    nullCity = i % 3 !== 0;
                    nullSalaryRange = i % 4 !== 0;
                    createDate = dateHelper.get() - getRandomInt(0, 5000);


                    random = getRandomInt(0, data.categories.length - 5);
                    subscriptions.push({
                        interval: interval,
                        city: nullCity ? null : data.cities[getRandomInt(0, 10)],
                        desiredSalaryRange: nullSalaryRange ? null : data.salaryRanges[getRandomInt(0, data.salaryRanges.length - 1)],
                        email: data.users[getRandomInt(0, data.users.length - 1)].email,
                        verified: true,
                        subscribed: subscribed,
                        createDate: createDate,
                        banned: banned,
                        categories: nullCategories ? [null] : data.categories[getRandomInt(0, 10)]
                    });
                }
            }
        }

        function generateSubscribersToAnonymous(subscriptions)
        {
            var random, interval, createDate, banned, subscribed, nullCategories, nullCity, nullSalaryRange;
            for (var i = 0; i < howManyAnonymous; i++) {
                interval = i % 5 !== 0 ? 'daily' : 'weekly';
                banned = i % 4 !== 0;
                subscribed = i % 6 !== 0;
                random = getRandomInt(0, data.categories.length - 5);

                nullCategories = i % 5 !== 0;
                nullCity = i % 3 !== 0;
                nullSalaryRange = i % 4 !== 0;
                createDate = dateHelper.get() - getRandomInt(0, 5000);

                subscriptions.push({
                    interval: interval,
                    city: nullCity ? null : data.cities[getRandomInt(0, 10)],
                    desiredSalaryRange: nullSalaryRange ? null : data.salaryRanges[getRandomInt(0, data.salaryRanges.length - 1)],
                    email: faker.internet.email(null, null, 'example.com'),
                    verified: false,
                    subscribed: subscribed,
                    createDate: createDate,
                    banned: banned,
                    categories: nullCategories ? [null] : data.categories[getRandomInt(0, 10)]
                });
            }
        }


        var subscriptions = [];
        generateSubscriptionsToUsers(subscriptions);
        generateSubscribersToAnonymous(subscriptions);
        SubscriptionModel = model || SubscriptionModel;
        return SubscriptionModel.removeAsync({}).then(function ()
        {
            console.log('count subscriptions ', subscriptions.length);
            return SubscriptionModel.createAsync(subscriptions);
        });
    }

    function generateEmailTokens() {
        console.log('generateEmailTokens');
        return VacanciesModel.find({}).then(function (result) {
            var promises = _.map(result, function (vacancy) {
                var newToken = {
                    originId: vacancy._id,
                    date: dateHelper.get(),
                    type: 'vacancy'
                };
                new EmailTokensModel(newToken).saveAsync();
            });

            console.log('count email tokens', promises.length);
            return Promise.all(promises);
        })
    }

    function getJobTitle(lang, onlyJobType) {
        var jobDescriptors = JSON.parse(fs.readFileSync('./seed/job_descriptor.json', 'utf8'));
        var jobAreas = JSON.parse(fs.readFileSync('./seed/job_area.json', 'utf8'));
        var jobTypes = JSON.parse(fs.readFileSync('./seed/job_type.json', 'utf8'));

        var randomJobDescriptor = jobDescriptors[Math.floor(Math.random() * jobDescriptors.length)].phrase[lang];
        var randomJobArea = jobAreas[Math.floor(Math.random() * jobAreas.length)].phrase[lang];
        var randomJobType = jobTypes[Math.floor(Math.random() * jobTypes.length)].phrase[lang];

        if (onlyJobType) {
            return randomJobType;
        }

        return randomJobDescriptor + " " + randomJobArea + " " + randomJobType;
    }

    function getRandomIntInclusive(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    function getRandomPhone()
    {
        var
                areaCode = ""+getRandomIntInclusive(1, 10),
                three1 = ""+getRandomIntInclusive(100, 999),
                three2 = ""+getRandomIntInclusive(100, 999),
                two = ""+getRandomIntInclusive(10, 99),
                two2 = ""+getRandomIntInclusive(10, 99) 
                ;
       
        
       return '+'+areaCode+' '+'('+three1+')'+' '+three2+'-'+two+'-'+two2;

    }

    function seedByServer()
    {
        return  getLanguages().then(generatedUsers).then(generatePassword).then(generatedVacancies).then(generateSubscribers).then(generateEmailTokens).then(function ()
        {
            console.log('finish seed data');

        }).catch(function (error)
        {
            console.error(error);
        });
    }

    function seedStandAlone()
    {
        function clearDatabase()
        {
            return PasswordModel.removeAsync({}).then(function ()
            {
                return SubscriptionModel.removeAsync({});
            }).then(function ()
            {
                return UsersModel.removeAsync({});
            }).then(function ()
            {
                return VacanciesModel.removeAsync({});
            }).then(function ()
            {
                return EmailTokensModel.removeAsync({});
            }).catch(function (error)
            {
                console.error(error);
            });
        }

        return connectPromise(require('./../app/config/config').url).then(function ()
        {
            return clearDatabase();
        }).then(require('./seed').seedByServer).then(seedByServer);
    }

    seedStandAlone().then(function ()
    {
        process.exit(0);
    }).catch(function ()
    {
        process.exit(1);
    });

})();
