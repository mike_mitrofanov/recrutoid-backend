(function ()
{
    'use strict';

    var models = {
        activationAccountToken: require('./../app/DAO/actiavateAccountToken').model,
        blackList: require('./../app/DAO/blacklistedDomainsDAO').model,
        categories: require('./../app/DAO/categoriesDAO').model,
        cities: require('./../app/DAO/citiesDAO').model,
        salaryRanges: require('../app/DAO/salaryRangesDAO').model,
        emailsTokens: require('./../app/DAO/emailTokensDAO').model,
        languages: require('./../app/DAO/languagesDAO').model,
        password: require('./../app/DAO/passwordsDAO').model,
        subscription: require('../app/DAO/subscriptionsDAO').model,
        tokens: require('../app/DAO/tokensDAO').model,
        user: require('./../app/DAO/usersDAO').model,
        userPermissions: require('./../app/DAO/usersPermissionsDAO').model,
        vacancies: require('../app/DAO/vacanciesDAO').model,
        seenVacancies: require('../app/DAO/vacanciesDAO').seenVacancies,
        settings: require('../app/DAO/settingsDAO').model,
        cms: require('../app/DAO/cmsDAO').model,
        bugreports: require('../app/DAO/bugreportsDAO').model,
        translations: require('../app/DAO/translationsDAO').model,
        subscriptions: require('../app/DAO/subscriptionsDAO').model,
        emailErrors: require('../app/DAO/emailErrorsDAO').model,
        phrases: require('../app/DAO/phrasesDAO').model
    };
    var Promise = require('bluebird');
    var mongoose = require('mongoose-bird')(require('mongoose'));
    var connectPromise = Promise.promisify(mongoose.connect, mongoose);

    function seedByServer()
    {
        return models.activationAccountToken.removeAsync({}).then(function ()
        {
            return models.blackList.removeAsync({});
        }).then(function ()
        {
            return models.categories.removeAsync({});
        }).then(function ()
        {
            return models.cities.removeAsync({});
        }).then(function ()
        {
            return models.salaryRanges.removeAsync({});
        }).then(function ()
        {
            return models.languages.removeAsync({});
        }).then(function ()
        {
            return models.password.removeAsync({});
        }).then(function ()
        {
            return models.subscription.removeAsync({});
        }).then(function ()
        {
            return models.tokens.removeAsync({});
        }).then(function ()
        {
            return models.user.removeAsync({});
        }).then(function ()
        {
            return models.userPermissions.removeAsync({});
        }).then(function ()
        {
            return models.vacancies.removeAsync({});
        }).then(function ()
        {
            return models.seenVacancies.removeAsync({});
        }).then(function ()
        {
            return models.settings.removeAsync({});
        }).then(function ()
        {
            return models.cms.removeAsync({});
        }).then(function ()
        {
            return models.subscriptions.removeAsync({});
        }).then(function ()
        {
            return models.bugreports.removeAsync({});
        }).then(function ()
        {
            return models.translations.removeAsync({});
        }).then(function ()
        {
            return models.emailsTokens.removeAsync({});
        }).then(function ()
        {
            return models.emailErrors.removeAsync({});
        }).then(function ()
        {
            return models.phrases.removeAsync({});
        }).then(function ()
        {
            return models.categories.createAsync(require('./category.json'));
        }).then(function ()
        {
            return models.cities.createAsync(require('./cities.json'));
        }).then(function ()
        {
            return models.salaryRanges.createAsync(require('./salary_ranges.json'));
        }).then(function ()
        {
            return models.languages.createAsync(require('./languages.json'));
        }).then(function ()
        {
            return models.blackList.createAsync(require('./blacklisted_domains.json'));
        }).then(function ()
        {
            return models.user.createAsync(require('./users.json'));
        }).then(function ()
        {
            return models.userPermissions.createAsync(require('./users_permissions.json'));
        }).then(function ()
        {
            return models.vacancies.createAsync(require('./vacancies.json'));
        }).then(function ()
        {
            return models.password.createAsync(require('./users_passwords.json'));
        }).then(function ()
        {
            return models.settings.createAsync({});
        }).then(function ()
        {
            return models.cms.createAsync(require('./cms.json'));
        }).then(function ()
        {
            return models.subscriptions.createAsync(require('./subscriptions.json'));
        }).then(function ()
        {
            return models.bugreports.createAsync(require('./bugreports.json'));
        }).then(function ()
        {
            return models.translations.createAsync(require('./translations.json'));
        }).then(function ()
        {
            return models.emailsTokens.createAsync(require('./email_tokens.json'));
        }).then(function ()
        {
            return models.emailErrors.createAsync(require('./email_errors.json'));
        }).then(function ()
        {
            return models.phrases.createAsync(require('./job_area.json'));
        }).then(function ()
        {
            return models.phrases.createAsync(require('./job_descriptor'));
        }).then(function ()
        {
            return models.phrases.createAsync(require('./job_type.json'));
        }).then(function ()
        {
            console.log('finish dictionary data');
        }).catch(function (error)
        {
            console.error('error');
            console.error(error);
        });
    }

    function seedStandAlone()
    {
        return connectPromise(require('./../app/config/config').url).then(seedByServer);
    }

    module.exports = {
        seedByServer: seedByServer,
        seedStandAlone: seedStandAlone
    };

})();
