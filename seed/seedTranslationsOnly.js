(function ()
{
    'use strict';

    var translationsModel = require('../app/DAO/translationsDAO').model;

    var Promise = require('bluebird');
    var mongoose = require('mongoose-bird')(require('mongoose'));
    var connectPromise = Promise.promisify(mongoose.connect, mongoose);

    function seedByServer()
    {
        return translationsModel.removeAsync({}).then(function ()
        {
            return translationsModel.createAsync(require('./translations.json'));
        }).then(function ()
        {
            console.log('Translations loaded');
        }).catch(function (error)
        {
            console.error('error');
            console.error(error);
        });
    }

    function seedStandAlone()
    {
        return connectPromise(require('./../app/config/config').url).then(seedByServer);
    }

    seedStandAlone().then(function ()
    {
        process.exit(0);
    }).catch(function ()
    {
        process.exit(1);
    });

})();
